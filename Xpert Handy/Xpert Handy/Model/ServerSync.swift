//
//  ServerSync.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/6/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import Alamofire
import Reachability
import SDWebImage


class ServerSync: NSObject {
    
    static  let shared = ServerSync()
    
    enum BackendError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
        case operationFailedError(status: Int ,reason: String)
        case networkError(reason: String)
    }
    
    func sendWebRequestTo(url : String, parameters : [NSObject : AnyObject]?, method : HTTPMethod, completionHandler:@escaping (NSDictionary?, Error?) -> Void)  {
        
        let reachability = try! Reachability()
        
        if (reachability.connection != .unavailable)
        {
            guard let url = URL(string: url) else
            {
                return
            }
            
            let authToken = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.AuthToken) ?? nil
            
            
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            //request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            //request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("*/*", forHTTPHeaderField: "Accept")
            request.addValue(Constants.AppToken, forHTTPHeaderField: "appToken")
            
            request.httpBody = nil
            if(parameters != nil)
            {
                let data = try! JSONSerialization.data(withJSONObject: parameters! as NSDictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = data
            }
            
            if (authToken != nil)
            {
                request.addValue(authToken!, forHTTPHeaderField: "Authorization")
            }
            
            
            print(parameters ?? "")
            print(url )
            print(request.allHTTPHeaderFields ?? "")
            
            
            
            AF.request(request)
                .responseJSON { response in
                    
                    guard response.error == nil else {
                        
                        let error : Error = response.error!
                        completionHandler(nil,error)
                        return
                    }
                    guard let responseData = response.data else {
                        
                        print("Error: did not receive data")
                        let error = BackendError.objectSerialization(reason: "No data in response")
                        completionHandler(nil,error)
                        return
                    }
                    
                    do {
                        if let jsonDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? NSDictionary
                        {
                            print(jsonDict.prettyPrintedJSON ?? "")
                            
                            let status = jsonDict["status"] as? Int
                            
                            if (status == Constants.ResponseStatus.InvalidToken)
                            {
                                completionHandler(jsonDict,nil)
                                self.invalidAuthTockenDetetced(data: jsonDict)
                            }
                            if (status == Constants.ResponseStatus.NotFound)
                            {
                                
                            }
                            else
                            {
                                completionHandler(jsonDict,nil)
                            }
                            
                        } else {
                            let error = BackendError.objectSerialization(reason: "Something went wrong.")
                            completionHandler(nil,error)
                        }
                    } catch {
                        completionHandler(nil,error)
                        return
                    }
                    
                }
        }
        else
        {
            let error = BackendError.networkError(reason: "Internet connection failed.")
            completionHandler(nil,error)
        }
    }
    
    
    func uploadImageTo(url : String, image : UIImage, fileName: String, method : HTTPMethod, completionHandler:@escaping (NSDictionary?, Error?) -> Void)  {
        
        let reachability = try! Reachability()
        
        if (reachability.connection != .unavailable)
        {
            guard let imageURL = URL(string: url) else
            {
                return
            }
            
            
            
            let imgData = image.jpegData(compressionQuality: 0.5)!
            
            let authToken = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.AuthToken) ?? ""
            
            let headers: HTTPHeaders
            headers = ["Authorization" : authToken, "appToken" : Constants.AppToken, "Content-Type" : "application/json", "Accept" : "application/json" ]
            
            
            AF.upload(multipartFormData: { (multipartFormData) in
                
                //multipartFormData.append(fileName.data(using: String.Encoding.utf8)!, withName: "image")
                multipartFormData.append(imgData, withName: "image", fileName: "image.png", mimeType: "image/png")
                
            }, to: imageURL, usingThreshold: UInt64.init(), method: method, headers: headers).response{ response in
                
                if(response.error == nil)
                {
                    do
                    {
                        if let jsonData = response.data
                        {
                            let jsonDict = try JSONSerialization.jsonObject(with: jsonData) as! NSDictionary
                            print(jsonDict)
                            
                            completionHandler(jsonDict,nil)
                            
                            
                        }
                        
                    }
                    catch
                    {
                        let error = BackendError.objectSerialization(reason:  "Something went wrong.")
                        completionHandler(nil,error)
                        
                    }
                }
                else
                {
                    print(response.description )
                    print(response.data ?? "" )
                    
                    print(response.debugDescription )
                    
                    let error = BackendError.objectSerialization(reason:  "Something went wrong.")
                    completionHandler(nil,error)
                    
                }
                
                
            }
            
        }
        else
        {
            let error = BackendError.networkError(reason: "Internet connection failed.")
            completionHandler(nil,error)
        }
    }
    
    
    func invalidAuthTockenDetetced(data : NSDictionary)
    {
        let appState = UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.AppState)
        if (appState == 2)
        {
            if let topVC = UIApplication.getTopMostViewController() {
                
                //Core.shared.signOut()
                
                let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "OnBoadingController") as? OnBoadingController
                topVC.navigationController?.pushViewController(vc!, animated: false)
                
                let errorDict = data.value(forKey: "error")
                let errorMessage = (errorDict as AnyObject).value(forKey: "message")
                
                topVC.alertMessageOk(title: "Error", message: errorMessage as! String)
                
                
            }
        }
    }
    
    
    func registerUser(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        
        
        self.sendWebRequestTo(url: Constants.API.Register, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func uploadImage(image : UIImage, fileName: String, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.uploadImageTo(url:  Constants.API.UploadImage, image: image, fileName: fileName ,method: .post) { (jsonDict, error) in
            completionHandler(jsonDict,error)
        }
    }
    
    
    func getServiceCategories(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.ServiceCategory, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getInformationVideos(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.GetInformationVideo, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getQuestinnareList(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.GetQuestionnaire, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getJobList(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.JobList, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getMyPosts(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.MyPosts, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getServiceProvider(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.GetServiceProvider, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func updateUser(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.UpdateUser, parameters: data , method: .put) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func updatServicecategory(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.ServiceCategory, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getUSerDeatails(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.GetUser, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func postJob(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.JobList, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func login(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.Login, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func resetPassword(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.PasswordReset, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func changePassword(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.ChangePassword, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    
    func getverifyPhoneOtp(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.VerifyMobile, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func verifyPhoneotp(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.VerifyMobile, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getverifyEmailOtp(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.VerifyEmail, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func verifyEmailOtp(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.VerifyMobile, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getSubscriptionList(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.Subscription, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func chooseSubscription(subId : Int, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let url = Constants.API.Subscription+"/"+String(subId)
        self.sendWebRequestTo(url: url, parameters: nil , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func confirmSubscription(subId : Int, data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let url = Constants.API.SubscriptionConfirm+"/"+String(subId)
        self.sendWebRequestTo(url: url, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getJobById(jobId : Int, data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let url = Constants.API.GetJob+"/"+String(jobId)
        self.sendWebRequestTo(url: url, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func updateJobStatus(jobId : Int, data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.UpdateJobStatus, jobId)
        self.sendWebRequestTo(url: urlString, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    func getSubCategories(catId : Int, data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.SubCategory, catId)
        self.sendWebRequestTo(url: urlString, parameters: nil , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getOffersForCategory(catId : Int, data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.OfferForCategory, catId)
        self.sendWebRequestTo(url: urlString, parameters: nil , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getOffers(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.Offers, parameters: nil , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getUser(userId : Int, data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.GetUserById, userId)
        
        
        self.sendWebRequestTo(url: urlString, parameters: nil , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getJobNotification(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.GetJobNotification, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getJobPayments(jobId : Int,data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.GetJobPayment, jobId)
        
        self.sendWebRequestTo(url: urlString, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func applyOffer(jobId : Int,data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.ApplyOffer, jobId)
        
        self.sendWebRequestTo(url: urlString, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getJobPaymentsPaid(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.JobPaymentsPaid, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getJobPaymentsReceived(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.JobPaymentsReceived, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func choosePayments(jobId : Int,data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.ChoosePaymentMethod, jobId)
        
        self.sendWebRequestTo(url: urlString, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func confirmPayments(jobId : Int,data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.ConfirmPayment, jobId)
        
        self.sendWebRequestTo(url: urlString, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func getBankAccont(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.Getbank, parameters: data , method: .get) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    
    func saveBankAccont(data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        self.sendWebRequestTo(url: Constants.API.SaveBank, parameters: data , method: .post) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
    func deleteBankAccont(bankId:Int, data : [NSObject : AnyObject]?, completionHandler:@escaping (NSDictionary?, Error?) -> Void) {
        
        let urlString = String(format: Constants.API.DeleteBank, bankId)
        
        self.sendWebRequestTo(url: urlString, parameters: data , method: .delete) { (jsonDict, error) in
            
            completionHandler(jsonDict,error)
        }
    }
}
