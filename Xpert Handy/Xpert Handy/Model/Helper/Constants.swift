//
//  Constants.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/6/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit


struct Constants {
    
    enum Appstate: Int {
        case DEFAULT = 0
        case UNCONFIRMED
        case LOGGEDIN
    }
    
    struct Font{
        static let BOLD_FONT         = "Poppins-Bold"
        static let REGULAR_FONT      = "Poppins-Regular"
        static let MEDIUM_FONT       = "Poppins-Medium"
        static let SEMI_BOLD_FONT    = "Poppins-SemiBold"
        
        
    }
    
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_BOUNDS        = UIScreen.main.bounds
        static let SCREEN_SIZE          = SCREEN_BOUNDS.size
        static let SCREEN_WIDTH         = SCREEN_SIZE.width
        static let SCREEN_HEIGHT        = SCREEN_SIZE.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let ACTION_H             = 120
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH  < 568.0
        static let IS_IPHONE_5_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_X_MAX      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        static let IS_IPHONE_X_SERIES   = IS_IPHONE_X_MAX || IS_IPHONE_X
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
        static let IS_IPAD_PRO_2ND      = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH > 1024.0 && ScreenSize.SCREEN_MAX_LENGTH < 1366.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1366.0
        
    }
    
    static let AppType              = ""
    static let AppDomain            = "http://xperthandy.in/api/v1/"
    static let AppBaseDomain        = "http://xperthandy.in"
    static let AppToken             = "fsda987243jhs89g29dsals89fl29ls932hs92hsa92"
    
    
    
    struct API{
        
        static let Register              = AppDomain + "register"
        static let GetUser               = AppDomain + "user"
        static let UpdateUser            = AppDomain + "user"
        static let UpdateProfileImage    = AppDomain + "user/profileImage"
        static let Logout                = AppDomain + "user/logout"
        static let ServiceCategory       = AppDomain + "serviceCategory"
        static let UploadImage           = AppDomain + "user/profileImage"
        static let GetInformationVideo   = AppDomain + "informationVideo"
        static let GetQuestionnaire      = AppDomain + "questionnaire"
        static let JobList               = AppDomain + "job"
        static let Login                 = AppDomain + "login"
        static let PasswordReset         = AppDomain + "passwordReset"
        static let VerifyMobile          = AppDomain + "verify-mobile"
        static let VerifyEmail           = AppDomain + "verify-email"
        static let Subscription          = AppDomain + "subscription"
        static let SubscriptionConfirm   = AppDomain + "subscription/confirm"
        static let ChangePassword        = AppDomain + "changePassword"
        static let GetJob                = AppDomain + "job"
        static let UpdateJobStatus       = AppDomain + "job/%d/updateStatus"
        static let SubCategory           = AppDomain + "serviceCategory/%d/subCategory"
        static let OfferForCategory      = AppDomain + "offer/serviceCategory/%d"
        static let Offers                = AppDomain + "offer"
        static let GetUserById           = AppDomain + "user/%d"
        static let GetJobNotification    = AppDomain + "jobNotification"
        static let GetJobPayment         = AppDomain + "%d/jobPayment"
        static let ApplyOffer            = AppDomain + "%d/applyOffer"
        static let JobPaymentsPaid       = AppDomain + "jobPaymentsPaid"
        static let JobPaymentsReceived   = AppDomain + "jobPaymentsReceived"
        static let ChoosePaymentMethod   = AppDomain + "%d/paymentMethod"
        static let ConfirmPayment        = AppDomain + "%d/confirmPayment"
        static let Getbank               = AppDomain + "bankAccount"
        static let SaveBank              = AppDomain + "bankAccount"
        static let DeleteBank            = AppDomain + "bankAccount/%d"
        static let MyPosts               = AppDomain + "job/myPosts"
        static let GetServiceProvider    = AppDomain + "serviceProvider"
        
        
    }
    
    struct RazorPayKeys
    {
        static let keyId     = "rzp_test_AekvglK3ygDvVH"
        static let secretkey = "gfPKDIy2eA16DtlavgnC8lrV"
    }    
    struct ResponseStatus
    {
        static let Success              = 200
        static let CreateSuccess        = 201
        static let BadRequest           = 400
        static let InvalidToken         = 401
        static let Forbidden            = 403
        static let NotFound             = 404
        static let NotVerified          = 412
    }
    
    struct UserDefaultKeys{
        
        static let AppState                = "app_state_ofuser"
        static let AuthToken               = "app_auth_token"
        static let UserEmail               = "app_user_email"
        static let UserType                = "app_user_type"
        static let UserPhone               = "app_user_phone"
        static let UserPassword            = "app_user_password"
        
        
    }
    
}

