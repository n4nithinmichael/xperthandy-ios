//
//  LeftSideMenuViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 10/20/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

class LeftSideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var usertitleLabel: UILabel!
    @IBOutlet var sidemenuTableView: UITableView!
    var categoryArray = Array<Int>()
    var userType = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.usertitleLabel.text = ""
        self.userImageView.setCornerRadius(30.0)
        self.sidemenuTableView.tableFooterView = UIView()
        
        self.getUser()
        
        self.usernameLabel.font =  UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        self.userType =  UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.UserType)
        
        // Do any additional setup after loading the view.
    }
    
    
    func getUser()  {
        
        
        
        
        ServerSync.shared.getUSerDeatails(data: nil) { (respose, error) in
            
            if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = respose?.value(forKey: "data") as? NSDictionary
                let name = dataDict?.value(forKey: "name") as? String
                let profile_image = dataDict?.value(forKey: "profile_image") as? String
                self.usernameLabel.text = name
                self.categoryArray = dataDict?.value(forKey: "category") as? Array<Int> ?? Array<Int>()
                
                if(self.categoryArray.count>0)
                {
                    self.loadServieCategory()
                }
                self.userImageView.sd_setImage(with: URL(string: profile_image ?? "" ), placeholderImage: UIImage(named: "customer_avatar"))
            }
        }
    }
    
    
    func loadServieCategory()  {
        ServerSync.shared.getServiceCategories(data: nil) { (responseObject, error) in
            
            if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                var catArray =  Array<NSDictionary> ()
                
                let dataDict = responseObject?.value(forKey: "data")
                let serviceArray = dataDict as! Array<NSDictionary>
                
                for category in serviceArray
                {
                    let catId = category.value(forKey: "id") as! Int
                    if (self.categoryArray.contains(catId))
                    {
                        catArray.append(category)
                    }
                }
                
                var occupation = ""
                //                for category in catArray
                //                {
                //                    let catname = category.value(forKey: "name") as! String
                //                    occupation = occupation+catname
                //                }
                
                if(catArray.count == 1)
                {
                    let catname = catArray[0].value(forKey: "name") as! String
                    occupation = catname
                }
                else if(catArray.count > 1)
                {
                    let frstcatname = catArray[0].value(forKey: "name") as! String
                    let secondname = catArray[1].value(forKey: "name") as! String
                    
                    occupation = frstcatname+" and "+secondname
                }
                self.usertitleLabel.text = occupation
                
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.userType == 2 {
            return 9
            
        }
        else{
            
            return 6
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SidemenuTableViewCell", for: indexPath) as! SidemenuTableViewCell
        
        cell.nameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        
        if self.userType == 2 {
            
            if indexPath.row == 0 {
                cell.nameLabel.text = "Dashboard"
                cell.iconImageView.image = UIImage(named: "sidemenu_dashboard")
            }
            else if indexPath.row == 1 {
                cell.nameLabel.text = "My profile"
                cell.iconImageView.image = UIImage(named: "sidemenu_profile")
            }
            else if indexPath.row == 2 {
                cell.nameLabel.text = "Tasks"
                cell.iconImageView.image = UIImage(named: "sidemenu_tasks")
            }
            else if indexPath.row == 3 {
                cell.nameLabel.text = "Payments"
                cell.iconImageView.image = UIImage(named: "sidemenu_payments")
            }
            else if indexPath.row == 4 {
                cell.nameLabel.text = "Notifications"
                cell.iconImageView.image = UIImage(named: "sidemenu_notification")
            }
            else if indexPath.row == 5 {
                cell.nameLabel.text = "Subscriptions"
                cell.iconImageView.image = UIImage(named: "sidemenu_logout")
            }
            else if indexPath.row == 6 {
                cell.nameLabel.text = "Contact Us"
                cell.iconImageView.image = UIImage(named: "sidemenu_contactus")
            }
            else if indexPath.row == 7 {
                cell.nameLabel.text = "Help"
                cell.iconImageView.image = UIImage(named: "sidemenu_help")
            }
            else if indexPath.row == 8 {
                cell.nameLabel.text = "Logout"
                cell.iconImageView.image = UIImage(named: "sidemenu_logout")
            }
            
        }
        else{
            
            if indexPath.row == 0 {
                cell.nameLabel.text = "Dashboard"
                cell.iconImageView.image = UIImage(named: "sidemenu_dashboard")
            }
            else if indexPath.row == 1 {
                cell.nameLabel.text = "My profile"
                cell.iconImageView.image = UIImage(named: "sidemenu_profile")
            }
            else if indexPath.row == 2 {
                cell.nameLabel.text = "Notifications"
                cell.iconImageView.image = UIImage(named: "sidemenu_notification")
            }
            else if indexPath.row == 3 {
                cell.nameLabel.text = "Contact Us"
                cell.iconImageView.image = UIImage(named: "sidemenu_contactus")
            }
            else if indexPath.row == 4 {
                cell.nameLabel.text = "Help"
                cell.iconImageView.image = UIImage(named: "sidemenu_help")
            }
            else if indexPath.row == 5 {
                cell.nameLabel.text = "Logout"
                cell.iconImageView.image = UIImage(named: "sidemenu_logout")
            }
        }
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if self.userType == 2 {
            
            if indexPath.row == 0 {
                
                self.dismiss(animated: true) {
                    
                }
            }
            else if indexPath.row == 1 {
                
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: false) {
                    
                }
                
            }
            else if indexPath.row == 2 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerJobsListViewController") as? CustomerJobsListViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: false) {
                    
                }
            }
            else if indexPath.row == 3 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "ServiceProviderPaymentsViewController") as? ServiceProviderPaymentsViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: false) {
                    
                }
                
                
                
            }
            else if indexPath.row == 4 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerNotificationsViewController") as? CustomerNotificationsViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: false) {
                    
                }
                
            }
            else if indexPath.row == 5 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "SubscriptionsListViewController") as? SubscriptionsListViewController
                vc?.isInnerPage = true
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: true) {
                    
                }
                
            }
            else if indexPath.row == 6 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: true) {
                    
                }
                
            }
            else if indexPath.row == 7 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: true) {
                    
                }
                
            }
            else if indexPath.row == 8 {
                
                UserDefaults.standard.setValue(0, forKey: Constants.UserDefaultKeys.AppState)
                
                
                if let topVC = UIApplication.getTopMostViewController() {
                    let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "OnBoadingController") as? OnBoadingController
                    topVC.navigationController?.pushViewController(vc!, animated: false)
                }
                
            }
        }
        else{
            
            if indexPath.row == 0 {
                self.dismiss(animated: true) {
                    
                }
            }
            else if indexPath.row == 1 {
                
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: false) {
                    
                }
                
            }
            else if indexPath.row == 2 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerNotificationsViewController") as? CustomerNotificationsViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: false) {
                    
                }
                
            }
            else if indexPath.row == 3 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: true) {
                    
                }
                
            }
            else if indexPath.row == 4 {
                
                let topViewController = UIApplication.getTopMostViewController()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController
                topViewController?.navigationController?.pushViewController(vc!, animated: true)
                
                self.dismiss(animated: true) {
                    
                }
                
            }
            else if indexPath.row == 5 {
                
                UserDefaults.standard.setValue(0, forKey: Constants.UserDefaultKeys.AppState)
                
                if let topVC = UIApplication.getTopMostViewController() {
                    let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "OnBoadingController") as? OnBoadingController
                    topVC.navigationController?.pushViewController(vc!, animated: false)
                }
                
            }
            
        }
        
        
    }
    
}
