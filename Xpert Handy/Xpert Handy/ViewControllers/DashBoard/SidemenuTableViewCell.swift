//
//  SidemenuTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 2/22/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class SidemenuTableViewCell: UITableViewCell {
    
    
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
