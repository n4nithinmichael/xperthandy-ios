//
//  EmailSendViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 6/15/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class EmailSendViewController: UIViewController {
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var resentButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.loginButton?.titleLabel!.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.resentButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.loginButton.setCornerRadius(4.0)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendButtonClicked(_ sender: Any) {
    }
    @IBAction func loginButtonClicked(_ sender: Any) {
         
    }
    
}
