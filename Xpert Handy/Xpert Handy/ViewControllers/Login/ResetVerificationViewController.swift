//
//  ResetVerificationViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 6/15/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit
import OTPFieldView

class ResetVerificationViewController: UIViewController,OTPFieldViewDelegate {
    
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var verifyButton: UIButton!
    @IBOutlet var resentButton: UIButton!
    @IBOutlet var otpView: OTPFieldView!
    var otpString: String!
    var enteredOtp: String!
    var emailAddress: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        
        self.verifyButton?.titleLabel!.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.resentButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.verifyButton.setCornerRadius(4.0)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func setupOtpView(){
        
        self.otpView.fieldsCount = 4
        self.otpView.fieldBorderWidth = 0
        self.otpView.defaultBorderColor = UIColor.black
        self.otpView.filledBorderColor = UIColor.green
        self.otpView.cursorColor = "151A5A".getUIColorFromHex()
        self.otpView.defaultBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.otpView.filledBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.otpView.displayType = .roundedCorner
        self.otpView.fieldSize = 50
        self.otpView.separatorSpace = 10
        self.otpView.shouldAllowIntermediateEditing = true
        self.otpView.delegate = self
        self.otpView.becomeFirstResponder()
        self.otpView.fieldFont = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)!
        self.otpView.initializeUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupOtpView()
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendButtonClicked(_ sender: Any) {
    }
    @IBAction func verifyButtonClicked(_ sender: Any) {
        
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
        
    }
    
    func enteredOTP(otp: String) {
        if otpString == otp {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateNewPasswordViewController") as? CreateNewPasswordViewController
            //            vc?.emailAddress = self.emailField.text
            //            vc?.otpString = otp?.stringValue
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        
        self.enteredOtp = otp
        print("OTPString: \(otp)")
        
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        print("Has entered all OTP? \(hasEnteredAll)")
        return false
    }
}
