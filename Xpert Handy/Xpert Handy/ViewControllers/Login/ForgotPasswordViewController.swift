//
//  ForgotPasswordViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 6/15/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var emailView: UIView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.emailLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailField.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.emailView.setCornerRadius(4.0)
        self.sendButton?.titleLabel!.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.loginButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.sendButton.setCornerRadius(4.0)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendButtonAction(_ sender: Any) {
        
        if(self.emailField.text?.count == 0 )
        {
            self.alertMessageOk(title: "Email/Mobile", message: "Please enter email address or mobile number")
            
        }
        else{
            
            //
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetVerificationViewController") as? ResetVerificationViewController
            //            vc?.emailAddress = self.emailField.text
            //            self.navigationController?.pushViewController(vc!, animated: true)
            
            self.forgot()
        }
        
    }
    
    func forgot()
    {
        
        UserDefaults().set(nil, forKey: Constants.UserDefaultKeys.AuthToken)
        
        let params: NSDictionary = ["email": self.emailField!.text!]
        
        self.showActivityIndicator(text: "Please wait.")
        ServerSync.shared.resetPassword(data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            self.dismissActivityIndicator()
            //fatalError("Failed to load a MyCustomCell from the table.")
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    
                    let data = responseObject!["data"] as? NSDictionary
                    let otp = data?.value(forKey: "otp_code") as? NSNumber
                    let token = data?.value(forKey: "token") as? String
                    UserDefaults().set(token, forKey: Constants.UserDefaultKeys.AuthToken)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetVerificationViewController") as? ResetVerificationViewController
                    vc?.emailAddress = self.emailField.text
                    vc?.otpString = otp?.stringValue
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                    self.alertMessageOk(title: "Otp", message: otp?.stringValue ?? "")
                    
                }
                else{
                    self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                }
                
            }
            
        }
    }
    
}
