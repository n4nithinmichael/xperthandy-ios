//
//  LoginViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 6/15/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var forgotPwButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var pwField: UITextField!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var pwlabel: UILabel!
    @IBOutlet var emailView: UIView!
    @IBOutlet var pwView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.loginButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.signupButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.forgotPwButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.pwlabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailField.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.emailView.setCornerRadius(4.0)
        self.pwField.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.pwView.setCornerRadius(4.0)
        self.loginButton.setCornerRadius(4.0)

    }
    
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        
        if(self.emailField.text?.count == 0 )
        {
            self.alertMessageOk(title: "Email", message: "Please enter email address.")
            
        }
        else if(self.pwField.text?.count == 0 )
        {
            self.alertMessageOk(title: "Password", message: "Please enter your password.")
            
        }
        else{
            
            self.login()
        }
        
    }
    
    func login()
    {
        let params: NSDictionary = ["email": self.emailField!.text!, "password": self.pwField.text!]
        
        ServerSync.shared.login(data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            //fatalError("Failed to load a MyCustomCell from the table.")
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    if(responseObject!["data"] != nil){
                        let data = responseObject!["data"] as? NSDictionary
                        let rolesArray = data?.value(forKey: "roles") as? NSArray
                        
                        if (rolesArray?.count ?? 0>0)
                        {
                            
                            
                            let rolesdict = rolesArray!.object(at: 0) as? NSDictionary
                            let userTypeId = rolesdict!.value(forKey: "id") as? Int
                            
                            
                            let tocken = data?.value(forKey: "token") as? NSString
                            let email = data?.value(forKey: "email") as? NSString
                            let phone = data?.value(forKey: "phone") as? NSString
                            
                            
                            UserDefaults.standard.setValue(2, forKey: Constants.UserDefaultKeys.AppState)
                            UserDefaults().set(tocken, forKey: Constants.UserDefaultKeys.AuthToken)
                            UserDefaults.standard.setValue(email, forKey: Constants.UserDefaultKeys.UserEmail)
                            UserDefaults.standard.setValue(phone, forKey: Constants.UserDefaultKeys.UserPhone)
                            UserDefaults.standard.setValue(userTypeId, forKey: Constants.UserDefaultKeys.UserType)
                            UserDefaults.standard.setValue(self.pwField.text!, forKey: Constants.UserDefaultKeys.UserPassword)
                            
                            
                            if userTypeId == 2 {
                                
                                let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                                //let navigationController:UINavigationController = mainStoryboard.instantiateInitialViewController() as! UINavigationController
                                
                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ServiceProviderDashBoardViewController") as! ServiceProviderDashBoardViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            else {
                                
                                let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                                // let navigationController:UINavigationController = mainStoryboard.instantiateInitialViewController() as! UINavigationController
                                
                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "CustomerDashBoardViewController") as! CustomerDashBoardViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                            
                        }
                        
                    }
                    
                }
                else{
                    self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                }
                
            }
            
        }
    }
    
}
