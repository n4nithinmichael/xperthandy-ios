//
//  CreateNewPasswordViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 7/12/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class CreateNewPasswordViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var pwField: UITextField!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var pwlabel: UILabel!
    @IBOutlet var emailView: UIView!
    @IBOutlet var pwView: UIView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var pwdescriptionLabel: UILabel!
    @IBOutlet var confirmpwdescriptionLabel: UILabel!
    @IBOutlet var currentPwField: UITextField!
    @IBOutlet var currentPwBgView: UIView!
    @IBOutlet var currentPwLabel: UILabel!
    @IBOutlet var currentPwView: UIView!
    @IBOutlet var newPwTop: NSLayoutConstraint!
    
    var setNewPw : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(self.setNewPw == true)
        {
            self.newPwTop.constant = 128.0
            self.currentPwView.isHidden = false
            self.titleLabel.text = "Change password"
            self.loginButton.setTitle("Change password", for: .normal)
        }
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.loginButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.emailLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.pwlabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.currentPwLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailField.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.currentPwField.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.emailView.setCornerRadius(5.0)
        self.pwField.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.pwView.setCornerRadius(5.0)
        self.currentPwBgView.setCornerRadius(5.0)
        self.loginButton.setCornerRadius(5.0)
        self.pwdescriptionLabel?.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.confirmpwdescriptionLabel?.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.descriptionLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func restButtonClicked(_ sender: Any) {
        
        if(self.setNewPw == true)
        {
            let pw = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.UserPassword) ?? ""
            
            if( pw.count > 0 && self.currentPwField.text != pw )
            {
                self.alertMessageOk(title: "Current Password", message: "Current password is wrong.")
                
            }
            else if(self.emailField.text?.count ?? 0 < 8 )
            {
                self.alertMessageOk(title: "Password", message: "Please enter valid new password.")
                
            }
            else if(self.pwField.text?.count ?? 0 < 8 )
            {
                self.alertMessageOk(title: "Confirm Password", message: "Both passwords must match.")
                
            }
            else if(self.pwField.text != self.emailField.text )
            {
                self.alertMessageOk(title: "Confirm Password", message: "Both passwords must match.")
                
            }
            else{
                
                self.reset()
            }
        }
        else{
            
            if(self.emailField.text?.count ?? 0 < 8 )
            {
                self.alertMessageOk(title: "Password", message: "Please enter valid password.")
                
            }
            else if(self.pwField.text?.count ?? 0 < 8 )
            {
                self.alertMessageOk(title: "Confirm Password", message: "Both passwords must match.")
                
            }
            else if(self.pwField.text != self.emailField.text )
            {
                self.alertMessageOk(title: "Confirm Password", message: "Both passwords must match.")
                
            }
            else{
                
                
                self.reset()
            }
        }
        
        
    }
    
    func reset()
    {
        let params: NSDictionary = ["password": self.emailField!.text!, "password_confirm": self.pwField.text!]
        
        ServerSync.shared.changePassword(data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            //fatalError("Failed to load a MyCustomCell from the table.")
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    if(responseObject!["data"] != nil){
                        let data = responseObject!["data"] as? NSDictionary
                        let rolesArray = data?.value(forKey: "roles") as? NSArray
                        
                        if (rolesArray?.count ?? 0>0)
                        {
                            
                            
                            let rolesdict = rolesArray!.object(at: 0) as? NSDictionary
                            let userTypeId = rolesdict!.value(forKey: "id") as? Int
                            let tocken = data?.value(forKey: "token") as? NSString
                            let email = data?.value(forKey: "email") as? NSString
                            let phone = data?.value(forKey: "phone") as? NSString
                            
                            
                            UserDefaults.standard.setValue(2, forKey: Constants.UserDefaultKeys.AppState)
                            UserDefaults().set(tocken, forKey: Constants.UserDefaultKeys.AuthToken)
                            UserDefaults.standard.setValue(email, forKey: Constants.UserDefaultKeys.UserEmail)
                            UserDefaults.standard.setValue(phone, forKey: Constants.UserDefaultKeys.UserPhone)
                            UserDefaults.standard.setValue(userTypeId, forKey: Constants.UserDefaultKeys.UserType)
                            UserDefaults.standard.setValue(self.pwField.text!, forKey: Constants.UserDefaultKeys.UserPassword)
                            
                            if(self.setNewPw == true)
                                
                            {
                                self.navigationController?.popViewController(animated: true)
                            }
                            else{
                                
                                if userTypeId == 2 {
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                                    //let navigationController:UINavigationController = mainStoryboard.instantiateInitialViewController() as! UINavigationController
                                    
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "ServiceProviderDashBoardViewController") as! ServiceProviderDashBoardViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                else {
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                                    // let navigationController:UINavigationController = mainStoryboard.instantiateInitialViewController() as! UINavigationController
                                    
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "CustomerDashBoardViewController") as! CustomerDashBoardViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                }
                            }
                            
                        }
                        
                    }
                    
                }
                else{
                    self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                }
                
            }
            
        }
    }
    
}
