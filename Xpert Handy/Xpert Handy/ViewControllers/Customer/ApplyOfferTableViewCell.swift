//
//  ApplyOfferTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 12/29/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class ApplyOfferTableViewCell: UITableViewCell {
    
    @IBOutlet var offerTitleLabel: UILabel!
    @IBOutlet var offerValidityLabel: UILabel!
    @IBOutlet var offerDescriptionLabel: UILabel!
    @IBOutlet var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
