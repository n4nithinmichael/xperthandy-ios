//
//  SubscriptionDetailsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 11/15/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit
import Razorpay


@objc protocol SubscriptionDetailsViewDelegate: AnyObject {
    
    func  subscriptionsPaid()
    
}

class SubscriptionDetailsViewController: UIViewController,ExternalWalletSelectionProtocol,RazorpayPaymentCompletionProtocolWithData,RazorpayPaymentCompletionProtocol {

    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        self.alertMessageOk(title: "Error", message: str)

    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        self.confirmPayment(paymentId: payment_id)

    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        self.alertMessageOk(title: "Error", message: str)

    }

    func onPaymentSuccess(_ payment_id: String) {

        self.confirmPayment(paymentId: payment_id)

    }
    
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        
    }
    
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var freelabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var subTitleLabel: UILabel!
    @IBOutlet var includeLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var subscribeButton: UIButton!
    @IBOutlet var payemntStartsLabel: UILabel!
    var isInnerPage = Bool()
    var subDict = NSDictionary()
    var selectedSubId = Int()
    var selectedOrderId = String()
    var razorpay: RazorpayCheckout!
    @objc public var delegate : SubscriptionDetailsViewDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        if(isInnerPage)
        {
            self.titleLabel.isHidden = false
            self.backButton.isHidden = false
        }
        

        self.subTitleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.priceLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 30)
        self.freelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.subscribeButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.includeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.descriptionLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.payemntStartsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)



        
        razorpay = RazorpayCheckout.initWithKey(Constants.RazorPayKeys.keyId, andDelegate: self)
        razorpay.setExternalWalletSelectionDelegate(self)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
           
        super.viewWillAppear(animated)
        
        self.subTitleLabel.text = subDict.value(forKey: "name") as? String
        self.freelabel.text = subDict.value(forKey: "freeValidity") as? String
        
        let rate  = subDict.value(forKey: "rate") as? Int
        let ratestrinmg = "\(rate ?? 0)"
        let validity = subDict.value(forKey: "validity") as? String
        let description = subDict.value(forKey: "description") as? String
        let shortDesc = subDict.value(forKey: "shortDescription") as? String
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = NSTextAlignment.left
        
        let priceString = NSMutableAttributedString(string: "₹ \(ratestrinmg) / \(validity ?? "")", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        priceString.setColorForText("₹ \(ratestrinmg)", with: "#151A5A".getUIColorFromHex())
        priceString.setFontForText("₹ \(ratestrinmg)", with:  UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 30)!)
        priceString.setColorForText("/ \(validity ?? "")", with: "#898CAD".getUIColorFromHex())
        priceString.setFontForText("/ \(validity ?? "")", with:  UIFont(name: Constants.Font.REGULAR_FONT, size: 16)!)

  

        self.priceLabel.attributedText = priceString
        self.priceLabel.numberOfLines = 1
        self.bgView.setCornerRadius(10.0)
        self.subscribeButton.setCornerRadius(5.0)

        self.descriptionLabel.text = description
        self.includeLabel.text = shortDesc
        
        let color  = subDict.value(forKey: "bgColor") as? String

        self.bgView.backgroundColor = color?.getUIColorFromHex()
        self.freelabel.setCornerRadius(12.5)
        let iconimage  = subDict.value(forKey: "icon") as? String

        self.avatarImageView.sd_setImage(with: URL(string: iconimage ?? "" ), placeholderImage: UIImage(named: "customer_avatar"))
        
    }
       

    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func subscribeButtonClicked(_ sender: Any) {
        
                let subId =  subDict.value(forKey: "id") as? Int
                self.selectedSubId = subId!
                self.choosePayment(subId: subId!)
    }
    
    
    func choosePayment(subId:Int){
        
        ServerSync.shared.chooseSubscription(subId: subId) { (respose, error) in
            
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it

                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                                        
                                        
                    let dataDict = respose?.value(forKey: "data")
                    self.showPaymentForm(dataDict: dataDict as! NSDictionary)
                   // self.showPaymentFormFromData(dataDict: dataDict as! NSDictionary)
                }
            }
            
       
        }
        
    }
    
    
    func confirmPayment(paymentId:String){
        
        
        let params: NSDictionary = ["payment_gateway_order_id":self.selectedOrderId ,"payment_gateway_payment_id":paymentId,"payment_gateway_signature": "9ef4dffbfd84f1318f6739a3ce19f9d85851857ae648f114332d8401e0949a3d"]

        
        ServerSync.shared.confirmSubscription(subId: self.selectedSubId, data: params as [NSObject : AnyObject]) { (respose, error) in
            
            

            if let error = error {
                // got an error in getting the data, need to handle it

                self.alertMessageOk(title: "Error", message: "Subscription failed.")

            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                                        
                    self.delegate?.subscriptionsPaid()

                    //let dataDict = respose?.value(forKey: "data")
                }
                else
                {
                    self.alertMessageOk(title: "Error", message: "Subscription failed.")

                }
            }
            
       
        }
        
    }
    
    
    internal func showPaymentFormFromData(dataDict: NSDictionary){
//        let options: [String:Any] = [
//                    "key": Constants.RazorPayKeys.keyId,
//                    "amount": dataDict.value(forKey: "amount") as! Int,
//                    "currency": dataDict.value(forKey: "currency") as! String,
//                    "description": dataDict.value(forKey: "description") as! String,
//                    "order_id": dataDict.value(forKey: "order_id") as! String,
//                    "image": dataDict.value(forKey: "image") as! String,
//                    "name": dataDict.value(forKey: "name") as! String,
//                    "prefill": [
//                        "email": dataDict.value(forKey: "user_email") as! String,
//                    ],
//                    "theme": [
//                        "color": dataDict.value(forKey: "theme.color") as! String
//                      ]
//                ]
        
        let options = dataDict.mutableCopy()
    
        razorpay.open(options as! [String:Any])
    }
    
    
    internal func showPaymentForm(dataDict: NSDictionary){
        
        let phone = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.UserPhone) ?? ""

        self.selectedOrderId = dataDict.value(forKey: "order_id") as! String
        let options: [String:Any] = [
                    "key": Constants.RazorPayKeys.keyId,
                    "amount": dataDict.value(forKey: "amount") as! Int,
                    "currency": dataDict.value(forKey: "currency") as! String,
                    "description": dataDict.value(forKey: "description") as! String,
                    "order_id": dataDict.value(forKey: "order_id") as! String,
                    "image": dataDict.value(forKey: "image") as! String,
                    "name": dataDict.value(forKey: "name") as! String,
                    "prefill": [
                        "contact": phone,
                        "email": dataDict.value(forKey: "user_email") as! String
                    ],
                    "theme": [
                        "color": dataDict.value(forKey: "theme.color") as! String
                      ]
                ]
        razorpay.open(options)
    }

}
