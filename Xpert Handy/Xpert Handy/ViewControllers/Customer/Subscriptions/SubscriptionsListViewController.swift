//
//  SubscriptionsListViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/17/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit


@objc protocol SubscriptionsViewDelegate: AnyObject {
    
    func  subscriptionsSelcted(subDict:NSDictionary)
    
}


class SubscriptionsListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var listTableview: UITableView!
    @IBOutlet var backButton: UIButton!
    @objc public var delegate : SubscriptionsViewDelegate!
    var subsArray = NSArray()

    var isInnerPage = Bool()

    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.listTableview.tableFooterView = UIView()
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        self.getSubs()

        if(isInnerPage)
        {
            self.titleLabel.isHidden = false
            self.backButton.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    
    func getSubs()  {
        
        ServerSync.shared.getSubscriptionList(data: nil) { (respose, error) in
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it

                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                                        
                                        
                    let dataDict = respose?.value(forKey: "data")
                    self.subsArray = dataDict as! NSArray
                    self.listTableview!.reloadData()
        
                }
            }
            
       
        }
    }

    @IBAction func backButtonsPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.subsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionListTableViewCell", for: indexPath) as! SubscriptionListTableViewCell
        
        cell.titleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        cell.priceLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 30)
        cell.freelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.includeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)

        let subDict = self.subsArray[indexPath.row] as! NSDictionary

        cell.titleLabel.text = subDict.value(forKey: "name") as? String
        cell.freelabel.text = subDict.value(forKey: "freeValidity") as? String
        
        let rate  = subDict.value(forKey: "rate") as? Int
        let ratestrinmg = "\(rate ?? 0)"
        let validity = subDict.value(forKey: "validity") as? String
        
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = NSTextAlignment.left
        
        let priceString = NSMutableAttributedString(string: "₹ \(ratestrinmg) / \(validity ?? "")", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        priceString.setColorForText("₹ \(ratestrinmg)", with: "#151A5A".getUIColorFromHex())
        priceString.setFontForText("₹ \(ratestrinmg)", with:  UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 30)!)
        priceString.setColorForText("/ \(validity ?? "")", with: "#898CAD".getUIColorFromHex())
        priceString.setFontForText("/ \(validity ?? "")", with:  UIFont(name: Constants.Font.REGULAR_FONT, size: 16)!)

        
        cell.priceLabel.attributedText = priceString
        cell.priceLabel.numberOfLines = 1
        cell.bgView.setCornerRadius(10.0)
        
        let color  = subDict.value(forKey: "bgColor") as? String

        cell.bgView.backgroundColor = color?.getUIColorFromHex()
        cell.freelabel.setCornerRadius(12.5)
        let iconimage  = subDict.value(forKey: "icon") as? String

        cell.avatarImageView.sd_setImage(with: URL(string: iconimage ?? "" ), placeholderImage: UIImage(named: "customer_avatar"))
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let subDict = self.subsArray[indexPath.row] as! NSDictionary

        if(isInnerPage)
        {
            let topViewController = UIApplication.getTopMostViewController()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "SubscriptionDetailsViewController") as? SubscriptionDetailsViewController
            vc?.isInnerPage = self.isInnerPage
            vc?.subDict = subDict
            topViewController?.navigationController?.pushViewController(vc!, animated: true)
        }
        else{
            
            self.delegate.subscriptionsSelcted(subDict: subDict)
        }
        

    }

}
