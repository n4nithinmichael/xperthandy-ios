//
//  SubscriptionListTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/17/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class SubscriptionListTableViewCell: UITableViewCell {

    @IBOutlet var bgView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var freelabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var includeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
