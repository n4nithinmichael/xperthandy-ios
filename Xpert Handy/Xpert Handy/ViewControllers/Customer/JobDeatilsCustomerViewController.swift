//
//  JobDeatilsCustomerViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/10/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class JobDeatilsCustomerViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var taskTitlelabel: UILabel!
    @IBOutlet var taskLabel: UILabel!
    @IBOutlet var expertTitleLabel: UILabel!
    @IBOutlet var expertLabel: UILabel!
    @IBOutlet var dateTitleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var payLabel: UILabel!
    @IBOutlet var payTitleLabel: UILabel!
    @IBOutlet var addressTitleLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var jobStatusView: UIView!
    @IBOutlet var jobStatusLabel: UILabel!
    @IBOutlet var workAcceptedLabel: UILabel!
    @IBOutlet var workAcceptedTimeLabel: UILabel!
    @IBOutlet var inProgressLabel: UILabel!
    @IBOutlet var inProgressTimeLabel: UILabel!
    @IBOutlet var cancelButton: UIButton!
    
    @IBOutlet var addHoursButton: UIButton!
    @IBOutlet var adddayButton: UIButton!
    
    @IBOutlet var startTimelabel: UILabel!
    @IBOutlet var addExtraHourslabel: UILabel!
    @IBOutlet var timetextField: UITextField!
    @IBOutlet var selectPaylabel: UILabel!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var timeView: UIView!
    
    @IBOutlet var ratingPopUpMainView: UIView!
    @IBOutlet var ratingsView: UIView!
    
    @IBOutlet var ratingJobLabel: UILabel!
    @IBOutlet var ratingTimeLabel: UILabel!
    @IBOutlet var ratingExpertNameLabel: UILabel!
    @IBOutlet var ratingExpertTitleLabel: UILabel!
    @IBOutlet var rateYourExperiencelabel: UILabel!
    @IBOutlet var commentTextView: UITextView!
    @IBOutlet var commentView: UIView!
    @IBOutlet var submitButton: UIButton!
    
    @IBOutlet var workFinixhedLabel: UILabel!
    @IBOutlet var workFinishedTimeLabel: UILabel!
    
    @IBOutlet var workAcceptedStatus: UIImageView!
    @IBOutlet var workInProgressStatus: UIImageView!
    @IBOutlet var workCompletedStatus: UIImageView!
    
    @IBOutlet var paymentView: UIView!
    @IBOutlet var payemntDetailsLabel: UILabel!
    @IBOutlet var serviceCostLabel: UILabel!
    @IBOutlet var serviceAmontlabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var paymentButton: UIView!
    @IBOutlet var totalAmontLabel: UILabel!
    @IBOutlet var offerTableView: UITableView!
    
    
    
    var serviceId : Int!
    var selectedVocher : String?
    var serviceProviderId : Int!
    var offersArray = NSArray()
    var paymentDict = NSDictionary()
    
    var isVoucherApplied: Bool?
    
    
    var jobId = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.taskTitlelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.expertTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.dateTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.payTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.addressTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.inProgressTimeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.workAcceptedTimeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.workFinishedTimeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        
        
        
        self.taskLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.expertLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.dateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.payLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.addressLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.workAcceptedLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.inProgressLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.workFinixhedLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        
        
        self.jobStatusLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        self.jobStatusView.roundCorners(corners: [.topLeft,.topRight], size: 27)
        
        self.addHoursButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.adddayButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.cancelButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.saveButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        self.addExtraHourslabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.startTimelabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.timetextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.selectPaylabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        
        self.cancelButton.setCornerRadius(5.0)
        self.addHoursButton.setCornerRadius(5.0)
        self.saveButton.setCornerRadius(5.0)
        self.adddayButton.setCornerRadius(5.0)
        self.timeView.setCornerRadius(5.0)
        
        
        self.ratingPopUpMainView.isHidden = true
        
        
        self.ratingsView.setCornerRadius(5.0)
        self.submitButton.setCornerRadius(5.0)
        self.commentView.setCornerRadius(5.0)
        
        self.ratingJobLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.ratingTimeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.ratingExpertNameLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.ratingExpertTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.rateYourExperiencelabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.commentTextView.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.submitButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        
        self.payemntDetailsLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.serviceCostLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.serviceAmontlabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 20)
        self.totalLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.totalAmontLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 20)
        
        self.paymentView.isHidden = true
        
        
        self.paymentView.setCornerRadius(16.0)
        self.paymentButton.setCornerRadius(5.0)
        
        self.isVoucherApplied = false
        self.selectedVocher = nil
        self.loadJob()
        
        
        if #available(iOS 15.0, *){
            self.offerTableView.sectionHeaderTopPadding = 0.0
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func workComplateButtonClicked(_ sender: Any) {
        
        self.ratingPopUpMainView.isHidden = false
    }
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ratingCloseButtonClicked(_ sender: Any) {
        self.ratingPopUpMainView.isHidden = true
        
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        self.cancelJob()
    }
    
    @IBAction func ratingsSubmitButtonClicked(_ sender: Any) {
        
        self.ratingPopUpMainView.isHidden = true
        
    }
    
    @IBAction func paymentButtonClickedAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController
        vc?.jobId = self.jobId
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func loadJob()  {
        
        
        ServerSync.shared.getJobById(jobId : self.jobId,data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let data = responseObject?["data"] as? NSDictionary
                let title  = data?.value(forKey: "title") as? String
                let date  = data?.value(forKey: "start_date") as? String
                let time  = data?.value(forKey: "start_time") as? String
                let description  = data?.value(forKey: "description") as? String
                let hours_required  = data?.value(forKey: "hours_required") as? NSNumber
                let job_status  = data?.value(forKey: "job_status") as? String
                
                let service_category = data?.value(forKey: "service_category") as? NSDictionary
                let minimum_price  = service_category?.value(forKey: "minimum_price") as? String
                let hourly_price  = service_category?.value(forKey: "hourly_price") as? String
                let service_type  = service_category?.value(forKey: "service_type") as? String
                let priceMin = Int(minimum_price!)
                let hours = hours_required!.intValue
                let pricePerHour = Int(hourly_price!)
                self.serviceId = service_category?.value(forKey: "id") as? Int
                
                var priceMax = pricePerHour! * hours
                
                if(priceMax<priceMin!)
                {
                    priceMax = priceMin!
                }
                
                
                
                self.taskLabel.text = title
                self.dateLabel.text = self.formatJobDate(date: date!+" "+time!)
                self.payLabel.text = "₹ \(priceMax)"
                
                var workAcccptedTime = ""
                var workInPrpgressTime = ""
                var workFinishedTime = ""
                let job_status_history = data?.value(forKey: "job_status_history") as? Array<NSDictionary>
                for statusObject in job_status_history! {
                    
                    let status = statusObject.value(forKey: "status") as! String
                    if(status == "Work accepted")
                    {
                        let time = statusObject.value(forKey: "created_at") as! String
                        workAcccptedTime = self.formatDate(date: time)
                        
                        let servceId = statusObject.value(forKey: "user_id") as! Int
                        
                        self.serviceProviderId = servceId
                        
                    }
                    else if (status == "Completed")
                    {
                        let time = statusObject.value(forKey: "created_at") as! String
                        workFinishedTime = self.formatDate(date: time)
                        self.paymentView.isHidden = false
                        
                    }
                    else if (status == "In Progress")
                    {
                        let time = statusObject.value(forKey: "created_at") as! String
                        workInPrpgressTime = self.formatDate(date: time)
                        
                    }
                }
                
                self.workAcceptedTimeLabel.text = workAcccptedTime
                self.workFinishedTimeLabel.text = workFinishedTime
                self.inProgressTimeLabel.text = workInPrpgressTime
                
                
                if(job_status == "Work accepted")
                {
                    self.cancelButton.isHidden = false
                    
                    self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                    self.workInProgressStatus.image = UIImage(named: "work_status_pending")
                    self.workCompletedStatus.image = UIImage(named: "work_status_pending")
                    
                    self.workAcceptedLabel.textColor = .black
                    self.inProgressLabel.textColor = "#ABB4C1".getUIColorFromHex()
                    self.workFinixhedLabel.textColor = "#ABB4C1".getUIColorFromHex()
                    
                    
                    
                }
                else if (job_status == "In Progress")
                {
                    self.cancelButton.isHidden = false
                    
                    
                    self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                    self.workInProgressStatus.image = UIImage(named: "work_status_done")
                    self.workCompletedStatus.image = UIImage(named: "work_status_pending")
                    
                    self.workAcceptedLabel.textColor = .black
                    self.inProgressLabel.textColor = .black
                    self.workFinixhedLabel.textColor = "#ABB4C1".getUIColorFromHex()
                }
                else if(job_status == "Completed")
                {
                    self.cancelButton.isHidden = true
                    
                    self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                    self.workInProgressStatus.image = UIImage(named: "work_status_done")
                    self.workCompletedStatus.image = UIImage(named: "work_status_done")
                    
                    self.workAcceptedLabel.textColor = .black
                    self.inProgressLabel.textColor = .black
                    self.workFinixhedLabel.textColor =  .black
                    
                    
                }
                else{
                    
                    self.workAcceptedStatus.image = UIImage(named: "work_status_pending")
                    self.workInProgressStatus.image = UIImage(named: "work_status_pending")
                    self.workCompletedStatus.image = UIImage(named: "work_status_pending")
                    
                    self.workAcceptedLabel.textColor = "#ABB4C1".getUIColorFromHex()
                    self.inProgressLabel.textColor = "#ABB4C1".getUIColorFromHex()
                    self.workFinixhedLabel.textColor = "#ABB4C1".getUIColorFromHex()
                    
                }
                
                let user = data?.value(forKey: "user") as? NSDictionary
                let name = user?.value(forKey: "name") as? String
                let district = user?.value(forKey: "district") as? String
                let area = user?.value(forKey: "area") as? String
                let state = user?.value(forKey: "state") as? String
                let pin = user?.value(forKey: "pin") as? String
                let town = user?.value(forKey: "town") as? String
                let profile_image = user?.value(forKey: "profile_image") as? String
                
                var addresss = ""
                if(area?.count ?? 0>0)
                {
                    addresss = area!+", "
                }
                if(town?.count ?? 0>0)
                {
                    addresss = town!+", "
                }
                if(district?.count ?? 0>0)
                {
                    addresss = district!+", "
                }
                if(state?.count ?? 0>0)
                {
                    addresss = state!+", "
                }
                if(pin?.count ?? 0>0)
                {
                    addresss = "Pin: "+pin!
                }
                
                self.addressLabel.text = addresss
                
                self.getOffers()
                self.getJobPayments()
                if(self.serviceProviderId != nil)
                {
                    self.getServiceProvider()
                }
                
                //                let gst = priceMax*18/100
                //                let convenice = priceMax*10/100
                //
                //                let total = priceMax+gst+convenice
                
                
                
            }
            
            
        }
    }
    
    
    @objc func cancelJob()  {
        
        let params: NSDictionary = ["job_status":"Cancelled"]
        
        
        ServerSync.shared.updateJobStatus(jobId : self.jobId, data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                self.navigationController?.popViewController(animated: true)
                
            }
        }
        
    }
    
    @objc func getOffers()  {
        
        
        
        ServerSync.shared.getOffers(data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = responseObject?.value(forKey: "data")
                
                self.offersArray = dataDict as! NSArray
                self.offerTableView.reloadData()
                
            }
        }
        
        
    }
    
    @objc func getServiceProvider()  {
        
        
        
        ServerSync.shared.getUser(userId: self.serviceProviderId,data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                
                
                
            }
        }
        
        
    }
    
    @objc func getJobPayments()  {
        
        
        
        ServerSync.shared.getJobPayments(jobId: self.jobId,data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                
                let dataDict = responseObject?.value(forKey: "data") as? NSDictionary
                
                // let convenience_fee = dataDict?.value(forKey: "convenience_fee") as! CGFloat
                let job_charges = dataDict?.value(forKey: "job_charges") as! String
                let total = dataDict?.value(forKey: "total") as! String
                //let gst = dataDict?.value(forKey: "gst") as! CGFloat
                
                //                let voucherDict = dataDict?.value(forKey: "voucherData") as? NSDictionary
                //                if (voucherDict != nil)
                //                {
                //                    self.isVoucherApplied = true
                //                    self.selectedVocher = voucherDict?.value(forKey: "code") as? String
                //                }
                
                self.serviceAmontlabel.text = "₹ \(job_charges)"
                self.totalAmontLabel.text =  "₹ \(total)"
                
                self.paymentDict = dataDict!
                self.offerTableView.reloadData()
                
            }
        }
        
        
    }
    
    
    @objc func applyOffer()  {
        
        
        if(self.selectedVocher != nil)
        {
            
            
            let params: NSDictionary = ["voucher": self.selectedVocher]
            
            
            ServerSync.shared.applyOffer(jobId: self.jobId,data: params as [NSObject : AnyObject]) { (responseObject, error) in
                
                if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    let dataDict = responseObject?.value(forKey: "data") as? NSDictionary
                    
                    let job_charges = dataDict?.value(forKey: "job_charges") as! String
                    let total = dataDict?.value(forKey: "total") as! String
                    
                    
                    self.serviceAmontlabel.text = "₹ \(job_charges)"
                    self.totalAmontLabel.text =  "₹ \(total)"
                    self.isVoucherApplied = true
                    
                    
                    self.paymentDict = dataDict!
                    self.offerTableView.reloadData()
                    
                }
            }
            
        }
        else{
            self.alertMessageOk(title: "Voucher", message: "Please select a voucher.")
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var sections = 2
        if (self.offersArray.count>0)
        {
            sections = sections+1
        }
        return sections
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0)
        {
            if (self.offersArray.count>0)
            {
                return self.offersArray.count+1
                
            }
            else{
                return 3
            }
            
        }
        else if (section == 1)
        {
            if (self.offersArray.count>0)
            {
                return 3
            }
            else{
                return 4
                
            }
        }
        else if (section == 2)
        {
            return 4
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0)
        {
            if (self.offersArray.count>0)
            {
                if(indexPath.row == self.offersArray.count)
                {
                    return 45
                    
                }
                else{
                    return 95
                    
                }
                
            }
            else{
                return 40
            }
        }
        else if (indexPath.section == 1)
        {
            if(indexPath.row == 2)
            {
                return 45
            }
            return 40
            
        }
        else if (indexPath.section == 2)
        {
            return 40
            
        }
        
        return 0
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
            
            if (self.offersArray.count>0)
            {
                
                if(indexPath.row == self.offersArray.count)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ApplyButtonTableViewCell", for: indexPath) as! ApplyButtonTableViewCell
                    cell.applyButton.backgroundColor = "#4D7BF3".getUIColorFromHex()
                    cell.applyButton.setTitle("Apply Selected Voucher", for: .normal)
                    cell.applyButton.setTitleColor(.white, for: .normal)
                    cell.applyButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
                    
                    cell.setApplyButtonTapped {
                        
                        self.applyOffer()
                        
                    }
                    
                    return cell
                    
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ApplyOfferTableViewCell", for: indexPath) as! ApplyOfferTableViewCell
                    
                    let dict = self.offersArray[indexPath.row] as! NSDictionary
                    let title = dict.value(forKey: "offer_code") as? String
                    let description = dict.value(forKey: "name") as? String
                    let validity = dict.value(forKey: "validity") as? String
                    let validityString = "Valid till " + self.formatOfferDate(date: validity!)
                    
                    cell.offerTitleLabel.text = title
                    cell.offerDescriptionLabel.text = description
                    cell.offerValidityLabel.text = validityString
                    
                    cell.offerTitleLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 14)
                    cell.offerDescriptionLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                    cell.offerValidityLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
                    let voucher = dict.value(forKey: "offer_code") as? String
                    if(voucher == self.selectedVocher)
                    {
                        cell.bgView.setCornerRadius(5.0)
                        cell.bgView.setBorder(color: "#4BBC71".getUIColorFromHex(), width: 1.0)
                    }
                    else{
                        cell.bgView.setCornerRadius(5.0)
                        cell.bgView.setBorder(color: "#DFDFDF".getUIColorFromHex(), width: 1.0)
                    }
                    
                    return cell
                }
            }
            else{
                if(indexPath.row == 2)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ApplyButtonTableViewCell", for: indexPath) as! ApplyButtonTableViewCell
                    
                    cell.applyButton.backgroundColor = "#EEF0FF".getUIColorFromHex()
                    cell.applyButton.setTitle("Redeem points", for: .normal)
                    cell.applyButton.setTitleColor("#4D7BF3".getUIColorFromHex(), for: .normal)
                    cell.applyButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
                    
                    cell.setApplyButtonTapped {
                        let topViewController = UIApplication.getTopMostViewController()
                        
                        topViewController?.alertMessageOk(title: "Redeem points", message: "You don't have enough points to redeem.")
                        
                    }
                    
                    return cell
                    
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
                    if(indexPath.row == 0)
                    {
                        cell.amountLabel.text = "0"
                        cell.titletextLabel.text = "Total reward points"
                        cell.amountLabel.textColor = "#5B6382".getUIColorFromHex()
                        cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                        cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                        cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                        cell.removeButton.isHidden = true
                        cell.couponLabel.isHidden = true
                    }
                    else{
                        cell.amountLabel.text = "₹ 0.00"
                        cell.titletextLabel.text = "Points worth"
                        cell.amountLabel.textColor = "#4BBC71".getUIColorFromHex()
                        cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                        cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                        cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                        cell.couponLabel.isHidden = true
                        cell.removeButton.isHidden = true
                        
                    }
                    return cell
                }
                
                
            }
            
            
        }
        else if (indexPath.section == 1)
        {
            
            if (self.offersArray.count>0)
            {
                if(indexPath.row == 2)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ApplyButtonTableViewCell", for: indexPath) as! ApplyButtonTableViewCell
                    
                    cell.applyButton.backgroundColor = "#EEF0FF".getUIColorFromHex()
                    cell.applyButton.setTitle("Redeem points", for: .normal)
                    cell.applyButton.setTitleColor("#4D7BF3".getUIColorFromHex(), for: .normal)
                    cell.applyButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
                    
                    cell.setApplyButtonTapped {
                        
                        let topViewController = UIApplication.getTopMostViewController()
                        topViewController?.alertMessageOk(title: "Redeem points", message: "You don't have enough points to redeem.")
                        
                    }
                    
                    return cell
                    
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
                    if(indexPath.row == 0)
                    {
                        cell.amountLabel.text = "0"
                        cell.titletextLabel.text = "Total reward points"
                        cell.amountLabel.textColor = "#5B6382".getUIColorFromHex()
                        cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                        cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                        cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                        cell.couponLabel.isHidden = true
                        cell.removeButton.isHidden = true
                        
                        
                    }
                    else{
                        cell.amountLabel.text = "₹ 0.00"
                        cell.titletextLabel.text = "Points worth"
                        cell.amountLabel.textColor = "#4BBC71".getUIColorFromHex()
                        cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                        cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                        cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                        cell.couponLabel.isHidden = true
                        cell.removeButton.isHidden = true
                        
                    }
                    return cell
                }
            }
            else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
                return cell
            }
            
            
            
            
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
            
            if(indexPath.row == 0)
            {
                
                let convenience_feeAmont = self.paymentDict.value(forKey: "convenience_fee") as? String
                let convenience_fee =  "+ ₹ \(convenience_feeAmont ?? "0.00")"
                
                cell.amountLabel.text = convenience_fee
                cell.titletextLabel.text = "Convenience fee 10%"
                cell.amountLabel.textColor = "#5B6382".getUIColorFromHex()
                cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                cell.couponLabel.isHidden = true
                cell.removeButton.isHidden = true
                
                
            }
            else if(indexPath.row == 1)
            {
                let gstAmont = self.paymentDict.value(forKey: "gst") as? String
                let gst =  "+ ₹ \(gstAmont ?? "0.00")"
                
                cell.amountLabel.text = gst
                cell.titletextLabel.text = "GST 18%"
                cell.amountLabel.textColor = "#5B6382".getUIColorFromHex()
                cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                cell.couponLabel.isHidden = true
                cell.removeButton.isHidden = true
                
            }
            else if(indexPath.row == 2)
            {
                
                let voucher_feeAmont = self.paymentDict.value(forKey: "voucher") as? String
                let voucherAmount =  "- ₹ \(voucher_feeAmont ?? "0.00")"
                
                cell.titletextLabel.text = "Voucher"
                cell.amountLabel.textColor = "#5B6382".getUIColorFromHex()
                cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                cell.couponLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 14)
                cell.couponLabel.textColor = "#FF6D6E".getUIColorFromHex()
                
                if (self.isVoucherApplied == true)
                {
                    cell.removeButton.isHidden = false
                    cell.couponLabel.isHidden = false
                    cell.couponLabel.text = self.selectedVocher
                    
                    cell.amountLabel.text = voucherAmount
                    
                    cell.setRemoveButtonTapped {
                        
                        self.isVoucherApplied = false
                        self.selectedVocher = nil
                        self.getJobPayments()
                    }
                    
                    
                }
                else{
                    cell.removeButton.isHidden = true
                    cell.couponLabel.isHidden = true
                    
                    cell.amountLabel.text = "₹ 0.00"
                    
                }
                
                
                
            }
            else if(indexPath.row == 3)
            {
                cell.amountLabel.text = "- ₹ 0.00"
                cell.titletextLabel.text = "Redeem points worth"
                cell.amountLabel.textColor = "#4BBC71".getUIColorFromHex()
                cell.titletextLabel.textColor = "#5B6382".getUIColorFromHex()
                cell.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
                cell.titletextLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
                cell.removeButton.isHidden = true
                cell.couponLabel.isHidden = true
            }
            
            cell.selectionStyle = .none
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0)
        {
            
            if (self.offersArray.count>0)
            {
                
                if(indexPath.row == self.offersArray.count)
                {
                    
                    
                }
                else
                {
                    let dict = self.offersArray[indexPath.row] as! NSDictionary
                    self.selectedVocher = dict.value(forKey: "offer_code") as? String
                    self.offerTableView.reloadData()
                    
                }
            }
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0
        {
            return 40
        }
        else if section == 1
        {
            if (self.offersArray.count>0)
            {
                return 40
            }
            else{
                return 0
            }
        }
        else{
            
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = .black
        
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40)
        
        
        label.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        label.textColor = "#151A5A".getUIColorFromHex()
        label.textAlignment = .left
        
        
        if section == 0
        {
            if (self.offersArray.count>0)
            {
                
                label.text = "Choose voucher"
                
            }
            else{
                
                label.text = "Redeem reward points"
                
            }
            
            headerView.addSubview(label)
            
            
        }
        else if section == 1
        {
            label.text = "Redeem reward points"
            headerView.addSubview(label)
            
        }
        
        
        
        headerView.backgroundColor = .white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: .leastNonzeroMagnitude))
    }
    
    func formatDate(date: String) -> String {
        let dateObj:Date? = DateFormatter().getDateFrom(stringDate: date, format: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        let formattedDate:String? = DateFormatter().getStringFrom(date: dateObj!, format: "dd MMM YYYY HH:mm a")
        return formattedDate ?? ""
    }
    
    func formatJobDate(date: String) -> String {
        let dateObj:Date? = DateFormatter().getDateFrom(stringDate: date, format: "yyyy-MM-dd HH:mm:ss")
        let formattedDate:String? = DateFormatter().getStringFrom(date: dateObj!, format: "dd MMM YYYY HH:mm a")
        return formattedDate ?? ""
    }
    
    func formatOfferDate(date: String) -> String {
        let dateObj:Date? = DateFormatter().getDateFrom(stringDate: date, format: "dd-MM-yyyy")
        let formattedDate:String? = DateFormatter().getStringFrom(date: dateObj!, format: "dd MMM YYYY")
        return formattedDate ?? ""
    }
    
}
