//
//  CustomerNotificationsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/13/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class CustomerNotificationsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var calenderViewHeight: NSLayoutConstraint!
    @IBOutlet var calenderInnerView: UIView!
    @IBOutlet var calendrLabel: UILabel!
    @IBOutlet var calenderView: UIView!
    @IBOutlet var searchtextField: UITextField!
    @IBOutlet var searchView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var notificationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.notificationTableView.tableFooterView = UIView()
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        self.searchView.setCornerRadius(4.0)
        self.searchtextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerNotificationsTableViewCell", for: indexPath) as! CustomerNotificationsTableViewCell
        
        cell.titleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        cell.dateLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.locationLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.bgView.dropShadow()
        cell.bgView.setBorder(color: "#E9E9E9".getUIColorFromHex(), width: 1.0)
        cell.bgView.setCornerRadius(10.0)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
}


