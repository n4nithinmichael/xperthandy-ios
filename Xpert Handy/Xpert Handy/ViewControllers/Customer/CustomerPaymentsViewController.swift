//
//  CustomerPaymentsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/13/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class CustomerPaymentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var topView: UIView!
    //    @IBOutlet var earnedLabel: UILabel!
    //    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var rewardslabel: UILabel!
    @IBOutlet var pointsLabel: UILabel!
    @IBOutlet var redeemButton: UIButton!
    @IBOutlet var searchView: UIView!
    //  @IBOutlet var linkAccountButton: UIButton!
    @IBOutlet var searchtextField: UITextField!
    // @IBOutlet var calenderButton: UIButton!
    //  @IBOutlet var depositsOnlyLabel: UILabel!
    // @IBOutlet var sortBylabel: UILabel!
    // @IBOutlet var sortByButton: UIButton!
    @IBOutlet var paymentsTableView: UITableView!
    
    var paymentsArray = Array<NSDictionary>()
    var filteredPayments =  Array<NSDictionary>()
    var searchActive : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.paymentsTableView.tableFooterView = UIView()
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        //        self.earnedLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        //        self.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 30)
        self.rewardslabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.pointsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 30)
        self.redeemButton.titleLabel?.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        
        
        //        self.depositsOnlyLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        //        self.sortBylabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        //
        //        self.linkAccountButton?.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        //        self.sortByButton.titleLabel?.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        
        
        self.topView.setCornerRadius(10.0)
        self.redeemButton.setCornerRadius(12.5)
        
        self.searchView.setCornerRadius(5.0)
        //        self.calenderButton.setCornerRadius(5.0)
        //        self.linkAccountButton.setCornerRadius(5.0)
        
        
        self.searchtextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.searchtextField.delegate = self
        self.searchtextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.getJobsPayments()
        
        // Do any additional setup after loading the view.
    }
    
    
    func getJobsPayments()  {
        
        ServerSync.shared.getJobPaymentsPaid(data: nil) { (responseObj, error) in
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                
                
            }
            else {
                
                if(responseObj!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    let payments = responseObj?.value(forKey: "data") as? Array<NSDictionary>
                    self.paymentsArray = payments ?? Array<NSDictionary>()
                    self.paymentsTableView.reloadData()
                }
            }
            
            
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if(searchActive) {
            return filteredPayments.count
        }
        return self.paymentsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentsTableViewCell", for: indexPath) as! PaymentsTableViewCell
        cell.nameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        cell.dateLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.amountLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 16)
        cell.typeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.bgView.dropShadow()
        cell.bgView.setBorder(color: "#E9E9E9".getUIColorFromHex(), width: 1.0)
        cell.bgView.setCornerRadius(10.0)
        
        let paymentDict : NSDictionary
        if(searchActive) {
            paymentDict = self.filteredPayments[indexPath.row]
            
        }
        else{
            paymentDict = self.paymentsArray[indexPath.row] as! NSDictionary
        }
        
        let userDict = paymentDict.value(forKey: "user") as? NSDictionary
        let name = userDict?.value(forKey: "name") as? String
        let payment_type = paymentDict.value(forKey: "payment_type") as? String
        
        let total = paymentDict.value(forKey: "total") as? String
        let date = paymentDict.value(forKey: "payment_date") as? String
        
        cell.amountLabel.text = "₹ \(total ?? "0.00")"
        cell.nameLabel.text = name
        if(date != nil)
        {
            cell.dateLabel.text = self.formatDate(date: date!)
            
        }
        else{
            cell.dateLabel.text = ""
            
        }
        
        if(payment_type == "cash")
        {
            cell.typeLabel.text = "Cash"
            
        }
        else{
            cell.typeLabel.text = "Card"
            
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    @IBAction func redeemButtonClicked(_ sender: Any) {
        
        let topViewController = UIApplication.getTopMostViewController()
        
        topViewController?.alertMessageOk(title: "Redeem points", message: "You don't have enough points to redeem.")
    }
    @IBAction func linkAccontButtonClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankAccountsViewController") as? BankAccountsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func formatDate(date: String) -> String {
        let dateObj:Date? = DateFormatter().getDateFrom(stringDate: date, format: "yyyy-MM-dd")
        let formattedDate:String? = DateFormatter().getStringFrom(date: dateObj!, format: "dd MMM YYYY")
        return formattedDate ?? ""
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        
        
        //  self.searchTextField.resignFirstResponder()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.text!.count>0)
        {
            searchActive = true
        }
        else{
            searchActive = false
            
        }
    }
    
    func textFieldCancelButtonClicked(_ textField: UITextField) {
        searchActive = false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.text!.count>0)
        {
            searchActive = true
        }
        else{
            searchActive = false
            
        }
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let searchText = textField.text ?? ""
        
        
        filteredPayments = self.paymentsArray.filter({ (job) -> Bool in
            
            if(searchText == "")
            {
                return true
            }
            
            let userDict = job.value(forKey: "user") as? NSDictionary
            let tmp = userDict?.value(forKey: "name") as! NSString
            
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        
        searchActive = true;
        
        
        self.paymentsTableView.reloadData()
    }
    
}
