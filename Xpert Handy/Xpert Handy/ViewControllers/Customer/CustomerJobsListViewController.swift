//
//  CustomerJobsListViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/13/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class CustomerJobsListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    @IBOutlet var jobListTableView: UITableView!
    
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var searchView: UIView!
    @IBOutlet var titlelabel: UILabel!
    @IBOutlet var taskscategoryCollectionView: UICollectionView!
    
    @IBOutlet var calenderViewHeight: NSLayoutConstraint!
    @IBOutlet var calenderInnerView: UIView!
    @IBOutlet var calenderView: UIView!
    
    @IBOutlet var dateCollectionView: UICollectionView!
    
    var selectedCategory : Int = 0
    var selecteDateIndex : Int = 0
    var startDate = Date()
    var endDate = Date()
    
    var newCount : Int = 0
    var pendingCount : Int = 0
    var completedCount : Int = 0
    var rejectedCount : Int = 0
    
    var jobsArray = NSArray()
    var jobTypeArray = Array<NSDictionary>()
    var filteredJobs =  Array<NSDictionary>()
    var searchActive : Bool = false
    
    
    var jobStatus = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedCategory = self.jobStatus-1
        
        
        self.jobListTableView.tableFooterView = UIView()
        self.titlelabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        jobListTableView.estimatedRowHeight = 150.0
        jobListTableView.rowHeight = UITableView.automaticDimension
        
        self.searchView.setCornerRadius(4.0)
        self.searchTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.searchTextField.delegate = self
        self.searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.selecteDateIndex = 0
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 120, height: 40)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 5
        layout.scrollDirection = .horizontal
        self.taskscategoryCollectionView!.collectionViewLayout = layout
        
        
        let layout1: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout1.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout1.itemSize = CGSize(width: 50, height: 70)
        layout1.minimumInteritemSpacing = 10
        layout1.minimumLineSpacing = 5
        layout1.scrollDirection = .horizontal
        self.dateCollectionView!.collectionViewLayout = layout1
        
        self.startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        self.endDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())!
        
        
        
        self.dateCollectionView!.reloadData()
        
        
        self.dateCollectionView.showsHorizontalScrollIndicator = false
        self.taskscategoryCollectionView.showsHorizontalScrollIndicator = false
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.searchTextField.text = ""
        
        
        let userTypeId =  UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.UserType)
        
        if(userTypeId == 2)
        {
            self.getJobs()
            
        }
        else{
            self.getMyPosts()
            
        }
        
    }
    
    func getJobs()  {
        
        ServerSync.shared.getJobList(data: nil) { (respose, error) in
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                
                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    let dataDict = respose?.value(forKey: "data")
                    self.jobsArray = dataDict as? NSArray ?? NSArray()
                    self.parseJobs()
                    
                }
            }
            
            
        }
    }
    
    
    func parseJobs()
    {
        self.jobTypeArray.removeAll()
        
        
        newCount  = 0
        pendingCount = 0
        completedCount = 0
        rejectedCount = 0
        
        var jobStatusString = ""
        if(self.jobStatus == 1)
        {
            jobStatusString = "Work accepted"
        }
        else if(self.jobStatus == 2)
        {
            jobStatusString = "In Progress"
        }
        else if(self.jobStatus == 3)
        {
            jobStatusString = "Completed"
        }
        else if(self.jobStatus == 4)
        {
            jobStatusString = "Rejected"
        }
        
        for jobs in jobsArray {
            
            let jobDict = jobs as? NSDictionary
            let status = jobDict?.value(forKey: "job_status") as! String
            if(status == jobStatusString)
            {
                self.jobTypeArray.append(jobDict!)
            }
            
        }
        
        
        for jobs in jobsArray {
            
            let jobDict = jobs as? NSDictionary
            let status = jobDict?.value(forKey: "job_status") as! String
            
            if(status == "Work accepted")
            {
                newCount = newCount+1
            }
            else if(status == "In Progress")
            {
                pendingCount = pendingCount+1
            }
            else if(status == "Completed")
            {
                completedCount = completedCount+1
            }
            else if(status == "Rejected")
            {
                rejectedCount = rejectedCount+1
            }
            
        }
        
        self.jobListTableView!.reloadData()
        self.taskscategoryCollectionView!.reloadData()
        
    }
    
    func getMyPosts()  {
        
        ServerSync.shared.getMyPosts(data: nil) { (respose, error) in
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                
                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    let dataDict = respose?.value(forKey: "data")
                    self.jobsArray = dataDict as? NSArray ?? NSArray()
                    
                    self.parseMyposts()
                }
            }
            
            
        }
    }
    
    func parseMyposts()
    {
        self.jobTypeArray.removeAll()
        
        
        newCount  = 0
        pendingCount = 0
        completedCount = 0
        rejectedCount = 0
        
        if(self.jobStatus == 1)
        {
            for jobs in jobsArray {
                
                let jobDict = jobs as? NSDictionary
                let status = jobDict?.value(forKey: "job_status") as! String
                if(status == "New")
                {
                    self.jobTypeArray.append(jobDict!)
                }
                
            }
            
        }
        else if(self.jobStatus == 2)
        {
            
            for jobs in jobsArray {
                
                let jobDict = jobs as? NSDictionary
                let status = jobDict?.value(forKey: "job_status") as! String
                if(status == "In Progress" || status == "Work accepted")
                {
                    self.jobTypeArray.append(jobDict!)
                }
                
            }
        }
        else if(self.jobStatus == 3)
        {
            
            for jobs in jobsArray {
                
                let jobDict = jobs as? NSDictionary
                let status = jobDict?.value(forKey: "job_status") as! String
                if(status == "Completed")
                {
                    self.jobTypeArray.append(jobDict!)
                }
                
            }
        }
        else if(self.jobStatus == 4)
        {
            
            for jobs in jobsArray {
                
                let jobDict = jobs as? NSDictionary
                let status = jobDict?.value(forKey: "job_status") as! String
                if(status == "Rejected")
                {
                    self.jobTypeArray.append(jobDict!)
                }
                
            }
            
        }
        
        
        
        
        for jobs in jobsArray {
            
            let jobDict = jobs as? NSDictionary
            let status = jobDict?.value(forKey: "job_status") as! String
            
            if(status == "Work accepted" || status == "In Progress")
            {
                pendingCount = pendingCount+1
            }
            else if(status == "New")
            {
                newCount = newCount+1
            }
            else if(status == "Completed")
            {
                completedCount = completedCount+1
            }
            else if(status == "Rejected")
            {
                rejectedCount = rejectedCount+1
            }
            
        }
        
        self.jobListTableView!.reloadData()
        self.taskscategoryCollectionView!.reloadData()
        
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if(searchActive) {
            return filteredJobs.count
        }
        
        return self.jobTypeArray.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskListTableViewCell", for: indexPath) as! TaskListTableViewCell
        cell.bgView.dropShadow()
        cell.bgView.setBorder(color: "#E9E9E9".getUIColorFromHex(), width: 1.0)
        cell.bgView.setCornerRadius(10.0)
        
        cell.titlelabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 16)
        cell.locationLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.daysLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.hoursLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.distanceLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.daysLabel.backgroundColor = "FF6D6E".getUIColorFromHex()
        cell.daysLabel.setCornerRadius(12)
        cell.selectionStyle = .none
        
        
        let jobDict: NSDictionary
        if(searchActive) {
            jobDict = self.filteredJobs[indexPath.row]
        } else {
            jobDict = self.jobTypeArray[indexPath.row]
            
        }
        
        let hours_required = jobDict.value(forKey: "hours_required") as? NSNumber ?? 0
        cell.titlelabel.text = jobDict.value(forKey: "title") as? String
        cell.hoursLabel.text = "\(hours_required) Hours work"
        
        
        
        let service_category = jobDict.value(forKey: "service_category") as? NSDictionary
        let service_type  = service_category?.value(forKey: "service_type") as? String
        
        if(service_type == "In Store")
        {
            cell.instoreImage.isHidden = false
        }
        else{
            cell.instoreImage.isHidden = true
            
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let userType =  UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.UserType)
        
        if userType == 2 {
            
            let jobDict: NSDictionary
            if(searchActive) {
                jobDict = self.filteredJobs[indexPath.row] as! NSDictionary
            } else {
                jobDict = self.jobTypeArray[indexPath.row] as! NSDictionary
                
            }
            let jobId = jobDict.value(forKey: "id") as! Int
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "JobDetailsExpertViewController") as? JobDetailsExpertViewController
            vc?.jobId = jobId
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else{
            
            let jobDict: NSDictionary
            if(searchActive) {
                jobDict = self.filteredJobs[indexPath.row] as! NSDictionary
            } else {
                jobDict = self.jobTypeArray[indexPath.row] as! NSDictionary
                
            }
            let jobId = jobDict.value(forKey: "id") as! Int
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "JobDeatilsCustomerViewController") as? JobDeatilsCustomerViewController
            vc?.jobId = jobId
            
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        
        
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.taskscategoryCollectionView {
            return 4
            
        }
        else{
            
            let daysBetween = Calendar.current.dateComponents([.day], from: self.startDate, to: self.endDate)
            
            return daysBetween.day!
            
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.taskscategoryCollectionView {
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaskCategoryCollectionViewCell", for: indexPath as IndexPath) as! TaskCategoryCollectionViewCell
            
            cell.nameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
            cell.countLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
            
            cell.countLabel.setCornerRadius(10.0)
            
            
            
            if indexPath.row == 0 {
                cell.nameLabel.text = "New"
                cell.countLabel.text = "\(newCount)"
            }
            else if indexPath.row == 1 {
                cell.nameLabel.text = "Pending"
                cell.countLabel.text = "\(pendingCount)"
                
                
            }
            else if indexPath.row == 2 {
                cell.nameLabel.text = "Completed"
                cell.countLabel.text = "\(completedCount)"
                
                
            }
            
            else if indexPath.row == 3 {
                cell.nameLabel.text = "Rejected"
                cell.countLabel.text = "\(rejectedCount)"
                
                
            }
            cell.nameLabel.textColor = "#000000".getUIColorFromHex()
            
            if indexPath.row == self.selectedCategory {
                
                cell.nameLabel.textColor = "#FF6D6E".getUIColorFromHex()
                
            }
            return cell
        }
        else{
            
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaskDateCollectionViewCell", for: indexPath as IndexPath) as! TaskDateCollectionViewCell
            
            cell.setCornerRadius(10)
            cell.setBorder(color: "E6E6EF".getUIColorFromHex(), width: 1.0)
            cell.dayLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)!
            cell.dateLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 18)!
            cell.dayLabel.textColor = "#C3C9D8".getUIColorFromHex()
            cell.dateLabel.textColor = "#151A5A".getUIColorFromHex()
            
            cell.backgroundColor = .white
            cell.bgView.backgroundColor = .white
            
            
            let cellDate = Calendar.current.date(byAdding: .day, value: indexPath.row, to: self.startDate)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd"
            let dateString = dateFormatter.string(from: cellDate)
            dateFormatter.dateFormat = "EEE"
            let dayString = dateFormatter.string(from: cellDate)
            
            cell.dateLabel.text = dateString
            cell.dayLabel.text = dayString
            
            if indexPath.row == self.selecteDateIndex {
                
                cell.setBorder(color: "4D7BF3".getUIColorFromHex(), width: 1.0)
                
                cell.dayLabel.textColor = .white
                cell.dateLabel.textColor = .white
                cell.backgroundColor = "#4D7BF3".getUIColorFromHex()
                cell.bgView.backgroundColor =  "#4D7BF3".getUIColorFromHex()
                
            }
            return cell
        }
        
        
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if collectionView == self.taskscategoryCollectionView {
            return CGSize(width:120, height: 40)
            
        }
        else{
            return CGSize(width:50, height: 70)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == self.taskscategoryCollectionView {
            self.selectedCategory = indexPath.row
            self.taskscategoryCollectionView.reloadData()
            self.jobStatus = indexPath.row+1
            
            
            let userTypeId =  UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.UserType)
            
            if(userTypeId == 2)
            {
                self.parseJobs()
                
            }
            else{
                self.getMyPosts()
                
            }
            
            self.searchTextField.text = ""
            
            
        }
        else{
            self.selecteDateIndex = indexPath.row
            self.dateCollectionView.reloadData()
            
            
        }
        
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        
        
        //  self.searchTextField.resignFirstResponder()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.text!.count>0)
        {
            searchActive = true
        }
        else{
            searchActive = false
            
        }
    }
    
    func textFieldCancelButtonClicked(_ textField: UITextField) {
        searchActive = false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.text!.count>0)
        {
            searchActive = true
        }
        else{
            searchActive = false
            
        }
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let searchText = textField.text ?? ""
        
        
        filteredJobs = self.jobTypeArray.filter({ (job) -> Bool in
            
            if(searchText == "")
            {
                return true
            }
            
            let tmp: NSString = job.value(forKey: "title") as! NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        
        searchActive = true;
        
        
        self.jobListTableView.reloadData()
    }
    
}






