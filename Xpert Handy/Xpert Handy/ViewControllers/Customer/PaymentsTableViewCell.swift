//
//  PaymentsTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/13/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class PaymentsTableViewCell: UITableViewCell {
    
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
