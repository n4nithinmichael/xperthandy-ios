//
//  BankAccountsTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/10/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class BankAccountsTableViewCell: UITableViewCell {
    
    @IBOutlet var primaryAccontlabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var bankNameLabel: UILabel!
    @IBOutlet var accontNumberLabel: UILabel!
    @IBOutlet var setAsPrimaryButton: UIButton!
    @IBOutlet var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
