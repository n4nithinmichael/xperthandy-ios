//
//  CustomerNotificationsTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/13/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class CustomerNotificationsTableViewCell: UITableViewCell {
    @IBOutlet var bgView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
