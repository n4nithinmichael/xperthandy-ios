//
//  OffersTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/17/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class OffersTableViewCell: UITableViewCell {

    @IBOutlet var bgView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var deatilsLabel: UILabel!
    @IBOutlet var codeBgView: UIView!
    @IBOutlet var codemainBgView: UIView!
    @IBOutlet var usecodeLabel: UILabel!
    @IBOutlet var codeLabel: UILabel!
    @IBOutlet var endsInlabel: UILabel!
    
    @IBOutlet var usecodeBgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
