//
//  MyOfferesViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/17/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class MyOfferesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var listTableview: UITableView!
    
    var offersArray = NSArray()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.listTableview.tableFooterView = UIView()
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)

        self.getOffers()

        // Do any additional setup after loading the view.
    }
    
    @objc func getOffers()  {
        
        
        
        ServerSync.shared.getOffers(data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = responseObject?.value(forKey: "data")

                self.offersArray = dataDict as! NSArray
                self.listTableview.reloadData()
                
            }
        }

            
    }
    
    @IBAction func backButtonsPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.offersArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OffersTableViewCell", for: indexPath) as! OffersTableViewCell
        
        cell.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 26)
        cell.deatilsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        cell.usecodeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.codeLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 12)
        cell.endsInlabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)


        cell.bgView.setCornerRadius(10.0)
        cell.usecodeBgView.setCornerRadius(15.0)
        cell.codeBgView.setCornerRadius(15.0)


        let dict = self.offersArray[indexPath.row] as! NSDictionary
        let title = dict.value(forKey: "offer_code") as? String
        let description = dict.value(forKey: "name") as? String
        let validity = dict.value(forKey: "validity") as? String
        let validityString = "Ends " + self.formatOfferDate(date: validity!)
        let voucher = dict.value(forKey: "offer_code") as? String

        cell.titleLabel.text = title
        cell.deatilsLabel.text = description
        cell.endsInlabel.text = validityString
        cell.codeLabel.text = voucher
                    

        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        
    }
    
    

    func formatOfferDate(date: String) -> String {
        let dateObj:Date? = DateFormatter().getDateFrom(stringDate: date, format: "dd-MM-yyyy")
        let formattedDate:String? = DateFormatter().getStringFrom(date: dateObj!, format: "MMM dd  YYYY")
        return formattedDate ?? ""
    }

}

