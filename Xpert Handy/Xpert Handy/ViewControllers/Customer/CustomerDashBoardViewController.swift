//
//  CustomerDashBoardViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 10/18/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import SideMenu

class CustomerDashBoardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var skillsCollectionView: UICollectionView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var postJonButton: UIButton!
    @IBOutlet var jobsButton: UIButton!
    @IBOutlet var paymentsButton: UIButton!
    @IBOutlet var rewardsButton: UIButton!
    
    var serviceArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profileImageView.setCornerRadius(60.0)
        
        self.postJonButton.setCornerRadius(4.0)
        self.searchView.setCornerRadius(4.0)
        self.jobsButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.paymentsButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.rewardsButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        self.postJonButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.searchTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.titleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.nameLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (Constants.ScreenSize.SCREEN_WIDTH-50)/2, height: 66)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .vertical
        self.skillsCollectionView!.collectionViewLayout = layout
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        self.getUser()
        self.loadServieCategory()  
        
    }
    
    @IBAction func jobButtonClicked(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerJobsListViewController") as? CustomerJobsListViewController
        vc!.jobStatus = 1
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func paymentsButtonClicked(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerPaymentsViewController") as? CustomerPaymentsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func rewardsButtonClicked(_ sender: Any) {
        
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "MyOfferesViewController") as? MyOfferesViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func postaJobButtonClicked(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "PostJobSelectCategoryViewController") as? PostJobSelectCategoryViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func getUser()  {
        
        
        
        ServerSync.shared.getUSerDeatails(data: nil) { (respose, error) in
            
            if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = respose?.value(forKey: "data") as? NSDictionary
                let name = dataDict?.value(forKey: "name") as? String
                let profile_image = dataDict?.value(forKey: "profile_image") as? String
                self.nameLabel.text = name
                //self.usertitleLabel.text = ""
                
                self.profileImageView.sd_setImage(with: URL(string: profile_image ?? "" ), placeholderImage: UIImage(named: "customer_avatar"))
            }
        }
    }
    
    func loadServieCategory()  {
        ServerSync.shared.getServiceCategories(data: nil) { (responseObject, error) in
            
            if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = responseObject?.value(forKey: "data")
                self.serviceArray = dataDict as! NSArray
                self.skillsCollectionView!.reloadData()
            }
            
        }
    }
    
    @IBAction func menuclicked(_ sender: Any) {
        
        // Define the menu
        // let menu = SideMenuNavigationController(rootViewController: YourViewController)
        // SideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenu") as! SideMenuNavigationController
        present(menu, animated: true, completion: nil)
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SkillsCollectionViewCell", for: indexPath as IndexPath) as! SkillsCollectionViewCell
        
        let serviceDict = self.serviceArray[indexPath.row] as! NSDictionary
        let color    = serviceDict.value(forKey: "background_colour_code") as! String
        let image    = serviceDict.value(forKey: "image") as! String
        let name    = serviceDict.value(forKey: "name") as! String
        cell.nameLabel.text = name
        
        cell.iconImageView.sd_setImage(with: URL(string: image ), placeholderImage: nil)
        
        
        cell.bgView.backgroundColor = color.getUIColorFromHex()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let serviceDict = self.serviceArray[indexPath.row] as! NSDictionary
        
        let serviceId = serviceDict.value(forKey: "id") as! NSNumber
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "PostJobSelectSubCategoryViewController") as? PostJobSelectSubCategoryViewController
        vc?.serviceId = Int(serviceId)
        vc?.serviceDict = serviceDict
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (Constants.ScreenSize.SCREEN_WIDTH-50)/2, height: 66)
    }
    
}
