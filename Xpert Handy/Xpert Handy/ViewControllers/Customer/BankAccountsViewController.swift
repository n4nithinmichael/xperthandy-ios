//
//  BankAccountsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/10/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class BankAccountsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var accountsTableView: UITableView!
    @IBOutlet var linkAccountButton: UIButton!
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.accountsTableView.tableFooterView = UIView()
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.linkAccountButton.setCornerRadius(5.0)
        self.linkAccountButton?.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankAccountsTableViewCell", for: indexPath) as! BankAccountsTableViewCell
        cell.primaryAccontlabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 20)
        cell.bankNameLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 16)
        cell.accontNumberLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        cell.setAsPrimaryButton?.titleLabel?.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        cell.bgView.dropShadow()
        cell.bgView.setBorder(color: "#E9E9E9".getUIColorFromHex(), width: 1.0)
        cell.bgView.setCornerRadius(10.0)
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    @IBAction func linkAccontButtonClicked(_ sender: Any) {
    }
    
}
