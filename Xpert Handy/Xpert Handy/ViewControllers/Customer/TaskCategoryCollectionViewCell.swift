//
//  TaskCategoryCollectionViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/28/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class TaskCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    
}
