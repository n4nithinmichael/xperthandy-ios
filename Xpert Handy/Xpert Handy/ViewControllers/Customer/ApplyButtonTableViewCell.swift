//
//  ApplyButtonTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 12/29/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class ApplyButtonTableViewCell: UITableViewCell {

    var applyButtonTapped:()->() = {}

    @IBOutlet var applyButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyButton.setCornerRadius(5.0)
        // Initialization code
    }

    @IBAction func applyButtonClicked(_ sender: Any) {
        
        applyButtonTapped()

    }
    
    func setApplyButtonTapped(action:@escaping ()->()){
        applyButtonTapped = action
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
