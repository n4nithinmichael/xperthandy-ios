//
//  PostJobSubCategoryTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/28/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class PostJobSubCategoryTableViewCell: UITableViewCell {
    
    @IBOutlet var checkImage: UIImageView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
