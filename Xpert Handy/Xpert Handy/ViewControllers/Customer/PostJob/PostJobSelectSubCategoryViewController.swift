//
//  PostJobSelectSubCategoryViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/28/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class PostJobSelectSubCategoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var serviceId : Int!
    var selectedSubCategory : Int!
    
    var serviceDict: NSDictionary!
    
    @IBOutlet var jobBgView: UIView!
    @IBOutlet var jobIconView: UIImageView!
    @IBOutlet var jobTitlelabel: UILabel!
    @IBOutlet var slectJobTitleLabel: UILabel!
    @IBOutlet var postButton: UIButton!
    @IBOutlet var postJobTitleLabel: UILabel!
    @IBOutlet var chnageLabel: UILabel!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var listtableView: UITableView!
    var jobsArray = NSArray()
    var selctedSubcat = NSDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let color    = serviceDict.value(forKey: "background_colour_code") as! String
        let image    = serviceDict.value(forKey: "image") as! String
        let name    = serviceDict.value(forKey: "name") as! String
        self.jobTitlelabel.text = name
        
        self.jobIconView.sd_setImage(with: URL(string: image ), placeholderImage: nil)
        
        
        self.jobBgView.backgroundColor = color.getUIColorFromHex()
        
        self.jobTitlelabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.slectJobTitleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.postJobTitleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.postButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.chnageLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.searchTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.searchView.setCornerRadius(4.0)
        self.postButton.setCornerRadius(4.0)
        self.jobBgView.setCornerRadius(4.0)
        
        self.listtableView.tableFooterView = UIView()
        
        
        self.getSubCategories()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func changeJobButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func postButtonAction(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "PostJobViewController") as? PostJobViewController
        vc?.serviceId = self.serviceId
        vc?.serviceDict = self.serviceDict
        vc?.selectedSubCategory = self.selctedSubcat
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    @objc func getSubCategories()  {
        
        
        
        ServerSync.shared.getSubCategories(catId : self.serviceId, data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                self.jobsArray = responseObject!["data"] as? NSArray ?? NSArray()
                
                self.listtableView.reloadData()
                
                
            }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  self.jobsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostJobSubCategoryTableViewCell", for: indexPath) as! PostJobSubCategoryTableViewCell
        
        cell.titleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        cell.bgView.setCornerRadius(10.0)
        
        let color    = serviceDict.value(forKey: "background_colour_code") as! String
        cell.bgView.backgroundColor = color.getUIColorFromHex()
        cell.checkImage.setCornerRadius(13.0)
        cell.checkImage.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
        cell.checkImage.image = nil
        
        if indexPath.row == self.selectedSubCategory {
            cell.checkImage.image = UIImage(named: "gender_selected")
            
        }
        
        cell.selectionStyle = .none
        
        
        let dict = self.jobsArray[indexPath.row] as? NSDictionary
        let name =  dict?.value(forKey: "name") as? String
        cell.titleLabel.text = name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedSubCategory = indexPath.row
        
        self.selctedSubcat = self.jobsArray[indexPath.row] as? NSDictionary ?? NSDictionary()
        
        self.listtableView.reloadData()
        
    }
    
}
