//
//  PostJobViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 2/22/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit
import YYCalendar

class PostJobViewController: UIViewController,UITextFieldDelegate {
    
    var serviceId : Int!
    var serviceDict: NSDictionary!
    var selectedSubCategory: NSDictionary!
    
    @IBOutlet var jobBgView: UIView!
    @IBOutlet var jobIconView: UIImageView!
    @IBOutlet var jobTitlelabel: UILabel!
    @IBOutlet var jobSubBgView: UIView!
    @IBOutlet var jobSubTitlelabel: UILabel!
    @IBOutlet var selectDatetext: UITextField!
    @IBOutlet var selectTimeText: UITextField!
    @IBOutlet var slectJobTitleLabel: UILabel!
    @IBOutlet var jobTitleView: UIView!
    @IBOutlet var jobTitleTextField: UITextField!
    @IBOutlet var jobDescriptionLabel: UILabel!
    @IBOutlet var uploadWorkLabel: UILabel!
    @IBOutlet var uploadImageView: UIView!
    @IBOutlet var takePhotoView: UIView!
    @IBOutlet var uploadPhotoLabel: UILabel!
    @IBOutlet var takePhotolabel: UILabel!
    @IBOutlet var seletTimeLabel: UILabel!
    @IBOutlet var selectTimeView: UIView!
    @IBOutlet var selectDateLabel: UILabel!
    @IBOutlet var selectDateView: UIView!
    @IBOutlet var selectpaylabel: UILabel!
    @IBOutlet var paySlider: UISlider!
    @IBOutlet var postButton: UIButton!
    @IBOutlet var postJobTitleLabel: UILabel!
    @IBOutlet var descriptionView: UIView!
    @IBOutlet var jobTitlabel: UILabel!
    @IBOutlet var maxImageLabel: UILabel!
    @IBOutlet var chnageLabel: UILabel!
    @IBOutlet var chnageSubLabel: UILabel!
    @IBOutlet var selectSublabel: UILabel!
    
    @IBOutlet var serviceTypeLabel: UILabel!
    @IBOutlet var instoreView: UIView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var instoreLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var instoreImageView: UIImageView!
    @IBOutlet var locationImageView: UIImageView!
    
    @IBOutlet var twoHourAmountLabel: UILabel!
    @IBOutlet var twoHourLabel: UILabel!
    @IBOutlet var fourHourLabel: UILabel!
    @IBOutlet var fourHourAmountLabel: UILabel!
    @IBOutlet var sixHourLabel: UILabel!
    @IBOutlet var sixHourAmountLabel: UILabel!
    @IBOutlet var eightHourLabel: UILabel!
    @IBOutlet var eightHourAmountlabel: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    
    var toolBar = UIToolbar()
    var datePicker  = UIDatePicker()
    
    
    let step:Float=2 // If you want UISlider to snap to steps by 10
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let color    = serviceDict.value(forKey: "background_colour_code") as! String
        let image    = serviceDict.value(forKey: "image") as! String
        let name    = serviceDict.value(forKey: "name") as! String
        self.jobTitlelabel.text = name
        let subname    = selectedSubCategory.value(forKey: "name") as? String
        self.jobSubTitlelabel.text = subname
        
        self.jobIconView.sd_setImage(with: URL(string: image ), placeholderImage: nil)
        
        
        self.jobBgView.backgroundColor = color.getUIColorFromHex()
        
        
        self.selectDatetext.inputView = UIView()
        self.selectDatetext.delegate = self
        self.selectTimeText.inputView = UIView()
        self.selectTimeText.delegate = self
        
        
        
        self.jobTitlelabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.jobSubTitlelabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.slectJobTitleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.jobDescriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.uploadPhotoLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.takePhotolabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.seletTimeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.selectDateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.selectpaylabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.jobTitlabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.uploadWorkLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.maxImageLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.chnageLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.chnageSubLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.selectSublabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.serviceTypeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        
        self.instoreLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.locationLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        
        
        self.postJobTitleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        
        self.postButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        self.postButton.setCornerRadius(4.0)
        self.jobBgView.setCornerRadius(4.0)
        self.jobSubBgView.setCornerRadius(4.0)
        
        self.jobTitleView.setCornerRadius(4.0)
        self.uploadImageView.setCornerRadius(4.0)
        self.takePhotoView.setCornerRadius(4.0)
        self.selectDateView.setCornerRadius(4.0)
        self.selectTimeView.setCornerRadius(4.0)
        self.descriptionView.setCornerRadius(4.0)
        self.instoreView.setCornerRadius(4.0)
        self.locationView.setCornerRadius(4.0)
        
        
        self.setBothUnSelected()
        
        
        
        self.twoHourLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.fourHourLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.sixHourLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.eightHourLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.twoHourAmountLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 22)
        self.fourHourAmountLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 22)
        self.eightHourAmountlabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 22)
        self.sixHourAmountLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 22)
        self.twoHourAmountLabel.setCornerRadius(7.0)
        self.fourHourAmountLabel.setCornerRadius(7.0)
        self.eightHourAmountlabel.setCornerRadius(7.0)
        self.sixHourAmountLabel.setCornerRadius(7.0)
        
        self.fourHourAmountLabel.isHidden = true
        self.eightHourAmountlabel.isHidden = true
        self.sixHourAmountLabel.isHidden = true
        
        
        self.paySlider.minimumValue = 2
        self.paySlider.maximumValue = 8
        self.paySlider.isContinuous = true
        self.paySlider.addTarget(self, action: #selector(self.sliderValueDidChange(_:)), for: .valueChanged)
        
        self.paySlider.value = 2
        
        // Do any additional setup after loading the view.
    }
    
    func setInstoreSelected(selected:Bool)  {
        
        self.setBothUnSelected()
        
        if selected {
            
            self.instoreView.backgroundColor = "#4BBC71".getUIColorFromHex()
            self.instoreLabel.textColor = .white
            self.instoreImageView.image = UIImage(named: "gender_selected")
        }
        else{
            self.locationView.backgroundColor = "#4BBC71".getUIColorFromHex()
            self.locationLabel.textColor = .white
            self.locationImageView.image = UIImage(named: "gender_selected")
            
        }
    }
    
    func setBothUnSelected()  {
        self.locationView.backgroundColor = "#EEF1F6".getUIColorFromHex()
        self.instoreView.backgroundColor = "#EEF1F6".getUIColorFromHex()
        self.instoreLabel.textColor = "#5B6382".getUIColorFromHex()
        self.locationLabel.textColor = "#5B6382".getUIColorFromHex()
        self.locationImageView.image = nil
        self.instoreImageView.image = nil
        
        
        
    }
    
    
    @objc func sliderValueDidChange(_ sender:UISlider!)
    {
        print("Slider value changed")
        
        // Use this code below only if you want UISlider to snap to values step by step
        let roundedStepValue = round(sender.value / step) * step
        sender.value = roundedStepValue
        if(roundedStepValue == 2)
        {
            self.twoHourAmountLabel.isHidden = false
            self.fourHourAmountLabel.isHidden = true
            self.sixHourAmountLabel.isHidden = true
            self.eightHourAmountlabel.isHidden = true
            
        }
        else if(roundedStepValue == 4)
        {
            self.twoHourAmountLabel.isHidden = true
            self.fourHourAmountLabel.isHidden = false
            self.sixHourAmountLabel.isHidden = true
            self.eightHourAmountlabel.isHidden = true
        }
        else if(roundedStepValue == 6)
        {
            self.twoHourAmountLabel.isHidden = true
            self.fourHourAmountLabel.isHidden = true
            self.sixHourAmountLabel.isHidden = false
            self.eightHourAmountlabel.isHidden = true
        }
        else if(roundedStepValue == 8)
        {
            self.twoHourAmountLabel.isHidden = true
            self.fourHourAmountLabel.isHidden = true
            self.sixHourAmountLabel.isHidden = true
            self.eightHourAmountlabel.isHidden = false
        }
        print("Slider step value \(Int(roundedStepValue))")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.selectDatetext {
            self.setUpCalenderView()
            
        }
        else if textField == self.selectTimeText
        {
            self.setUptimeView()
        }
        
    }
    func setUptimeView()  {
        
        datePicker.datePickerMode = .time
        datePicker.addTarget(self, action: #selector(dateChange(datePicker:)), for: UIControl.Event.valueChanged)
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
        self.selectTimeText.inputView = datePicker
        self.selectTimeText.text = formatDate(date: Date()) // todays Date
        
        toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height -  260.0, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.isTranslucent = false
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
        toolBar.sizeToFit()
        toolBar.backgroundColor = datePicker.backgroundColor
        self.view.addSubview(toolBar)
    }
    
    @objc func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        self.selectTimeText.resignFirstResponder()
        
    }
    
    @objc func dateChange(datePicker: UIDatePicker)
    {
        self.selectTimeText.text = formatDate(date: datePicker.date)
    }
    
    func formatDate(date: Date) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
    }
    
    func setUpCalenderView()  {
        
        var dateComponent = DateComponents()
        
        
        dateComponent.year = 100
        
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        
        print(futureDate!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let selectedFutureDate = dateFormatter.string(from: futureDate!)
        let selectedCurrentDate = dateFormatter.string(from: Date())
        
        
        
        let calendarView = YYCalendar(limitedCalendarLangType: .ENG2, date: selectedCurrentDate, minDate: selectedCurrentDate, maxDate: selectedFutureDate, format: "yyyy-MM-dd", completion: { (date) in
            
            //            let dateFormatter = DateFormatter()
            //            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            //            dateFormatter.dateFormat = "MM/dd/yyyy"
            //            let selectedDate = dateFormatter.date(from:date)!
            self.selectDatetext.text = date
            
        })
        calendarView.saturdayColor = .black
        calendarView.selectedDayColor = UIColor(red: 0.725, green: 0.576, blue: 0.035, alpha: 1)
        calendarView.show()
        
        
        
    }
    
    
    
    @IBAction func changeJobButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeSubButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func uploadPhotoButtonSelected(_ sender: Any) {
        
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            
        }
    }
    @IBAction func takePhotoButtonSelected(_ sender: Any) {
        
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            
        }
    }
    
    
    @IBAction func instoreButtonSelected(_ sender: Any) {
        
        self.setInstoreSelected(selected: true)
    }
    @IBAction func locationButtonSelected(_ sender: Any) {
        
        self.setInstoreSelected(selected: false)
        
    }
    
    @IBAction func postButtonAction(_ sender: Any) {
        
        if(self.jobTitleTextField.text?.count == 0 )
        {
            self.alertMessageOk(title: "Name.", message: "Please enter job title.")
            
        }
        else if(self.descriptionTextView.text?.count == 0 )
        {
            self.alertMessageOk(title: "Name.", message: "Please enter job description.")
            
        }
        else if(self.selectDatetext.text?.count == 0 )
        {
            self.alertMessageOk(title: "Name.", message: "Please enter job date.")
            
        }
        else if(self.selectTimeText.text?.count == 0 )
        {
            self.alertMessageOk(title: "Name.", message: "Please enter time.")
            
        }
        else{
            let params: NSDictionary = ["service_category_id": self.serviceId!, "title": self.jobTitleTextField.text!,"description": self.descriptionTextView.text!,"start_date": self.selectDatetext.text!,"start_time": self.selectTimeText.text!,"hours_required":self.paySlider.value]
            
            ServerSync.shared.postJob(data: params as [NSObject : AnyObject]) { (responseObject, error) in
                
                
                if let error = error {
                    // got an error in getting the data, need to handle it
                    print(error)
                    
                    self.alertMessageOk(title: "Error", message: error.localizedDescription)
                    
                }
                else {
                    
                    
                    
                    if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                    {
                        self.navigationController?.popToViewController(ofClass: CustomerDashBoardViewController.self)
                        
                    }
                    
                    let topViewController = UIApplication.getTopMostViewController()
                    
                    
                    topViewController?.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                    
                }
                
                
                
            }
            
        }
    }
    
}
