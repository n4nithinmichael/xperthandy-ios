//
//  TaskDateCollectionViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/12/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class TaskDateCollectionViewCell: UICollectionViewCell {
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var bgView: UIView!
    
}
