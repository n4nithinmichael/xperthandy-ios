//
//  PaymentTableViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 12/29/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {
    
    
    var removeButtonTapped:()->() = {}
    
    @IBOutlet var couponLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var titletextLabel: UILabel!
    @IBOutlet var removeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func removeButtonClicked(_ sender: Any) {
        
        removeButtonTapped()
        
    }
    
    
    func setRemoveButtonTapped(action:@escaping ()->()){
        removeButtonTapped = action
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
