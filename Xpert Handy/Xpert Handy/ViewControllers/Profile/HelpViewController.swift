//
//  HelpViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/21/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit
import ExpyTableView

class HelpViewController: UIViewController,ExpyTableViewDataSource, ExpyTableViewDelegate {
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var frequentlyAskedLabel: UILabel!
    @IBOutlet var textsTableView: ExpyTableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.frequentlyAskedLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.searchTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.searchView.setCornerRadius(4.0)
        
        self.textsTableView.rowHeight = UITableView.automaticDimension
        textsTableView.dataSource = self
        textsTableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonsPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return ExpyTableViewDefaultValues.expandableStatus
        
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpHeaderTableViewCell") as! HelpHeaderTableViewCell
        cell.titleLabel.text = "What you need to get started"
        cell.titleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
        if state == ExpyState.didExpand {
            
            
        }
        else if state == ExpyState.didCollapse
        {
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTableViewCell", for: indexPath) as! HelpTableViewCell
        cell.titleLabel.text = "sodales vestibulum urna. Pellentesque leo massa, feugiat quis massa ac, mattis posuere velit. Donec laoreet turpis sapien"
        cell.titleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        cell.selectionStyle = .none
        
        return cell
        
    }
    
}


class HelpHeaderTableViewCell : UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var plusImage: UIImageView!
}

class HelpTableViewCell : UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}
