//
//  ContactUsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/21/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    @IBOutlet var callView: UIView!
    @IBOutlet var callLabel: UILabel!
    @IBOutlet var emailView: UIView!
    @IBOutlet var emaiLabel: UILabel!
    @IBOutlet var sendMessageLabel: UILabel!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var fullnameBgView: UIView!
    @IBOutlet var altNumberBgView: UIView!
    @IBOutlet var emailBgView: UIView!
    @IBOutlet var nametextField: UITextField!
    @IBOutlet var alternateNumber: UITextField!
    @IBOutlet var emailtextField: UITextField!
    @IBOutlet var jobDescriptionLabel: UILabel!
    @IBOutlet var descriptionView: UIView!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        
        self.fullNameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.numberLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.jobDescriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.sendMessageLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        
        self.callLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emaiLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.nextButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        self.nextButton.setCornerRadius(4.0)
        self.fullnameBgView.setCornerRadius(4.0)
        self.altNumberBgView.setCornerRadius(4.0)
        self.emailBgView.setCornerRadius(4.0)
        self.callView.setCornerRadius(4.0)
        self.emailView.setCornerRadius(4.0)
        
        self.descriptionView.setCornerRadius(4.0)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonsPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func nextButtonSelected(_ sender: Any) {
        
    }
    
}
