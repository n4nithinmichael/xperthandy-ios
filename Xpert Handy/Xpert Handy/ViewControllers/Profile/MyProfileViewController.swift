//
//  MyProfileViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 2/27/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController {
    
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var personalDetailsLabel: UILabel!
    @IBOutlet var personalEditButton: UIButton!
    @IBOutlet var fullanameLabel: UILabel!
    @IBOutlet var fullnameTextfield: UITextField!
    @IBOutlet var altNumberlabel: UILabel!
    @IBOutlet var altNumberField: UITextField!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var dobLabel: UILabel!
    @IBOutlet var dobtextField: UITextField!
    @IBOutlet var genderlabel: UILabel!
    @IBOutlet var genderTextField: UITextField!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var addresseditButton: UIButton!
    @IBOutlet var housenameLabel: UILabel!
    @IBOutlet var housenametextfield: UITextField!
    @IBOutlet var areaLabel: UILabel!
    @IBOutlet var areatextField: UITextField!
    @IBOutlet var townlabel: UILabel!
    @IBOutlet var towntextField: UITextField!
    @IBOutlet var districtTextField: UITextField!
    @IBOutlet var districtlabel: UILabel!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var staettextField: UITextField!
    @IBOutlet var pincodeLabel: UILabel!
    @IBOutlet var pincodeTextField: UITextField!
    @IBOutlet var chnagePwlabel: UILabel!
    @IBOutlet var saveButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.personalDetailsLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 16)
        self.addressLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 16)
        self.personalEditButton.titleLabel?.font = UIFont(name: Constants.Font.BOLD_FONT, size: 12)
        self.addresseditButton.titleLabel?.font = UIFont(name: Constants.Font.BOLD_FONT, size: 12)
        self.saveButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.saveButton.setCornerRadius(5)
        
        self.fullanameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.altNumberlabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.dobLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.genderlabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.housenameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.areaLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.townlabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.districtlabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.stateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.pincodeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.chnagePwlabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        
        self.avatarImageView.setCornerRadius(50.0)
        
        self.fullnameTextfield.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.altNumberField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.emailTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.dobtextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.genderTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.housenametextfield.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.areatextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.towntextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.districtTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.staettextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.pincodeTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        self.getUser()
        
        // Do any additional setup after loading the view.
    }
    
    func getUser()  {
        
        
        
        ServerSync.shared.getUSerDeatails(data: nil) { (respose, error) in
            
            
            if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = respose?.value(forKey: "data") as? NSDictionary
                let name = dataDict?.value(forKey: "name") as? String
                let email = dataDict?.value(forKey: "email") as? String
                var dob = dataDict?.value(forKey: "dob") as? String ?? ""
                var phone = dataDict?.value(forKey: "phone") as? String ?? ""
                let gender = dataDict?.value(forKey: "gender") as? String
                
                
                let house_name = dataDict?.value(forKey: "house_name") as? String
                let pin = dataDict?.value(forKey: "pin") as? String
                let area = dataDict?.value(forKey: "area") as? String
                let town = dataDict?.value(forKey: "town") as? String
                let district = dataDict?.value(forKey: "district") as? String
                

                
                if phone.count>10 {
                    
                    phone = String(phone.dropFirst(2))
                }
                
                if dob.count > 10 {
                    dob = String(dob.prefix(10))
                    
                }
                
                let profile_image = dataDict?.value(forKey: "profile_image") as? String
                self.fullnameTextfield.text = name
                self.emailTextField.text = email
                self.dobtextField.text = dob
                self.genderTextField.text = gender ?? ""
                self.altNumberField.text = phone
                
                self.housenametextfield.text = house_name
                self.pincodeTextField.text = pin
                self.areatextField.text = area
                self.towntextField.text = town
                self.districtTextField.text = district
                self.staettextField.text = "Kerala"
                
                self.avatarImageView.sd_setImage(with: URL(string: profile_image ?? "" ), placeholderImage: UIImage(named: "customer_avatar"))
            }
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func chanegProfile(_ sender: Any) {
        
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            self.avatarImageView.image = image
            
            ServerSync.shared.uploadImage(image: image, fileName: "") { (responseObject, error) in
                
            }
            
        }
    }
    @IBAction func chnagePwButtonClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateNewPasswordViewController") as? CreateNewPasswordViewController
        vc?.setNewPw = true
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    @IBAction func peersonalEditButtonClicked(_ sender: Any) {
    }
    @IBAction func savebuttonClicked(_ sender: Any) {
    }

}
