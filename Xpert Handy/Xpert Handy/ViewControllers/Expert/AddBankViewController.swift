//
//  AddBankViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 1/3/22.
//  Copyright © 2022 Nithin Michael. All rights reserved.
//

import UIKit

class AddBankViewController: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var linkAccountButton: UIButton!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nameBgView: UIView!
    @IBOutlet var nametextField: UITextField!
    @IBOutlet var banknameLabel: UILabel!
    @IBOutlet var banknameBgView: UIView!
    @IBOutlet var banknametextField: UITextField!
    @IBOutlet var ifscLabel: UILabel!
    @IBOutlet var ifscBgView: UIView!
    @IBOutlet var ifsctextField: UITextField!
    @IBOutlet var accNoLabel: UILabel!
    @IBOutlet var accNoBgView: UIView!
    @IBOutlet var accNoTextField: UITextField!
    @IBOutlet var confrimAccNoLabel: UILabel!
    @IBOutlet var confrimAccNoBgView: UIView!
    @IBOutlet var confrimAccNoTextField: UITextField!
    @IBOutlet var branchNameLabel: UILabel!
    @IBOutlet var branchNameBgView: UIView!
    @IBOutlet var branchNameTextField: UITextField!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var cityBgView: UIView!
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var stateBgView: UIView!
    @IBOutlet var stateTextField: UITextField!
    @IBOutlet var pincodeLabel: UILabel!
    @IBOutlet var pincodeBgView: UIView!
    @IBOutlet var pincodeTextField: UITextField!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var addressBgView: UIView!
    @IBOutlet var addressTextField: UITextField!
    @IBOutlet var accDtlsLabel: UILabel!
    @IBOutlet var termsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.accDtlsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.nameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.banknameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.ifscLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.accNoLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.confrimAccNoLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.branchNameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.cityLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.stateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.pincodeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.addressLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.termsLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.nametextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.banknametextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.ifsctextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.accNoTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.confrimAccNoTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.branchNameTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.cityTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.stateTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.pincodeTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.addressTextField.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        
        self.nameBgView.setCornerRadius(5.0)
        self.banknameBgView.setCornerRadius(5.0)
        self.ifscBgView.setCornerRadius(5.0)
        self.accNoBgView.setCornerRadius(5.0)
        self.confrimAccNoBgView.setCornerRadius(5.0)
        self.branchNameBgView.setCornerRadius(5.0)
        self.cityBgView.setCornerRadius(5.0)
        self.stateBgView.setCornerRadius(5.0)
        self.pincodeBgView.setCornerRadius(5.0)
        self.addressBgView.setCornerRadius(5.0)
        
        self.linkAccountButton.setCornerRadius(5.0)
        self.linkAccountButton?.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func linkAccontButtonClicked(_ sender: Any) {
        
        if(self.nametextField.text?.count == 0 )
        {
            self.alertMessageOk(title: "Name", message: "Please enter a valid name.")
            
        }
        else if (self.banknametextField.text?.count == 0)
        {
            self.alertMessageOk(title: "Bank", message: "Please enter a valid bank name.")
            
        }
        else if (self.ifsctextField.text?.count == 0)
        {
            self.alertMessageOk(title: "IFSC", message: "Please enter ifsc code.")
            
        }
        else if (self.accNoTextField.text?.count == 0)
        {
            self.alertMessageOk(title: "Accont number", message: "Please enter the account number.")
            
        }
        else if (self.confrimAccNoTextField.text?.count == 0)
        {
            self.alertMessageOk(title: "Confirm account number", message: "Please confirm account number.")
            
        }
        else if (self.accNoTextField.text != self.confrimAccNoTextField.text)
        {
            self.alertMessageOk(title: "Confirm account number", message: "Please confirm account number.")
            
        }
        else if (self.branchNameTextField.text?.count == 0)
        {
            self.alertMessageOk(title: "Branch name", message: "Please enter branch name.")
            
        }
        else if (self.cityTextField.text?.count == 0)
        {
            self.alertMessageOk(title: "City", message: "Please enter city")
            
        }
        else if (self.stateTextField.text?.count == 0)
        {
            self.alertMessageOk(title: "State", message: "Please enter state")
            
        }
        else if (self.pincodeTextField.text?.count == 0)
        {
            self.alertMessageOk(title: "Pincode", message: "Please enter pincode")
            
        }
        else{
            
            self.addBank()
        }
    }
    
    
    @objc func addBank()  {
        
        let params: NSDictionary = ["account_name":self.nametextField.text!,"account_number":self.accNoTextField.text!,"bank_name":self.banknametextField.text!,"branch_name":self.branchNameTextField.text!,"branch_address":self.addressTextField.text!,"branch_city":self.cityTextField.text!,"branch_state":self.stateTextField.text!,"branch_pin":self.pincodeTextField.text!]
        
        
        ServerSync.shared.saveBankAccont(data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                
                
                
                if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
                let topViewController = UIApplication.getTopMostViewController()
                
                
                topViewController?.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                
                
            }
            
        }
        
    }
    
}
