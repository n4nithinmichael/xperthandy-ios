//
//  ServiceProviderDashBoardViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/15/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import SideMenu

class ServiceProviderDashBoardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var titleLabael: UILabel!
    @IBOutlet var taskCountLabel: UILabel!
    @IBOutlet var taskButton: UIButton!
    @IBOutlet var completedCountLabel: UILabel!
    @IBOutlet var complatedButton: UIButton!
    @IBOutlet var pendingButton: UIButton!
    @IBOutlet var pendingCountlabel: UILabel!
    var jobsArray = NSArray()
    var categoryArray = Array<Int>()
    
    @IBOutlet var notificationsLabel: UILabel!
    
    @IBOutlet var notificationTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabael.text = ""
        
        self.profileImageView.setCornerRadius(60.0)
        
        self.pendingButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.complatedButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.taskButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        self.completedCountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 20)
        self.pendingCountlabel.font  = UIFont(name: Constants.Font.MEDIUM_FONT, size: 20)
        self.taskCountLabel.font  = UIFont(name: Constants.Font.MEDIUM_FONT, size: 20)
        
        self.nameLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.titleLabael.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.notificationsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        self.getUser()
        self.getJobsNotification()
        self.getJobs()
    }
    
    
    func getJobs()  {
        
        ServerSync.shared.getJobList(data: nil) { (respose, error) in
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                
                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    let dataDict = respose?.value(forKey: "data")
                    let jobs = dataDict as? NSArray ?? NSArray()
                    
                    var complatedCount = 0
                    var inProgressCount = 0
                    var acceptedCount = 0
                    
                    for job in jobs {
                        
                        let jobDict = job as? NSDictionary
                        let status = jobDict?.value(forKey: "job_status") as? String
                        
                        if(status == "Work accepted")
                        {
                            acceptedCount = acceptedCount+1
                        }
                        else if (status == "Completed")
                        {
                            complatedCount = complatedCount+1
                            
                        }
                        else if (status == "In Progress")
                        {
                            inProgressCount = inProgressCount+1
                        }
                    }
                    
                    self.taskCountLabel.text = "\(acceptedCount)"
                    self.completedCountLabel.text = "\(complatedCount)"
                    self.pendingCountlabel.text = "\(inProgressCount)"
                    
                    
                }
                
            }
            
            
        }
    }
    
    
    func getServiceProvider()  {
        
        ServerSync.shared.getServiceProvider(data: nil) { (respose, error) in
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                
                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    let dataDict = respose?.value(forKey: "data")
                    
                }
            }
            
            
        }
    }
    
    
    func getUser()  {
        
        
        ServerSync.shared.getUSerDeatails(data: nil) { (respose, error) in
            
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                
                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    let dataDict = respose?.value(forKey: "data") as? NSDictionary
                    let name = dataDict?.value(forKey: "name") as? String
                    let profile_image = dataDict?.value(forKey: "profile_image") as? String
                    self.nameLabel.text = name
                    //self.titleLabael.text = ""
                    self.categoryArray = dataDict?.value(forKey: "category") as! Array<Int>
                    self.profileImageView.sd_setImage(with: URL(string: profile_image ?? "" ), placeholderImage: UIImage(named: "customer_avatar"))
                    self.loadServieCategory()
                    
                }
                
                
            }
            
            
        }
    }
    
    func loadServieCategory()  {
        ServerSync.shared.getServiceCategories(data: nil) { (responseObject, error) in
            
            if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                var catArray =  Array<NSDictionary> ()
                
                let dataDict = responseObject?.value(forKey: "data")
                let serviceArray = dataDict as! Array<NSDictionary>
                
                for category in serviceArray
                {
                    let catId = category.value(forKey: "id") as! Int
                    if (self.categoryArray.contains(catId))
                    {
                        catArray.append(category)
                    }
                }
                
                var occupation = ""
                //                for category in catArray
                //                {
                //                    let catname = category.value(forKey: "name") as! String
                //                    occupation = occupation+catname
                //                }
                
                if(catArray.count == 1)
                {
                    let catname = catArray[0].value(forKey: "name") as! String
                    occupation = catname
                }
                else if(catArray.count > 1)
                {
                    let frstcatname = catArray[0].value(forKey: "name") as! String
                    let secondname = catArray[1].value(forKey: "name") as! String
                    
                    occupation = frstcatname+" and "+secondname
                }
                self.titleLabael.text = occupation
                
            }
            
        }
    }
    
    //    func getJobs()  {
    //
    //        ServerSync.shared.getJobList(data: nil) { (respose, error) in
    //
    //
    //
    //            if let error = error {
    //                // got an error in getting the data, need to handle it
    //
    //
    //            }
    //            else {
    //
    //                if(respose!["statusCode"] as? Int == Constants.ResponseStatus.Success)
    //                {
    //
    //
    //                    let dataDict = respose?.value(forKey: "data")
    //                    self.jobsArray = dataDict as! NSArray
    //                    self.notificationTableView!.reloadData()
    //
    //                }
    //            }
    //
    //
    //        }
    //    }
    
    func getJobsNotification()  {
        
        ServerSync.shared.getJobNotification(data: nil) { (respose, error) in
            
            
            
            if let error = error {
                // got an error in getting the data, need to handle it
                
                
            }
            else {
                
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    let dataDict = respose?.value(forKey: "data")
                    self.jobsArray = dataDict as? NSArray ?? NSArray()
                    self.notificationTableView!.reloadData()
                    
                }
            }
            
            
        }
    }
    
    func refresh ()  {
        
        self.getJobsNotification()
        self.notificationTableView.setContentOffset(.zero, animated: true)
        
    }
    
    @IBAction func menuclicked(_ sender: Any) {
        
        // Define the menu
        // let menu = SideMenuNavigationController(rootViewController: YourViewController)
        // SideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenu") as! SideMenuNavigationController
        present(menu, animated: true, completion: nil)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.jobsArray.count+1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row == self.jobsArray.count)
        {
            return 40
            
        }
        return 250
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == self.jobsArray.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ApplyButtonTableViewCell", for: indexPath) as! ApplyButtonTableViewCell
            cell.applyButton.backgroundColor = "#4D7BF3".getUIColorFromHex()
            cell.applyButton.setTitle("Refresh", for: .normal)
            cell.applyButton.setTitleColor(.white, for: .normal)
            cell.applyButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
            
            cell.setApplyButtonTapped {
                
                self.refresh()
                
            }
            
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SPDashboardTableViewCell", for: indexPath) as! SPDashboardTableViewCell
            
            let jobDict = self.jobsArray[indexPath.row] as! NSDictionary
            
            let hours_required = jobDict.value(forKey: "hours_required") as? NSNumber ?? 0
            cell.titleLabel.text = jobDict.value(forKey: "title") as? String
            cell.descriptionLabel.text = jobDict.value(forKey: "description") as? String
            cell.timeLabel.text = "\(hours_required) Hours work"
            cell.dateLabel.text = jobDict.value(forKey: "start_time") as? String
            
            
            
            let service_category = jobDict.value(forKey: "service_category") as? NSDictionary
            let minimum_price  = service_category?.value(forKey: "minimum_price") as? String
            let hourly_price  = service_category?.value(forKey: "hourly_price") as? String
            let service_type  = service_category?.value(forKey: "service_type") as? String
            let priceMin = Int(minimum_price!)
            let hours = hours_required.intValue
            let pricePerHour = Int(hourly_price!)
            
            
            
            var priceMax = pricePerHour! * hours
            
            if(priceMax<priceMin!)
            {
                priceMax = priceMin!
            }
            
            cell.amoundLabel.text = "Pay Rs \(priceMax)"
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == self.jobsArray.count)
        {
            
            
        }
        else{
            
            let jobDict = self.jobsArray[indexPath.row] as! NSDictionary
            let jobId = jobDict.value(forKey: "id") as! Int
            if let topVC = UIApplication.getTopMostViewController() {
                let job_status  = jobDict.value(forKey: "job_status") as? String
                
                
                
                let mainStoryboard = UIStoryboard(name: "Main" , bundle: nil)
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "SPJobNotificationDetailsViewController") as? SPJobNotificationDetailsViewController
                vc?.jobId = jobId
                topVC.navigationController?.pushViewController(vc!, animated: true)
                
            }
        }
        
        
        
        
        
    }
    
    @IBAction func tasksButtonClicked(_ sender: Any) {
        
        let topViewController = UIApplication.getTopMostViewController()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerJobsListViewController") as? CustomerJobsListViewController
        vc!.jobStatus = 1
        topViewController?.navigationController?.pushViewController(vc!, animated: true)
        
    }
    @IBAction func completedButtonClicked(_ sender: Any) {
        let topViewController = UIApplication.getTopMostViewController()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerJobsListViewController") as? CustomerJobsListViewController
        vc!.jobStatus = 3
        topViewController?.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    @IBAction func pendingButtonClicked(_ sender: Any) {
        
        let topViewController = UIApplication.getTopMostViewController()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerJobsListViewController") as? CustomerJobsListViewController
        vc!.jobStatus = 2
        topViewController?.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
}
