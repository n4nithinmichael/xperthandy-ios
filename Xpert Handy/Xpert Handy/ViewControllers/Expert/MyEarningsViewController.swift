//
//  MyEarningsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/10/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class MyEarningsViewController: UIViewController {
    
    @IBOutlet var titlelabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var dateView: UIView!
    @IBOutlet var dateDetailsLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titlelabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.dateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.dateDetailsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.amountLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        
        self.dateView.setCornerRadius(5.0)
        
        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
