//
//  JObDetailsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/3/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit
import Switches

class JobDetailsExpertViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var taskTitlelabel: UILabel!
    @IBOutlet var taskLabel: UILabel!
    @IBOutlet var expertTitleLabel: UILabel!
    @IBOutlet var expertLabel: UILabel!
    @IBOutlet var dateTitleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var payLabel: UILabel!
    @IBOutlet var payTitleLabel: UILabel!
    @IBOutlet var addressTitleLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var jobStatusView: UIView!
    @IBOutlet var jobStatusLabel: UILabel!
    @IBOutlet var workAcceptedLabel: UILabel!
    @IBOutlet var workAcceptedTimeLabel: UILabel!
    @IBOutlet var inProgressLabel: UILabel!
    @IBOutlet var inProgressTimeLabel: UILabel!
    @IBOutlet var workFinixhedLabel: UILabel!
    @IBOutlet var workFinishedTimeLabel: UILabel!
    @IBOutlet var yourRatingsLabel: UILabel!
    @IBOutlet var workFinishedView: UIView!
    
    
    @IBOutlet var popuuptaskTitlelabel: UILabel!
    @IBOutlet var popuuptaskLabel: UILabel!
    @IBOutlet var popuupdateTitleLabel: UILabel!
    @IBOutlet var popuupdateLabel: UILabel!
    @IBOutlet var popuuppayLabel: UILabel!
    @IBOutlet var popuuppayTitleLabel: UILabel!
    @IBOutlet var popuupaddressTitleLabel: UILabel!
    @IBOutlet var popuupaddressLabel: UILabel!
    @IBOutlet var popUpStartButton: UIButton!
    @IBOutlet var popupMainView: UIView!
    @IBOutlet var popupView: UIView!
    @IBOutlet var workAcceptedStatus: UIImageView!
    @IBOutlet var workInProgressStatus: UIImageView!
    @IBOutlet var workCompletedStatus: UIImageView!
    
    @IBOutlet var shortnameLabel: UILabel!
    @IBOutlet var iconView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var workCompleteButton: UIButton!
    @IBOutlet var namelabel: UILabel!
    @IBOutlet var instoreImage: UIImageView!
    @IBOutlet var cancelButton: UIButton!
    var jobId = Int()
    
    @IBOutlet var yourratingView: UIView!
    @IBOutlet var typeOfServiceLabel: UILabel!
    
    @IBOutlet weak var satrtWork: YapSwitch! {
        didSet {
            
            satrtWork.shape = .rounded
            satrtWork.thumbCornerRadius = 0
            satrtWork.offText = "Swipe to start work"
            satrtWork.onText = "Work started"
            satrtWork.onTextColor = .white
            satrtWork.offTextColor = .white
            satrtWork.onTintColor = "#4BBC71".getUIColorFromHex()
            satrtWork.offTintColor = "#4BBC71".getUIColorFromHex()
            satrtWork.onThumbImage = UIImage(named: "work_start_switch_off")
            satrtWork.offThumbImage = UIImage(named: "work_start_switch_off")
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.taskTitlelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.dateTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.payTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.addressTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.workFinishedTimeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.inProgressTimeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.workAcceptedTimeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        //self.paymethodLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.typeOfServiceLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        
        
        self.taskLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.dateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.payLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.addressLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.workAcceptedLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.inProgressLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.workFinixhedLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        // self.paidLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        
        
        self.jobStatusLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.yourRatingsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        //self.viewDetailsButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        self.jobStatusView.roundCorners(corners: [.topLeft,.topRight], size: 27)
        
        
        self.popuuptaskTitlelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.popuupdateTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.popuuppayTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.popuupaddressTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        
        self.workCompleteButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.cancelButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        
        self.popuuptaskLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.popuupdateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.popuuppayLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.popuupaddressLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        self.popUpStartButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.popUpStartButton.setCornerRadius(5.0)
        
        self.popupView.setCornerRadius(5.0)
        self.workCompleteButton.setCornerRadius(5.0)
        self.cancelButton.setCornerRadius(5.0)
        
        
        self.yourratingView.setCornerRadius(5.0)
        
        self.iconView.setCornerRadius(30.0)
        self.namelabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 16)
        self.shortnameLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 20)
        
        
        self.workCompleteButton.isHidden = true
        self.satrtWork.isHidden = true
        
        self.taskLabel.text = ""
        self.dateLabel.text = ""
        self.payLabel.text = ""
        
        self.workAcceptedTimeLabel.text = ""
        self.workFinishedTimeLabel.text = ""
        self.inProgressTimeLabel.text = ""
        
        self.workAcceptedStatus.image = UIImage(named: "work_status_pending")
        self.workInProgressStatus.image = UIImage(named: "work_status_pending")
        self.workCompletedStatus.image = UIImage(named: "work_status_pending")
        
        self.workAcceptedLabel.textColor = "#ABB4C1".getUIColorFromHex()
        
        self.inProgressLabel.textColor = "#ABB4C1".getUIColorFromHex()
        self.workFinixhedLabel.textColor = "#ABB4C1".getUIColorFromHex()
        
        self.addressLabel.text = ""
        
        //        let labelTapGesture = UITapGestureRecognizer(target:self,action:#selector(self.startJob))
        //        satrtWork.addGestureRecognizer(labelTapGesture)
        satrtWork.addTarget(self, action: #selector(self.startJob), for: [.touchDown, .touchDragEnter])
        
        
        self.loadJob()
        
    }
    
    
    
    func loadJob()  {
        
        
        ServerSync.shared.getJobById(jobId : self.jobId,data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let data = responseObject?["data"] as? NSDictionary
                let title  = data?.value(forKey: "title") as? String
                let date  = data?.value(forKey: "start_date") as? String
                let time  = data?.value(forKey: "start_time") as? String
                let description  = data?.value(forKey: "description") as? String
                let hours_required  = data?.value(forKey: "hours_required") as? NSNumber
                let job_status  = data?.value(forKey: "job_status") as? String
                
                let service_category = data?.value(forKey: "service_category") as? NSDictionary
                let minimum_price  = service_category?.value(forKey: "minimum_price") as? String
                let hourly_price  = service_category?.value(forKey: "hourly_price") as? String
                let service_type  = service_category?.value(forKey: "service_type") as? String
                let priceMin = Int(minimum_price!)
                let hours = hours_required!.intValue
                let pricePerHour = Int(hourly_price!)
                
                
                
                var priceMax = pricePerHour! * hours
                
                if(priceMax<priceMin!)
                {
                    priceMax = priceMin!
                }
                
                
                
                self.taskLabel.text = title
                self.dateLabel.text = self.formatJobDate(date: date!+" "+time!)
                self.payLabel.text = "₹ \(priceMax)"
                
                var workAcccptedTime = ""
                var workInPrpgressTime = ""
                var workFinishedTime = ""
                let job_status_history = data?.value(forKey: "job_status_history") as? Array<NSDictionary>
                for statusObject in job_status_history! {
                    
                    let status = statusObject.value(forKey: "status") as! String
                    if(status == "Work accepted")
                    {
                        let time = statusObject.value(forKey: "created_at") as! String
                        workAcccptedTime = self.formatDate(date: time)
                    }
                    else if (status == "Completed")
                    {
                        let time = statusObject.value(forKey: "created_at") as! String
                        workFinishedTime = self.formatDate(date: time)
                        
                    }
                    else if (status == "In Progress")
                    {
                        let time = statusObject.value(forKey: "created_at") as! String
                        workInPrpgressTime = self.formatDate(date: time)
                        
                    }
                }
                
                self.workAcceptedTimeLabel.text = workAcccptedTime
                self.workFinishedTimeLabel.text = workFinishedTime
                self.inProgressTimeLabel.text = workInPrpgressTime
                
                if(service_type == "In Store")
                {
                    self.instoreImage.isHidden = false
                }
                else{
                    self.instoreImage.isHidden = true
                    
                }
                if(job_status == "Work accepted")
                {
                    self.yourratingView.isHidden = true
                    self.workCompleteButton.isHidden = true
                    self.satrtWork.isHidden = false
                    self.cancelButton.isHidden = false
                    
                    self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                    self.workInProgressStatus.image = UIImage(named: "work_status_pending")
                    self.workCompletedStatus.image = UIImage(named: "work_status_pending")
                    
                    self.workAcceptedLabel.textColor = .black
                    self.inProgressLabel.textColor = "#ABB4C1".getUIColorFromHex()
                    self.workFinixhedLabel.textColor = "#ABB4C1".getUIColorFromHex()
                    
                }
                else if (job_status == "In Progress")
                {
                    self.yourratingView.isHidden = true
                    self.workCompleteButton.isHidden = false
                    self.satrtWork.isHidden = true
                    self.cancelButton.isHidden = false
                    
                    
                    self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                    self.workInProgressStatus.image = UIImage(named: "work_status_done")
                    self.workCompletedStatus.image = UIImage(named: "work_status_pending")
                    
                    self.workAcceptedLabel.textColor = .black
                    self.inProgressLabel.textColor = .black
                    self.workFinixhedLabel.textColor = "#ABB4C1".getUIColorFromHex()
                }
                else if(job_status == "Completed")
                {
                    self.yourratingView.isHidden = false
                    self.workCompleteButton.isHidden = true
                    self.satrtWork.isHidden = true
                    self.cancelButton.isHidden = true
                    
                    self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                    self.workInProgressStatus.image = UIImage(named: "work_status_done")
                    self.workCompletedStatus.image = UIImage(named: "work_status_done")
                    
                    self.workAcceptedLabel.textColor = .black
                    self.inProgressLabel.textColor = .black
                    self.workFinixhedLabel.textColor =  .black
                }
                else{
                    
                }
                
                let user = data?.value(forKey: "user") as? NSDictionary
                let name = user?.value(forKey: "name") as? String
                let district = user?.value(forKey: "district") as? String
                let area = user?.value(forKey: "area") as? String
                let state = user?.value(forKey: "state") as? String
                let pin = user?.value(forKey: "pin") as? String
                let town = user?.value(forKey: "town") as? String
                let profile_image = user?.value(forKey: "profile_image") as? String
                
                var addresss = ""
                if(area?.count ?? 0>0)
                {
                    addresss = area!+", "
                }
                if(town?.count ?? 0>0)
                {
                    addresss = town!+", "
                }
                if(district?.count ?? 0>0)
                {
                    addresss = district!+", "
                }
                if(state?.count ?? 0>0)
                {
                    addresss = state!+", "
                }
                if(pin?.count ?? 0>0)
                {
                    addresss = "Pin: "+pin!
                }
                
                self.addressLabel.text = addresss
                self.namelabel.text = name
                self.shortnameLabel.text = name?.initials
                
                
            }
            
            
        }
    }
    
    @objc func startJob()  {
        
        let params: NSDictionary = ["job_status":"In Progress"]
        
        
        ServerSync.shared.updateJobStatus(jobId : self.jobId, data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                self.yourratingView.isHidden = true
                self.workCompleteButton.isHidden = false
                self.satrtWork.isHidden = true
                self.cancelButton.isHidden = false
                
                
                self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                self.workInProgressStatus.image = UIImage(named: "work_status_done")
                self.workCompletedStatus.image = UIImage(named: "work_status_pending")
                
                self.workAcceptedLabel.textColor = .black
                self.inProgressLabel.textColor = .black
                self.workFinixhedLabel.textColor = "#ABB4C1".getUIColorFromHex()
                
            }
        }
        
    }
    
    @objc func cancelJob()  {
        
        let params: NSDictionary = ["job_status":"Cancelled"]
        
        
        ServerSync.shared.updateJobStatus(jobId : self.jobId, data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                self.navigationController?.popViewController(animated: true)
                
            }
        }
        
    }
    
    @objc func completeJob()  {
        
        let params: NSDictionary = ["job_status":"Completed"]
        
        
        ServerSync.shared.updateJobStatus(jobId : self.jobId, data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                self.yourratingView.isHidden = false
                self.workCompleteButton.isHidden = true
                self.satrtWork.isHidden = true
                self.cancelButton.isHidden = true
                
                
                self.workAcceptedStatus.image = UIImage(named: "work_status_done")
                self.workInProgressStatus.image = UIImage(named: "work_status_done")
                self.workCompletedStatus.image = UIImage(named: "work_status_done")
                self.workAcceptedLabel.textColor = .black
                self.inProgressLabel.textColor = .black
                self.workFinixhedLabel.textColor = .black
                
            }
        }
        
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        self.cancelJob()
    }
    @IBAction func workCompleteButtonAction(_ sender: Any) {
        
        self.completeJob()
    }
    
    @IBAction func paynowButtonAction(_ sender: Any) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func viewDetailsButtonClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEarningsViewController") as? MyEarningsViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func showStartPopupButtonClicked(_ sender: Any) {
        
        self.popupMainView.isHidden = false
    }
    
    @IBAction func startWorkButtonClicked(_ sender: Any) {
        
        self.popupMainView.isHidden =  true
    }
    
    @IBAction func closePopupButtonClicked(_ sender: Any) {
        
        self.popupMainView.isHidden =  true
    }
    
    func formatDate(date: String) -> String {
        let dateObj:Date? = DateFormatter().getDateFrom(stringDate: date, format: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        let formattedDate:String? = DateFormatter().getStringFrom(date: dateObj!, format: "dd MMM YYYY HH:mm a")
        return formattedDate ?? ""
    }
    
    func formatJobDate(date: String) -> String {
        let dateObj:Date? = DateFormatter().getDateFrom(stringDate: date, format: "yyyy-MM-dd HH:mm:ss")
        let formattedDate:String? = DateFormatter().getStringFrom(date: dateObj!, format: "dd MMM YYYY HH:mm a")
        return formattedDate ?? ""
    }
    
}
