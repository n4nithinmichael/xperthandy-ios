//
//  SPDashboardNotificationsCollectionViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 3/7/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class SPDashboardNotificationsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var bgView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var amoundLabel: UILabel!
    @IBOutlet var viewButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bgView.setCornerRadius(8.0)
        self.bgView.dropShadow()
        
        self.titleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.dateLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 11)
        
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.timeLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.locationLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.distanceLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.amoundLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        
        
        
        self.viewButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        
        // Initialization code
    }
    
    @IBAction func viewButtonClicked(_ sender: Any) {
    }
}
