//
//  SPJobNotificationDetailsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/3/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class SPJobNotificationDetailsViewController: UIViewController {
    @IBOutlet var urgentLabel: UILabel!
    @IBOutlet var jobTitleLabel: UILabel!
    @IBOutlet var postedDateLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var hoursLabel: UILabel!
    @IBOutlet var descriptionTitleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var locationTitlelabel: UILabel!
    @IBOutlet var addressTitlelabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var prefTimeTitlelabel: UILabel!
    @IBOutlet var acceptButton: UIButton!
    @IBOutlet var prefTimeLabel: UILabel!
    @IBOutlet var rejectButton: UIButton!
    
    var jobId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.urgentLabel.setCornerRadius(12.5)
        self.urgentLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 11)
        self.jobTitleLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 20)
        self.postedDateLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 16)
        self.amountLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 32)
        self.hoursLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.descriptionTitleLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.locationTitlelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.addressTitlelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.prefTimeTitlelabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.locationLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.addressLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.prefTimeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        
        self.acceptButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.rejectButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        self.acceptButton.setCornerRadius(4.0)
        self.rejectButton.setCornerRadius(4.0)
        
        self.urgentLabel.text = ""
        self.jobTitleLabel.text = ""
        self.postedDateLabel.text = ""
        self.prefTimeLabel.text = ""
        self.hoursLabel.text = ""
        self.descriptionLabel.text = ""
        self.locationLabel.text = ""
        self.addressLabel.text = ""
        self.amountLabel.text = ""
        
        
        
        
        self.loadJob()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func acceptJobButton(_ sender: Any) {
        
        self.acceptJob()
        
    }
    
    @IBAction func rejectJobButton(_ sender: Any) {
        
        self.rejetJob()
    }
    
    func loadJob()  {
        
        
        ServerSync.shared.getJobById(jobId : self.jobId,data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let data = responseObject?["data"] as? NSDictionary
                let title  = data?.value(forKey: "title") as? String
                let date  = data?.value(forKey: "start_date") as? String
                let time  = data?.value(forKey: "start_time") as? String
                let description  = data?.value(forKey: "description") as? String
                let hours_required  = data?.value(forKey: "hours_required") as? NSNumber
                let job_status  = data?.value(forKey: "job_status") as? String
                
                let service_category = data?.value(forKey: "service_category") as? NSDictionary
                let minimum_price  = service_category?.value(forKey: "minimum_price") as? String
                let hourly_price  = service_category?.value(forKey: "hourly_price") as? String
                let service_type  = service_category?.value(forKey: "service_type") as? String
                let priceMin = Int(minimum_price!)
                let hours = hours_required!.intValue
                let pricePerHour = Int(hourly_price!)
                
                
                
                var priceMax = pricePerHour! * hours
                
                if(priceMax<priceMin!)
                {
                    priceMax = priceMin!
                }
                
                
                self.urgentLabel.text = job_status
                self.jobTitleLabel.text = title
                self.postedDateLabel.text = date
                self.prefTimeLabel.text = time
                self.hoursLabel.text = hours_required!.stringValue+" Hours work"
                self.descriptionLabel.text = description
                self.amountLabel.text = "₹ \(priceMax)"
                
                
                
            }
            
            
        }
    }
    
    func rejetJob()  {
        
        let params: NSDictionary = ["job_status":"Rejected"]
        
        
        ServerSync.shared.updateJobStatus(jobId : self.jobId, data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
                //                let topViewController = UIApplication.getTopMostViewController()
                //
                //                
                //                topViewController?.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                
            }
        }
        
    }
    func acceptJob()  {
        
        
        let params: NSDictionary = ["job_status":"Work accepted"]
        
        
        ServerSync.shared.updateJobStatus(jobId : self.jobId, data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    self.navigationController?.popViewController(animated: true)
                    
                }
                
                //                let topViewController = UIApplication.getTopMostViewController()
                //
                //
                //                topViewController?.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                
            }
            
        }
    }
    
    
}
