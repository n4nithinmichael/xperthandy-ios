//
//  PaymentViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 4/26/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit
import Razorpay

class PaymentViewController: UIViewController,ExternalWalletSelectionProtocol,RazorpayPaymentCompletionProtocolWithData,RazorpayPaymentCompletionProtocol {
    
    @IBOutlet var payLabel: UILabel!
    @IBOutlet var cashButton: UIButton!
    @IBOutlet var cardButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var payementButtonView: UIView!
    var razorpay: RazorpayCheckout!
    
    var jobId = Int()
    var amount = String()
    
    var paymentOrderId = String()
    
    var paymentOption : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.SEMI_BOLD_FONT, size: 20)
        self.payementButtonView.setCornerRadius(5.0)
        self.cashButton.setCornerRadius(21.0)
        self.cardButton.setCornerRadius(21.0)
        self.payLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.cashButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.cardButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.cashButton.setTitleColor("000000".getUIColorFromHex(), for: .normal)
        self.cardButton.setTitleColor("000000".getUIColorFromHex(), for: .normal)
        self.cashButton.backgroundColor = "#EEF1F6".getUIColorFromHex()
        self.cardButton.backgroundColor = "#EEF1F6".getUIColorFromHex()
        
        self.getJobPayments()
        
        razorpay = RazorpayCheckout.initWithKey(Constants.RazorPayKeys.keyId, andDelegate: self)
        razorpay.setExternalWalletSelectionDelegate(self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cashButtonClicked(_ sender: Any) {
        
        self.paymentOption = "cash"
        self.cashButton.setTitleColor("4BBC71".getUIColorFromHex(), for: .normal)
        self.cashButton.backgroundColor = .white
        self.cashButton.setBorder(color: "4BBC71".getUIColorFromHex(), width: 1.0)
        
        self.cardButton.setTitleColor("000000".getUIColorFromHex(), for: .normal)
        self.cardButton.backgroundColor = "EEF1F6".getUIColorFromHex()
        self.cardButton.setBorder(color: "EEF1F6".getUIColorFromHex(), width: 1.0)
        
        self.choosePayment()
        
    }
    @IBAction func cardButtonClicked(_ sender: Any) {
        
        self.paymentOption = "card"
        
        self.cardButton.setTitleColor("4BBC71".getUIColorFromHex(), for: .normal)
        self.cardButton.backgroundColor = .white
        self.cardButton.setBorder(color: "4BBC71".getUIColorFromHex(), width: 1.0)
        
        self.cashButton.setTitleColor("000000".getUIColorFromHex(), for: .normal)
        self.cashButton.backgroundColor = "EEF1F6".getUIColorFromHex()
        self.cashButton.setBorder(color: "EEF1F6".getUIColorFromHex(), width: 1.0)
        
        self.choosePayment()
    }
    
    @objc func getJobPayments()  {
        
        
        
        ServerSync.shared.getJobPayments(jobId: self.jobId,data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                
                let dataDict = responseObject?.value(forKey: "data") as? NSDictionary
                
                let total = dataDict?.value(forKey: "total") as! String
                
                
                self.payLabel.text =  "PAY ₹ \(total)"
                self.amount = total
                
                
            }
        }
        
        
    }
    
    func showPaymentOption ()
    {
        
        let phone = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.UserPhone) ?? ""
        let email = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.UserEmail) ?? ""
        
        
        let options: [String:Any] = [
            "key": Constants.RazorPayKeys.keyId,
            "amount": self.amount,
            "currency": "INR",
            "description": "",
            "order_id": self.paymentOrderId,
            "image": "",
            "name": "",
            "prefill": [
                "contact": phone,
                "email": email
            ],
            "theme": [
                "color": ""
            ]
        ]
        razorpay.open(options)
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        self.alertMessageOk(title: "Error", message: str)
        
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        // self.confirmPayment(paymentId: payment_id)
        self.confirmPayment()
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        self.alertMessageOk(title: "Error", message: str)
        
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        
        self.confirmPayment()
        
    }
    
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        
    }
    
    
    func choosePayment()
    {
        
        if(self.paymentOption != nil)
        {
            
            let params: NSDictionary = ["payment_type": self.paymentOption!]
            
            
            ServerSync.shared.choosePayments(jobId: self.jobId,data: params as [NSObject : AnyObject]) { (responseObject, error) in
                
                if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    let dataDict = responseObject?.value(forKey: "data") as? NSDictionary
                    let paymentid = dataDict?.value(forKey: "payment_gateway_order_id") as? String
                    self.paymentOrderId = paymentid ?? ""
                    
                    
                    
                }
            }
        }
        else{
            self.alertMessageOk(title: "Choose", message: "Please choose a payment option.")
        }
    }
    
    func confirmPayment()
    {
        
        if(self.paymentOption != nil)
        {
            
            let params: NSDictionary = ["payment_gateway_order_id": self.paymentOrderId]
            
            
            ServerSync.shared.confirmPayments(jobId: self.jobId,data: params as [NSObject : AnyObject]) { (responseObject, error) in
                
                if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    
                    self.navigationController?.popToViewController(ofClass: CustomerDashBoardViewController.self)
                    
                    
                    let topViewController = UIApplication.getTopMostViewController()
                    
                    
                    topViewController?.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                }
            }
        }
        else{
            self.alertMessageOk(title: "Choose", message: "Please choose a payment option.")
            
        }
    }
    
    @IBAction func paymentButtonClickedAction(_ sender: Any) {
        
        if(self.paymentOption == "cash")
        {
            self.confirmPayment()
            
        }
        else if (self.paymentOption == "card"){
            self.showPaymentOption()
            
        }
        else{
            self.alertMessageOk(title: "Choose", message: "Please choose a payment option.")
            
        }
    }
    
}
