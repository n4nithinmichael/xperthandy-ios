//
//  OtpVarificationViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/6/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import OTPFieldView
class OtpVarificationViewController: UIViewController,OTPFieldViewDelegate {
    
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var verifyButton: UIButton!
    @IBOutlet var otpView: OTPFieldView!
    @IBOutlet var resendButton: UIButton!
    var otpString: String!
    var enteredOtp: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.verifyButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.resendButton.titleLabel?.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.verifyButton.setCornerRadius(4.0)
        
        // Do any additional setup after loading the view.
    }
    
    func setupOtpView(){
        
        self.otpView.fieldsCount = 4
        self.otpView.fieldBorderWidth = 0
        self.otpView.defaultBorderColor = UIColor.black
        self.otpView.filledBorderColor = UIColor.green
        self.otpView.cursorColor = "151A5A".getUIColorFromHex()
        self.otpView.defaultBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.otpView.filledBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.otpView.displayType = .roundedCorner
        self.otpView.fieldSize = 50
        self.otpView.separatorSpace = 10
        self.otpView.shouldAllowIntermediateEditing = true
        self.otpView.delegate = self
        self.otpView.becomeFirstResponder()
        self.otpView.fieldFont = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)!
        self.otpView.initializeUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupOtpView()
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendButtonClicked(_ sender: Any) {
    }
    @IBAction func verifyButtonClicked(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "SelectUserTypeViewController") as? SelectUserTypeViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
        //        if self.enteredOtp.count == 0 {
        //            self.alertMessageOk(title: "Otp", message:"Please enter the otp.")
        //
        //        }
        //        else if self.enteredOtp == self.otpString{
        //            let vc = self.storyboard!.instantiateViewController(withIdentifier: "SelectUserTypeViewController") as? SelectUserTypeViewController
        //            self.navigationController?.pushViewController(vc!, animated: true)
        //
        //        }
        //        else{
        //            self.alertMessageOk(title: "Otp", message: "Otp is wrong.")
        //
        //        }
        
        
    }
    
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
        
    }
    
    func enteredOTP(otp: String) {
        if otpString == otp {
            
            UserDefaults.standard.setValue(1, forKey: Constants.UserDefaultKeys.AppState)
            
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "SelectUserTypeViewController") as? SelectUserTypeViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        self.enteredOtp = otp
        print("OTPString: \(otp)")
        
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        print("Has entered all OTP? \(hasEnteredAll)")
        return false
        
    }
    
}

