//
//  SelectUserTypeViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

class SelectUserTypeViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var verifyButton: UIButton!
    @IBOutlet var skilledWorkerLabel: UILabel!
    @IBOutlet var customerLabel: UILabel!
    @IBOutlet var skilledDescription: UILabel!
    @IBOutlet var customerDescription: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var skilledWorkerView: UIView!
    @IBOutlet var customerView: UIView!
    @IBOutlet var skilledWorkerSelctedImage: UIImageView!
    @IBOutlet var customerSelectedImage: UIImageView!
    
    var userTypeId : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.verifyButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.verifyButton.setCornerRadius(4.0)
        self.skilledWorkerView.setCornerRadius(20.0)
        self.customerView.setCornerRadius(20.0)
        self.skilledWorkerLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.customerLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.skilledDescription.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.customerDescription.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.descriptionLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        
        self.userTypeId = 0
        
        //self.selectSkilled(select: true)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skilledWorkerSelected(_ sender: Any) {
        
        self.selectSkilled(select: true)
    }
    
    @IBAction func customerSelected(_ sender: Any) {
        self.selectSkilled(select: false)
        
    }
    func selectSkilled(select:Bool)  {
        
        if select {
            self.skilledWorkerSelctedImage.image = UIImage(named: "skilled_worker_selected")
            self.customerSelectedImage.image = UIImage(named: "customer_unselected")
            self.userTypeId = 2
            
            
        }
        else{
            self.skilledWorkerSelctedImage.image = UIImage(named: "skilled_worker_unselected")
            self.customerSelectedImage.image = UIImage(named: "customer_selected")
            self.userTypeId = 3
            
        }
    }
    @IBAction func createProfileButtonClicked(_ sender: Any) {
        
        
        
        if self.userTypeId == 0 {
            self.alertMessageOk(title: "User type", message: "Please select a user type.")
            
        }
        else{
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "InformationsTabViewController") as? InformationsTabViewController
            //vc?.userTypeId = self.userTypeId
            UserDefaults.standard.setValue(self.userTypeId, forKey: Constants.UserDefaultKeys.UserType)
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }

}
