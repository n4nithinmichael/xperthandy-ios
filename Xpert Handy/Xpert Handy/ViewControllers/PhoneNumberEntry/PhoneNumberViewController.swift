//
//  PhoneNumberViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/6/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

class PhoneNumberViewController: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var phoneView: UIView!
    @IBOutlet var phoneTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.sendButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.phoneTextField.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.phoneView.setCornerRadius(4.0)
        self.sendButton.setCornerRadius(4.0)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        self.sendOtp()
        
    }
    
    
    func sendOtp()  {
        
        // Core.shared.deleteAllUserDatails()
        
        
        //        let vc = self.storyboard!.instantiateViewController(withIdentifier: "OtpVarificationViewController") as? OtpVarificationViewController
        //        self.navigationController?.pushViewController(vc!, animated: true)
        //
        //        return
        
        let phoneNumberString = self.phoneTextField.text?.removeWhitespace() ?? ""
        
        if(phoneNumberString.isPhoneNumber == true && phoneNumberString.count == 10)
        {
            
            
            
            let params: NSDictionary = ["phone": "91"+self.phoneTextField.text!]
            
            ServerSync.shared.registerUser(data: params as [NSObject : AnyObject]) { (responseObject, error) in
                
                
                if let error = error {
                    // got an error in getting the data, need to handle it
                    print(error)
                    
                    self.alertMessageOk(title: "Error", message: error.localizedDescription)
                    
                }
                else {
                    
                    if(responseObject!["statusCode"] != nil)
                    {
                        
                        if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                        {
                            
                            let tockenDict = responseObject?.value(forKey: "data")
                            let tocken = (tockenDict as AnyObject).value(forKey: "token")
                            let otp = (tockenDict as AnyObject).value(forKey: "otp") as! NSNumber
                            
                            if  (tocken != nil)
                            {
                                UserDefaults().set(tocken, forKey: Constants.UserDefaultKeys.AuthToken)
                            }
                            
                            
                            
                            let vc = self.storyboard!.instantiateViewController(withIdentifier: "OtpVarificationViewController") as? OtpVarificationViewController
                            vc?.otpString = otp.stringValue
                            self.navigationController?.pushViewController(vc!, animated: true)
                            
                            self.alertMessageOk(title: "Otp", message: otp.stringValue)
                            
                        }
                    }
                    else
                    {
                        self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    }
                    
                }
                
                
            }
        }
        else{
            
            self.alertMessageOk(title: "Phone number.", message: "Please enter a valid phone number.")
            
        }
    }
    
}
