//
//  OnBoardingPaginationViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/6/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

class OnBoardingPaginationViewController: UIViewController {

    @IBOutlet var imagevIew: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 24)
        self.descriptionLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)

        // Do any additional setup after loading the view.
    }

}
