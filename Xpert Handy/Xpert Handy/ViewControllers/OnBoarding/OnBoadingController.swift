//
//  OnBoadingController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 8/16/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import PageMaster
import ISPageControl

class OnBoadingController: UIViewController,PageMasterDelegate {
    
    @IBOutlet var paginationView: UIView!
    private let pageMaster = PageMaster([])
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var skipButton: UIButton!
    
    lazy var viewControllers: [UIViewController] = {
        var viewControllers = [UIViewController]()
        for i in 0 ..< 3 {
            viewControllers.append(makeChildViewController(at: i))
        }
        return viewControllers
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupPageMaster()
        
        self.skipButton.titleLabel?.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)

        
        self.pageControl.numberOfPages = viewControllers.count
        self.pageControl.pageIndicatorTintColor = UIColor(red: 0.902, green: 0.902, blue: 0.937, alpha: 1)
        self.pageControl.currentPageIndicatorTintColor = UIColor(red: 0.302, green: 0.482, blue: 0.953, alpha: 1)

        self.view.bringSubviewToFront(self.pageControl)
        self.setupPageFor(index: 0)
        
        self.skipButton.setTitleColor("#FF6D6E".getUIColorFromHex(), for: .normal)

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    @IBAction func skipButtonPressed(_ sender: Any) {
        
    }
    
    private func setupPageMaster() {
        
        self.pageMaster.pageDelegate = self
        self.pageMaster.isInfinite = false
        self.pageMaster.view.frame = self.paginationView.bounds
        self.paginationView.addSubview(self.pageMaster.view)
        self.pageMaster.setup(viewControllers)
        
        self.pageMaster.didMove(toParent: self)
        
        
        
    }
    
    
    func makeChildViewController(at index: Int?) -> OnBoardingPaginationViewController {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "OnBoardingPaginationViewController") as! OnBoardingPaginationViewController
        self.paginationView.layoutIfNeeded()
        viewController.view.frame = self.paginationView.bounds
        return viewController
    }
    
    
    func pageMaster(_ master: PageMaster, didChangePage page: Int) {
        self.pageControl.currentPage = page
        self.setupPageFor(index: page)
        
    }
    
    func setupPageFor(index:Int)  {
    }
    
}

