//
//  AnswerCollectionViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/15/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

class AnswerCollectionViewCell: UICollectionViewCell {
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var answerLabel: UILabel!
    
    // Note: must be strong
    @IBOutlet private var maxWidthConstraint: NSLayoutConstraint! {
        didSet {
            maxWidthConstraint.isActive = false
        }
    }
    
    var maxWidth: CGFloat? = nil {
        didSet {
            guard let maxWidth = maxWidth else {
                return
            }
            maxWidthConstraint.isActive = true
            maxWidthConstraint.constant = maxWidth
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.numberLabel.font  =  UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.answerLabel.font  =  UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.contentView.setCornerRadius(4.0)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
