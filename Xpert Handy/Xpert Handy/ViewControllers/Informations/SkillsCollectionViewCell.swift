//
//  SkillsCollectionViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/14/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

class SkillsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bgView.setCornerRadius(8.0)
        
        self.nameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        // Initialization code
    }
}
