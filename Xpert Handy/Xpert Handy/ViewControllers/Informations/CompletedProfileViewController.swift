//
//  CompletedProfileViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

class CompletedProfileViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func completedProfileClicked(_ sender: Any) {
        
        let userTypeId =  UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.UserType)
        
        if userTypeId == 2 {
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ServiceProviderDashBoardViewController") as? ServiceProviderDashBoardViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        else {
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "CustomerDashBoardViewController") as? CustomerDashBoardViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
            
        }
        
    }
    
}
