//
//  VerifyEmailPhoneViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 6/15/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit
import OTPFieldView

@objc protocol VerifyViewDelegate: AnyObject {
    
    func  verifyNextSelcted()
    
}


class VerifyEmailPhoneViewController: UIViewController,OTPFieldViewDelegate {
    
    
    var delegate : VerifyViewDelegate!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var verifyNumberLabel: UILabel!
    @IBOutlet var verifyEmailLabel: UILabel!
    @IBOutlet var numberBgView: UIView!
    @IBOutlet var emailBgView: UIView!
    @IBOutlet var numberVerifyView: UIView!
    @IBOutlet var emailverifyView: UIView!
    @IBOutlet var numbertextField: UITextField!
    @IBOutlet var emailtextField: UITextField!
    @IBOutlet var phoneotpView: OTPFieldView!
    @IBOutlet var emailotpView: OTPFieldView!
    @IBOutlet var numberSendButton: UIButton!
    @IBOutlet var emailSendButton: UIButton!
    
    
    var otpState: Int!
    
    var phoneOtpString: String!
    var emailOtpString: String!
    
    var enteredOtp: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nextButton.setCornerRadius(4.0)
        
        self.numberLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.verifyNumberLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.verifyEmailLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.nextButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.numberSendButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailSendButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.numberBgView.setCornerRadius(4.0)
        self.emailBgView.setCornerRadius(4.0)
        self.numberSendButton.setCornerRadius(4.0)
        self.emailSendButton.setCornerRadius(4.0)
        
        self.emailverifyView.isHidden = true
        
        self.nextButton.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    func setupOtpView(){
        
        self.phoneotpView.fieldsCount = 4
        self.phoneotpView.fieldBorderWidth = 0
        self.phoneotpView.defaultBorderColor = UIColor.black
        self.phoneotpView.filledBorderColor = UIColor.green
        self.phoneotpView.cursorColor = "151A5A".getUIColorFromHex()
        self.phoneotpView.defaultBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.phoneotpView.filledBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.phoneotpView.displayType = .roundedCorner
        self.phoneotpView.fieldSize = 50
        self.phoneotpView.separatorSpace = 10
        self.phoneotpView.shouldAllowIntermediateEditing = true
        self.phoneotpView.delegate = self
        self.phoneotpView.becomeFirstResponder()
        self.phoneotpView.fieldFont = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)!
        self.phoneotpView.initializeUI()
        
        
        
        self.emailotpView.fieldsCount = 4
        self.emailotpView.fieldBorderWidth = 0
        self.emailotpView.defaultBorderColor = UIColor.black
        self.emailotpView.filledBorderColor = UIColor.green
        self.emailotpView.cursorColor = "151A5A".getUIColorFromHex()
        self.emailotpView.defaultBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.emailotpView.filledBackgroundColor = "EEF1F6".getUIColorFromHex()
        self.emailotpView.displayType = .roundedCorner
        self.emailotpView.fieldSize = 50
        self.emailotpView.separatorSpace = 10
        self.emailotpView.shouldAllowIntermediateEditing = true
        self.emailotpView.delegate = self
        self.emailotpView.becomeFirstResponder()
        self.emailotpView.fieldFont = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)!
        self.emailotpView.initializeUI()
        
        self.emailtextField.isUserInteractionEnabled = false
        self.numbertextField.isUserInteractionEnabled  = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupOtpView()
        self.getUser()
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        
        self.delegate.verifyNextSelcted()
    }
    
    @IBAction func verifyMobileClicked(_ sender: Any) {
        
        self.sendPhoneOtp()
    }
    @IBAction func verifyEmailClicked(_ sender: Any) {
        
        self.sendEmailOtp()
    }
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
        
    }
    
    
    func getUser()  {
        
        let email = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.UserEmail) ?? nil
        let phone = UserDefaults.standard.string(forKey: Constants.UserDefaultKeys.UserPhone) ?? nil
        self.emailtextField.text = email
        self.numbertextField.text = phone
        
    }
    
    func sendPhoneOtp()
    {
        
        self.otpState = 1
        ServerSync.shared.getverifyPhoneOtp(data: nil) { (responseObject, error) in
            
            //fatalError("Failed to load a MyCustomCell from the table.")
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    let data = responseObject!["data"] as? NSDictionary
                    let otp = data?.value(forKey: "otp_code") as? NSNumber
                    self.phoneOtpString = otp?.stringValue
                    self.alertMessageOk(title: "otp", message: otp!.stringValue)
                    
                }
                else{
                    self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                }
                
            }
            

        }
    }
    
    func verifyPhoneOtp(otp:String)
    {
        
        let params: NSDictionary = ["otp_code": otp]
        
        
        self.otpState = 2
        ServerSync.shared.verifyPhoneotp(data: params as [NSObject : AnyObject]) { (responseObject, error) in
            
            //fatalError("Failed to load a MyCustomCell from the table.")
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                }
                else{
                    self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                }
                
            }
            
            
            
        }
    }
    
    func sendEmailOtp()
    {
        
        self.otpState = 2
        
        
        ServerSync.shared.getverifyEmailOtp(data:  nil) { (responseObject, error) in
            
            //fatalError("Failed to load a MyCustomCell from the table.")
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    let data = responseObject!["data"] as? NSDictionary
                    let otp = data?.value(forKey: "otp_code") as? NSNumber
                    self.emailOtpString = otp?.stringValue
                    self.alertMessageOk(title: "otp", message: otp!.stringValue)
                }
                else{
                    self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                }
                
            }
            
            
            
        }
    }
    
    func verifyEmailOtp(otp:String)
    {
        
        self.otpState = 2
        
        let params: NSDictionary = ["otp_code": otp]
        
        
        ServerSync.shared.verifyEmailOtp(data:  params as [NSObject : AnyObject]) { (responseObject, error) in
            
            //fatalError("Failed to load a MyCustomCell from the table.")
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                }
                else{
                    self.alertMessageOk(title: "", message: responseObject!["message"] as! String)
                    
                }
                
            }
            
        }
    }
    
    
    func enteredOTP(otp: String) {
        
        if(self.otpState == 0)
        {
            self.otpState = 1
        }
        
        self.enteredOtp = otp
        print("OTPString: \(otp)")
        
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        
        if(self.otpState == 1)
        {
            if(self.phoneOtpString == self.enteredOtp)
            {
                self.emailverifyView.isHidden = false
                
                self.verifyPhoneOtp(otp: self.enteredOtp)
                
                self.enteredOtp = ""
            }
            
            
        }
        else if(self.otpState == 2) {
            
            if(self.emailOtpString == self.enteredOtp)
            {
                self.verifyEmailOtp(otp: self.enteredOtp)
                
                
                self.enteredOtp = ""
                
                self.nextButton.isHidden = false
            }
            
        }
        print("Has entered all OTP? \(hasEnteredAll)")
        return false
        
    }
    
}
