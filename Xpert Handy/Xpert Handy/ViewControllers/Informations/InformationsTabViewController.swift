//
//  InformationsTabViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import PageMaster
import ISPageControl

class InformationsTabViewController: UIViewController,PageMasterDelegate,BasicInfoDelegate,LocationAndAddressDelegate,SelectWorkAreaDelegate,VerificationViewDelegate,InformationVideoViewDelegate,QuestinnairDelegate,ResultDelegate,VerifyViewDelegate,SubscriptionsViewDelegate,SubscriptionDetailsViewDelegate {
    
    
    var userTypeId = UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.UserType)
    @IBOutlet var paginationView: UIView!
    private let pageMaster = PageMaster([])
    @IBOutlet var pageControl: UIPageControl!
    
    @IBOutlet var titleLabel: UILabel!
    
    lazy var viewControllers: [UIViewController] = {
        
        var views : Int
        if self.userTypeId == 3{
            views = 4
        }
        else{
            views = 11
        }
        var viewControllers = [UIViewController]()
        for i in 0 ..< views {
            viewControllers.append(makeChildViewController(at: i))
        }
        return viewControllers
    }()
    
    var infoDict = NSMutableDictionary()
    var subdict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userTypeId =  UserDefaults.standard.integer(forKey: Constants.UserDefaultKeys.UserType)
        
        self.setupPageMaster()
        
        self.titleLabel.font = UIFont(name: Constants.Font.BOLD_FONT, size: 20)
        
        self.pageControl.numberOfPages = viewControllers.count
        self.pageControl.pageIndicatorTintColor = UIColor(red: 0.902, green: 0.902, blue: 0.937, alpha: 1)
        self.pageControl.currentPageIndicatorTintColor = UIColor(red: 0.302, green: 0.482, blue: 0.953, alpha: 1)
        
        self.view.bringSubviewToFront(self.pageControl)
        self.setupPageFor(index: 0)
        self.removeSwipeGesture()
        
        
        if self.userTypeId == 3{
        }
        else{
            // self.pageMaster.setPage(5, animated: true)
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    func removeSwipeGesture(){
        for view in self.pageMaster.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        if self.pageMaster.currentPage == 0{
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.pageMaster.setPage(self.pageMaster.currentPage-1, animated: true)
            
        }
    }
    
    
    private func setupPageMaster() {
        
        self.pageMaster.pageDelegate = self
        self.pageMaster.isInfinite = false
        self.pageMaster.view.frame = self.paginationView.bounds
        self.paginationView.addSubview(self.pageMaster.view)
        self.pageMaster.setup(viewControllers)
        self.pageMaster.didMove(toParent: self)
        
    }
    
    
    func makeChildViewController(at index: Int?) -> UIViewController {
        
        var viewController = UIViewController ()
        if index == 0 {
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "BasicViewController") as! BasicViewController
            viewController1.userTypeId = self.userTypeId
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }
        
        else if index == 1 {
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "VerifyEmailPhoneViewController") as! VerifyEmailPhoneViewController
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }
        
        else if index == 2 {
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "LocationAndAddressViewController") as! LocationAndAddressViewController
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }
        else if index == 3 {
            
            if self.userTypeId == 3{
                
                let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "InformationVideosViewController") as! InformationVideosViewController
                viewController1.delegate = self
                self.paginationView.layoutIfNeeded()
                viewController1.view.frame = self.paginationView.bounds
                return viewController1
                
            }
            else{
                
                let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "SelectWorkAreaViewController") as! SelectWorkAreaViewController
                viewController1.delegate = self
                self.paginationView.layoutIfNeeded()
                viewController1.view.frame = self.paginationView.bounds
                return viewController1
                
            }
            
            
            
        }
        else if index == 4 {
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }
        
        else if index == 5 {
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "SubscriptionsListViewController") as! SubscriptionsListViewController
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }
        
        else if index == 6 {
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "SubscriptionDetailsViewController") as! SubscriptionDetailsViewController
            viewController1.delegate = self
            viewController1.subDict = self.subdict
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }
        
        else if index == 7 {
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "InformationVideosViewController") as! InformationVideosViewController
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
            
        }
        else if index == 8 {
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }
        else if index == 9 {
            
            
            let viewController1 = self.storyboard!.instantiateViewController(withIdentifier: "ResultViewController") as! ResultViewController
            viewController1.delegate = self
            self.paginationView.layoutIfNeeded()
            viewController1.view.frame = self.paginationView.bounds
            return viewController1
            
        }

        return viewController
        
    }
    
    
    func pageMaster(_ master: PageMaster, didChangePage page: Int) {
        
        if page == 0 {
            self.titleLabel.text = "Basic information"
        }
        if page == 1 {
            self.titleLabel.text = "Verification"
        }
        else if page == 2 {
            self.titleLabel.text = "Location and address"
            
        }
        else if page == 3 {
            
            if self.userTypeId == 3{
                
                self.titleLabel.text = "Information videos"
                
                
            }
            else{
                
                self.titleLabel.text = "Select work area"
                
                
            }
            
            
        }
        else if page == 4 {
            self.titleLabel.text = "Verification"
            
        }
        else if page == 5 {
            
            
            self.titleLabel.text = "Subscription plans"
            
        }
        else if page == 6 {
            
            let view = master.currentViewController as? SubscriptionDetailsViewController
            view?.subDict = self.subdict
            
            self.titleLabel.text = "Plan details"
            
        }
        else if page == 7 {
            self.titleLabel.text = "Information videos"
            
        }
        else if page == 8 {
            self.titleLabel.text = "Questionnaire"
            
        }
        else if page == 9 {
            self.titleLabel.text = "Result"
            
        }
        
        self.pageControl.currentPage = page
        self.setupPageFor(index: page)
        
    }
    
    func setupPageFor(index:Int)  {
    }
    
    
    func  nextButtonPressedWith(name: String, altNumber:String,email: String, pw:String, dob:String, gender:String) {
        
        // self.pageMaster.setPage(1, animated: true)
        
        self.infoDict.setValue(name, forKey: "name")
        self.infoDict.setValue(altNumber, forKey: "phone")
        self.infoDict.setValue(email, forKey: "email")
        self.infoDict.setValue(dob, forKey: "dob")
        self.infoDict.setValue(gender, forKey: "gender")
        self.infoDict.setValue(self.userTypeId, forKey: "userType")
        self.infoDict.setValue(self.userTypeId, forKey: "roles")
        self.infoDict.setValue(pw, forKey: "password")
        self.infoDict.setValue(pw, forKey: "password_confirm")
        
        
        ServerSync.shared.registerUser(data: self.infoDict as [NSObject : AnyObject]) { (respose, error) in
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else
            {
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    let data = respose!["data"] as? NSDictionary
                    let token = data!["token"] as? NSString
                    
                    // UserDefaults.standard.setValue(2, forKey: Constants.UserDefaultKeys.AppState)
                    UserDefaults.standard.setValue(token, forKey: Constants.UserDefaultKeys.AuthToken)
                    UserDefaults.standard.setValue(self.infoDict.value(forKey:"email"), forKey: Constants.UserDefaultKeys.UserEmail)
                    UserDefaults.standard.setValue(self.infoDict.value(forKey:"phone"), forKey: Constants.UserDefaultKeys.UserPhone)
                    UserDefaults.standard.setValue(self.infoDict.value(forKey:"password"), forKey: Constants.UserDefaultKeys.UserPassword)
                    
                    self.pageMaster.setPage(1, animated: true)
                    
                }
                else{
                    
                    self.alertMessageOk(title: "", message: respose!["message"] as! String)
                    
                }
            }
            
        }
        
        
        
    }
    
    func  verifyNextSelcted()
    {
        self.pageMaster.setPage(2, animated: true)
        
    }
    func nextButtonPressedWith(housename: String, area: String, town: String, district: String, pincode: String) {
        
        self.infoDict.setValue(housename, forKey: "house_name")
        self.infoDict.setValue(area, forKey: "area")
        self.infoDict.setValue(town, forKey: "town")
        self.infoDict.setValue(district, forKey: "district")
        self.infoDict.setValue("Kerala", forKey: "state")
        self.infoDict.setValue(pincode, forKey: "pin")
        
        
        
        ServerSync.shared.updateUser(data: self.infoDict as [NSObject : AnyObject]) { (respose, error) in
            
            if let error = error {
                // got an error in getting the data, need to handle it
                print(error)
                
                self.alertMessageOk(title: "Error", message: error.localizedDescription)
                
            }
            else
            {
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    if (self.userTypeId == 3)
                    {
                        UserDefaults.standard.setValue(2, forKey: Constants.UserDefaultKeys.AppState)
                        
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CompleteProfileViewController") as? CompleteProfileViewController
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    else{
                        self.pageMaster.setPage(3, animated: true)
                        
                    }
                }
                else{
                    
                    self.pageMaster.setPage(3, animated: true)
                    
                    self.alertMessageOk(title: "", message: respose!["message"] as! String)
                    
                }
            }
            
        }
        
    }
    
    func  workAreaSelected(serviceArray: NSMutableArray)
    {
        
        let params: NSDictionary = ["serviceCategory": serviceArray]
        
        
        ServerSync.shared.updatServicecategory(data: params as [NSObject : AnyObject]) { (respose, error) in
            
            self.pageMaster.setPage(4, animated: true)
            
        }
        
    }
    
    func  filesSelcted()
    {
        self.pageMaster.setPage(5, animated: true)
        
    }
    
    func  subscriptionsSelcted(subDict:NSDictionary)       // self.pageMaster.setPage(6, animated: true)
    {
        self.subdict = subDict
        
        self.pageMaster.setPage(6, animated: true)
        
    }
    
    func subscriptionsPaid() {
        self.pageMaster.setPage(7, animated: true)
        
    }
    
    
    func  videoNextSelcted()
    {
        
        if (self.userTypeId == 3)
        {
            UserDefaults.standard.setValue(3, forKey: Constants.UserDefaultKeys.AppState)
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "CompleteProfileViewController") as? CompleteProfileViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else{
            
            self.pageMaster.setPage(8, animated: true)
            
        }
        
        
    }
    func  questionnairNextClicked()
    {
        self.pageMaster.setPage(9, animated: true)
        
    }
    
    func  resultNextClicked()
    {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "CompleteProfileViewController") as? CompleteProfileViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }

}
