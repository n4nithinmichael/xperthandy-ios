//
//  LocationAndAddressViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import PopMenu

@objc protocol LocationAndAddressDelegate: AnyObject {
    
    func  nextButtonPressedWith(housename: String, area:String,town: String, district:String, pincode:String)
    
}

class LocationAndAddressViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var housenameLabel: UILabel!
    @IBOutlet var housenameView: UIView!
    @IBOutlet var areaLabel: UILabel!
    @IBOutlet var areaView: UIView!
    @IBOutlet var townLabel: UILabel!
    @IBOutlet var townView: UIView!
    @IBOutlet var districtLabel: UILabel!
    @IBOutlet var districtView: UIView!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var pincodeLabel: UILabel!
    @IBOutlet var stateView: UIView!
    @IBOutlet var pincideView: UIView!
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var houseNameTextField: UITextField!
    @IBOutlet var areaTextField: UITextField!
    @IBOutlet var townTextField: UITextField!
    @IBOutlet var districtTextField: UITextField!
    @IBOutlet var pincodetextField: UITextField!
    
    @objc public var delegate : LocationAndAddressDelegate!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.nextButton.setCornerRadius(4.0)
        self.housenameView.setCornerRadius(4.0)
        self.areaView.setCornerRadius(4.0)
        self.townView.setCornerRadius(4.0)
        self.districtView.setCornerRadius(4.0)
        self.stateView.setCornerRadius(4.0)
        self.pincideView.setCornerRadius(4.0)
        
        self.housenameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.areaLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.townLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.districtLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.stateLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.pincodeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.nextButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.districtTextField.inputView = UIView()
        self.districtTextField.delegate = self
        
        self.getUser()
        // self.districtTextField.addTarget(self, action: #selector(self.districtSelected), for: .touchDown)
        
        // Do any additional setup after loading the view.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.districtTextField {
            self.setUpPopUpMenu()
        }
    }
    
    
    func setUpPopUpMenu()  {
        
        let manager = PopMenuManager.default
        
        
        
        let action1 = PopMenuDefaultAction(title: "Thiruvananthapuram", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        let action2 = PopMenuDefaultAction(title: "Kollam", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        let action3 = PopMenuDefaultAction(title: "Alappuzha", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        let action4 = PopMenuDefaultAction(title: "Pathanamthitta", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        let action5 = PopMenuDefaultAction(title: "Kottayam", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action6 = PopMenuDefaultAction(title: "Idukki", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action7 = PopMenuDefaultAction(title: "Ernakulam", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        
        let action8 = PopMenuDefaultAction(title: "Thrissur", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action9 = PopMenuDefaultAction(title: "Palakkad", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action10 = PopMenuDefaultAction(title: "Malappuram", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action11 = PopMenuDefaultAction(title: "Kozhikode", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action12 = PopMenuDefaultAction(title: "Wayanadu", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action13 = PopMenuDefaultAction(title: "Kannur", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        let action14 = PopMenuDefaultAction(title: "Kasaragod", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            self.districtTextField.text = action.title
            
        })
        
        
        manager.addAction(action1)
        manager.addAction(action2)
        manager.addAction(action3)
        manager.addAction(action4)
        manager.addAction(action5)
        manager.addAction(action6)
        manager.addAction(action7)
        manager.addAction(action8)
        manager.addAction(action9)
        manager.addAction(action10)
        manager.addAction(action11)
        manager.addAction(action12)
        manager.addAction(action13)
        manager.addAction(action14)
        manager.present()
        
        
    }
    
    func getUser()  {
        
        
        ServerSync.shared.getUSerDeatails(data: nil) { (respose, error) in
            
            if(respose != nil)
            {
                if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    let dataDict = respose?.value(forKey: "data") as? NSDictionary
                    let house_name = dataDict?.value(forKey: "house_name") as? String
                    let pin = dataDict?.value(forKey: "pin") as? String
                    let area = dataDict?.value(forKey: "area") as? String
                    let town = dataDict?.value(forKey: "town") as? String
                    let district = dataDict?.value(forKey: "district") as? String
                    
                    
                    
                    self.houseNameTextField.text = house_name
                    self.pincodetextField.text = pin
                    self.areaTextField.text = area
                    self.townTextField.text = town
                    self.districtTextField.text = district
                    
                    
                }
            }
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        self.delegate.nextButtonPressedWith(housename: self.houseNameTextField?.text ?? "", area: self.areaTextField.text ?? "", town: self.townTextField.text ?? "", district: self.districtTextField.text ?? "", pincode: self.pincodetextField.text ?? "")
    }
    
}
