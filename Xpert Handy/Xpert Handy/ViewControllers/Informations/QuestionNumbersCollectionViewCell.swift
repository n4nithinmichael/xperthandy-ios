//
//  QuestionNumbersCollectionViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 2/21/21.
//  Copyright © 2021 Nithin Michael. All rights reserved.
//

import UIKit

class QuestionNumbersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.numberLabel.font =  UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        
        self.numberLabel.textColor = " #5B6382".getUIColorFromHex()
        
    }
    
    
}
