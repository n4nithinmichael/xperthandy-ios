//
//  VerificationViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import CoreLocation
import LocationPicker
import MapKit

@objc protocol VerificationViewDelegate: AnyObject {
    
    func  filesSelcted()
    
}

class VerificationViewController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet var titlelabael: UILabel!
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var aadharLabel: UILabel!
    @IBOutlet var aadharNolabel: UILabel!
    @IBOutlet var aadharNoView: UIView!
    @IBOutlet var aadharNumberTextField: UITextField!
    @IBOutlet var uploadAadharView: UIView!
    @IBOutlet var taekAadharView: UIView!
    @IBOutlet var jpgLabel: UILabel!
    @IBOutlet var aadharView: UIView!
    @IBOutlet var uploadAadharLabel: UILabel!
    @IBOutlet var takeAadharLabel: UILabel!
    
    @IBOutlet var pccView: UIView!
    @IBOutlet var pccLabel: UILabel!
    @IBOutlet var pccShowlabel: UILabel!
    
    @IBOutlet var serviceLocationView: UIView!
    @IBOutlet var serviceLocationLabel: UILabel!
    @IBOutlet var currentLocationLabel: UILabel!
    @IBOutlet var currentLocationView: UIView!
    @IBOutlet var otherLocationLabel: UILabel!
    @IBOutlet var otherLocationView: UIView!
    @IBOutlet var serviceAvailabeleIn: UILabel!
    @IBOutlet var kmLabel: UILabel!
    
    @IBOutlet var trainingView: UIView!
    @IBOutlet var traininglabel: UILabel!
    @IBOutlet var trainingField: UITextField!
    @IBOutlet var trainingFieldView: UIView!
    
    @IBOutlet var refernceView: UIView!
    @IBOutlet var referncelabel: UILabel!
    @IBOutlet var refernceField: UITextField!
    @IBOutlet var refernceFieldView: UIView!
    @IBOutlet var reefrenceSkipButton: UIButton!
    
    @IBOutlet var workImageLabel: UILabel!
    @IBOutlet var workImageTitlelabel: UILabel!
    @IBOutlet var workImageTitleView: UIView!
    @IBOutlet var workImageTitleTextField: UITextField!
    @IBOutlet var uploadWorkImageView: UIView!
    @IBOutlet var takeWorkImageView: UIView!
    @IBOutlet var maximumLabel: UILabel!
    @IBOutlet var workImageView: UIView!
    @IBOutlet var uploadWorkImageLabel: UILabel!
    @IBOutlet var takeWorkImageLabel: UILabel!
    @IBOutlet var workImageSkipButton: UIButton!
    @IBOutlet var serviceRangeSlider: UISlider!
    
    @IBOutlet var serviceTypeLabel: UILabel!
    @IBOutlet var instoreView: UIView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var instoreLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var instoreImageView: UIImageView!
    @IBOutlet var locationImageView: UIImageView!
    
    let locationManager = CLLocationManager()
    
    
    var delegate : VerificationViewDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titlelabael.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.nextButton.setCornerRadius(4.0)
        self.nextButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        self.aadharView.setCornerRadius(10.0)
        self.aadharView.dropShadow()
        self.aadharLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.aadharNolabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.jpgLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.uploadAadharLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.takeAadharLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.uploadAadharView.setCornerRadius(4.0)
        self.taekAadharView.setCornerRadius(4.0)
        self.aadharNoView.setCornerRadius(4.0)
        
        self.pccView.setCornerRadius(10.0)
        self.pccLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.pccShowlabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.serviceLocationView.setCornerRadius(10.0)
        self.serviceLocationView.dropShadow()
        self.serviceLocationLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.currentLocationLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 14)
        self.otherLocationLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 14)
        self.serviceAvailabeleIn.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.kmLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.currentLocationView.setCornerRadius(4.0)
        self.otherLocationView.setCornerRadius(4.0)
        
        self.trainingView.setCornerRadius(10.0)
        self.trainingView.dropShadow()
        self.traininglabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.trainingFieldView.setCornerRadius(4.0)
        
        self.refernceView.setCornerRadius(10.0)
        self.refernceView.dropShadow()
        self.referncelabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.reefrenceSkipButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.refernceFieldView.setCornerRadius(4.0)
        
        self.workImageView.setCornerRadius(10.0)
        self.workImageView.dropShadow()
        self.workImageLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        self.workImageTitlelabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.maximumLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.uploadWorkImageLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.takeWorkImageLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.workImageTitleView.setCornerRadius(4.0)
        self.takeWorkImageView.setCornerRadius(4.0)
        self.uploadAadharView.setCornerRadius(4.0)
        self.workImageSkipButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.serviceTypeLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        
        self.instoreLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.locationLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        
        self.instoreView.setCornerRadius(4.0)
        self.locationView.setCornerRadius(4.0)
        
        self.setInstoreSelected(selected: false)
    }
    
    
    @IBAction func instoreButtonSelected(_ sender: Any) {
        
        self.setInstoreSelected(selected: true)
    }
    @IBAction func locationButtonSelected(_ sender: Any) {
        
        self.setInstoreSelected(selected: false)
        
    }
    
    
    func setInstoreSelected(selected:Bool)  {
        
        self.setBothUnSelected()
        
        if selected {
            
            self.instoreView.backgroundColor = "#4BBC71".getUIColorFromHex()
            self.instoreLabel.textColor = .white
            self.instoreImageView.image = UIImage(named: "gender_selected")
        }
        else{
            self.locationView.backgroundColor = "#4BBC71".getUIColorFromHex()
            self.locationLabel.textColor = .white
            self.locationImageView.image = UIImage(named: "gender_selected")
            
        }
    }
    
    func setBothUnSelected()  {
        self.locationView.backgroundColor = "#EEF1F6".getUIColorFromHex()
        self.instoreView.backgroundColor = "#EEF1F6".getUIColorFromHex()
        self.instoreLabel.textColor = "#5B6382".getUIColorFromHex()
        self.locationLabel.textColor = "#5B6382".getUIColorFromHex()
        self.locationImageView.image = nil
        self.instoreImageView.image = nil
        
        
        
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        self.delegate.filesSelcted()
    }
    
    @IBAction func uploadImageButtonClicked(_ sender: Any) {
        
        ImagePickerManager().pickImage(self){ image in
            
            
        }
        
    }
    @IBAction func takePhotoButtonClicked(_ sender: Any) {
        
        ImagePickerManager().pickImage(self){ image in
            
            
        }
    }
    @IBAction func currentLocationButtonClicked(_ sender: Any) {
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
        
    }
    
    @IBAction func otherLocationButtonClicked(_ sender: Any) {
        

        
        let locationPicker = LocationPickerViewController()
        
        
        //        // you can optionally set initial location
        //        let placemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.locationManager.location?.coordinate.latitude, longitude: self.locationManager.location?.coordinate.latitude), addressDictionary: nil)
        //        let location = Location(name: "1 Infinite Loop, Cupertino", location: self.locationManager.location, placemark: placemark)
        //        locationPicker.location = location
        
        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        
        // default: navigation bar's `barTintColor` or `UIColor.white`
        locationPicker.currentLocationButtonBackground = .blue
        
        // ignored if initial location is given, shows that location instead
        locationPicker.showCurrentLocationInitially = true // default: true
        
        locationPicker.mapType = .standard // default: .Hybrid
        
        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false
        
        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"
        
        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"
        
        // optional region distance to be used for creation region when user selects place from search results
        locationPicker.resultRegionDistance = 500 // default: 600
        
        locationPicker.completion = { location in
            // do some awesome stuff with location
        }
        
        self.present(locationPicker, animated: true, completion: nil)
        
        //se,f.pushViewController(locationPicker, animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
    
}
