//
//  InfoVideoCollectionViewCell.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/14/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit


class InfoVideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var thumbnailImage: UIImageView!
    var playButtonTapped:()->() = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.nameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        // Initialization code
    }
    @IBAction func playButtonClicked(_ sender: Any) {
        playButtonTapped()
    }
    func setPlayButtonTapped(action:@escaping ()->()){
        playButtonTapped = action
    }
}
