//
//  BasicViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import YYCalendar


@objc protocol BasicInfoDelegate: AnyObject {
    
    func  nextButtonPressedWith(name: String, altNumber:String,email: String, pw:String, dob:String, gender:String)
    
}

class BasicViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var pwLabel: UILabel!
    @IBOutlet var confirmPwLabel: UILabel!
    @IBOutlet var dobLabel: UILabel!
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var maleLabel: UILabel!
    @IBOutlet var femaleLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var fullnameBgView: UIView!
    @IBOutlet var altNumberBgView: UIView!
    @IBOutlet var emailBgView: UIView!
    @IBOutlet var pwBgView: UIView!
    @IBOutlet var confirmpwBgView: UIView!
    @IBOutlet var dateBgview: UIView!
    @IBOutlet var maleBgView: UIView!
    @IBOutlet var femaleBgView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nametextField: UITextField!
    @IBOutlet var alternateNumber: UITextField!
    @IBOutlet var emailtextField: UITextField!
    @IBOutlet var pwTextField: UITextField!
    @IBOutlet var confirmpwTextField: UITextField!
    @IBOutlet var dobTextField: UITextField!
    @IBOutlet var maleSelectedImageView: UIImageView!
    @IBOutlet var femaleSelectedImageview: UIImageView!
    
    @objc public var delegate : BasicInfoDelegate!
    
    
    var userTypeId : Int!
    var gender : String = ""
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fullNameLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.numberLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.emailLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.pwLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.confirmPwLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.dobLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.genderLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.femaleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.maleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.nextButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        self.nextButton.setCornerRadius(4.0)
        self.fullnameBgView.setCornerRadius(4.0)
        self.altNumberBgView.setCornerRadius(4.0)
        self.emailBgView.setCornerRadius(4.0)
        self.pwBgView.setCornerRadius(4.0)
        self.confirmpwBgView.setCornerRadius(4.0)
        self.dateBgview.setCornerRadius(4.0)
        self.maleBgView.setCornerRadius(4.0)
        self.femaleBgView.setCornerRadius(4.0)
        self.profileImageView.setCornerRadius(50.0)
        
        self.dobTextField.inputView = UIView()
        self.dobTextField.delegate = self
        
        // self.getUser()
        // Do any additional setup after loading the view.
    }
    
    func getUser()  {
        

        ServerSync.shared.getUSerDeatails(data: nil) { (respose, error) in
            
            
            if(respose?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = respose?.value(forKey: "data") as? NSDictionary
                let name = dataDict?.value(forKey: "name") as? String
                let email = dataDict?.value(forKey: "email") as? String
                var dob = dataDict?.value(forKey: "dob") as? String ?? ""
                var phone = dataDict?.value(forKey: "phone") as? String ?? ""
                let gender = dataDict?.value(forKey: "gender") as? String
                
                if phone.count>10 {
                    
                    phone = String(phone.dropFirst(2))
                }
                
                if dob.count > 10 {
                    dob = String(dob.prefix(10))
                    
                }
                
                let profile_image = dataDict?.value(forKey: "profile_image") as? String
                self.nametextField.text = name
                self.emailtextField.text = email
                self.dobTextField.text = dob
                self.gender = gender ?? ""
                self.alternateNumber.text = phone
                
                if self.gender == "Male" {
                    self.maleSelected(select: true)
                }
                else if self.gender == "Female"{
                    
                    self.maleSelected(select: false)
                    
                }
                
                self.profileImageView.sd_setImage(with: URL(string: profile_image ?? "" ), placeholderImage: UIImage(named: "customer_avatar"))
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.dobTextField {
            self.setUpCalenderView()
        }
    }
    
    
    func setUpCalenderView()  {
        
        var dateComponent = DateComponents()
        
        
        dateComponent.year = 100
        
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        
        print(futureDate!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let selectedFutureDate = dateFormatter.string(from: futureDate!)
        let selectedCurrentDate = dateFormatter.string(from: Date())
        
        
        
        let calendarView = YYCalendar(limitedCalendarLangType: .ENG2, date: selectedCurrentDate, minDate:  nil, maxDate: selectedFutureDate, format: "yyyy-MM-dd", completion: { (date) in
     
            self.dobTextField.text = date
            
        })
        calendarView.saturdayColor = .black
        calendarView.selectedDayColor = UIColor(red: 0.725, green: 0.576, blue: 0.035, alpha: 1)
        calendarView.show()
        
        
        
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @IBAction func selectProfileImageButtonClicked(_ sender: Any) {
        
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            self.profileImageView.image = image
            
            ServerSync.shared.uploadImage(image: image, fileName: "") { (responseObject, error) in
                
            }
            
        }
    }
    @IBAction func maleButtonClicked(_ sender: Any) {
        
        self.maleSelected(select: true)
    }
    @IBAction func femaleButtonClicked(_ sender: Any) {
        
        self.maleSelected(select: false)
        
    }
    @IBAction func nextButtonSelected(_ sender: Any) {
        
        
        
        let email = self.emailtextField.text ?? ""
        
        
        let phoneNumberString = self.alternateNumber.text?.removeWhitespace() ?? ""
        
        
        if(self.nametextField.text?.count == 0 )
        {
            self.alertMessageOk(title: "Name.", message: "Please enter a valid name.")
            
        }
        else if(phoneNumberString.isPhoneNumber == false || phoneNumberString.count != 10)
                
        {
            self.alertMessageOk(title: "Phonenumber.", message: "Please enter a valid phine number.")
            
        }
        else if (email.count == 0 || !email.isValidEmail() )
        {
            self.alertMessageOk(title: "Email.", message: "Please enter a valid email.")
            
        }
        else if (self.dobTextField.text?.count == 0)
        {
            self.alertMessageOk(title: "Date of birth.", message: "Please enter a valid date of birth.")
            
        }
        else if (self.gender.count == 0)
        {
            self.alertMessageOk(title: "Gender.", message: "Please select your gender.")
            
        }
        else{
            
            self.delegate.nextButtonPressedWith(name: self.nametextField.text!, altNumber: "91"+self.alternateNumber.text!, email: self.emailtextField.text ?? "",pw:self.pwTextField.text!, dob: self.dobTextField.text!, gender: self.gender)
        }
        
        
    }
    
    func maleSelected(select:Bool)  {
        
        if select {
            self.maleSelectedImageView.image = UIImage(named: "gender_selected")
            self.femaleSelectedImageview.image = nil
            self.gender = "Male"
            
            
        }
        else{
            self.maleSelectedImageView.image = nil
            self.femaleSelectedImageview.image =  UIImage(named: "gender_selected")
            self.gender = "Female"
            
        }
    }
}
