//
//  QuestionsViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/15/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit


@objc protocol QuestionsDeligate: AnyObject {
    
    func  questionAnswered()
    
}

class QuestionsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout! {
        didSet {
            collectionLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    @IBOutlet var questionCollectionView: UICollectionView!
    var selectedAnswer : Int = 0
    var delegate : QuestionsDeligate!
    var questionsDict : NSDictionary!
    var answeresArray : NSArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.answeresArray = self.questionsDict.value(forKey: "answerList") as? NSArray
        
        self.questionCollectionView.reloadData()
        //        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //        layout.itemSize = CGSize(width: Constants.ScreenSize.SCREEN_WIDTH-40, height: 45)
        //        layout.minimumInteritemSpacing = 10
        //        layout.minimumLineSpacing = 10
        //        layout.scrollDirection = .vertical
        //        self.questionCollectionView!.collectionViewLayout = layout
        
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.answeresArray.count+1
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionCollectionViewCell", for: indexPath as IndexPath) as! QuestionCollectionViewCell
            cell.maxWidth = collectionView.bounds.width - 16
            
            cell.titleLabel.text = self.questionsDict.value(forKey: "question") as? String
            return cell
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnswerCollectionViewCell", for: indexPath as IndexPath) as! AnswerCollectionViewCell
            cell.maxWidth = collectionView.bounds.width - 46
            
            let answerDict = self.answeresArray.object(at: indexPath.row-1) as? NSDictionary
            cell.answerLabel.text = answerDict!.value(forKey: "answer") as? String
            
            if indexPath.row == 1 {
                cell.numberLabel.text = "a"
            }
            else if indexPath.row == 2 {
                cell.numberLabel.text = "b"
                
            }
            else if indexPath.row == 3 {
                cell.numberLabel.text = "c"
                
            }
            else if indexPath.row == 4 {
                cell.numberLabel.text = "d"
                
            }
            
            if indexPath.row == self.selectedAnswer {
                cell.contentView.backgroundColor = "#4BBC71".getUIColorFromHex()
            }
            else{
                cell.contentView.backgroundColor = "#EEF1F6".getUIColorFromHex()
                
            }
            
            cell.layoutSubviews()
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
        }
        else{
            self.selectedAnswer = indexPath.row
            
            self.questionCollectionView.reloadData()
            self.delegate.questionAnswered()
        }
    }
    
}
