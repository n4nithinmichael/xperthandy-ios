//
//  InformationVideosViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

@objc protocol InformationVideoViewDelegate: AnyObject {
    
    func  videoNextSelcted()
    
}

class InformationVideosViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var selectionLabel: UILabel!
    @IBOutlet var videosCollectionView: UICollectionView!
    
    var delegate : InformationVideoViewDelegate!
    
    var videosArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectionLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 12)
        self.nextButton.setCornerRadius(4.0)
        self.nextButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: Constants.ScreenSize.SCREEN_WIDTH-40, height: 245)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .vertical
        self.videosCollectionView!.collectionViewLayout = layout
        // Do any additional setup after loading the view.
        self.loadInformationsVideos()
    }
    
    
    func loadInformationsVideos()  {
        
        
        ServerSync.shared.getInformationVideos(data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = responseObject?.value(forKey: "data")
                self.videosArray = dataDict as! NSArray
                self.videosCollectionView!.reloadData()
            }
            
            
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        self.delegate.videoNextSelcted()
    }
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videosArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InfoVideoCollectionViewCell", for: indexPath as IndexPath) as! InfoVideoCollectionViewCell
        
        let serviceDict = self.videosArray[indexPath.row] as! NSDictionary
        let url    = serviceDict.value(forKey: "url") as! String
        let name    = serviceDict.value(forKey: "title") as! String
        
        cell.nameLabel.text = name
        
        let queryItems = URLComponents(string: url)?.queryItems
        let param1 = queryItems?.filter({$0.name == "v"}).first
        let value = param1?.value as! String
        
        let thumbnail = String(format: "http://img.youtube.com/vi/%@/0.jpg", value)
        
        cell.thumbnailImage.sd_setImage(with: URL(string: thumbnail ), placeholderImage: nil)
        
        cell.setPlayButtonTapped {
            
            let playerView = YTPlayerView(frame: self.view.frame)
            playerView.load(withVideoId: value);
            
        }
        return cell
    }
    
    
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: Constants.ScreenSize.SCREEN_WIDTH-40, height: 245)
    }
    
}
