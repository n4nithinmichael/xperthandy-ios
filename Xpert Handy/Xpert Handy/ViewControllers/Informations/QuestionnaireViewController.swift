//
//  QuestionnaireViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import PageMaster

@objc protocol QuestinnairDelegate: AnyObject {
    
    func  questionnairNextClicked()
    
}

class QuestionnaireViewController: UIViewController,PageMasterDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,QuestionsDeligate {
    
    @IBOutlet var paginationView: UIView!
    @IBOutlet var titleLabel: UILabel!
    private let pageMaster = PageMaster([])
    @IBOutlet var nextButton: UIButton!
    var selectedQuestion = 0
    var delegate : QuestinnairDelegate!
    var qestionsArray : NSArray!
    
    @IBOutlet var numbeersCollectionView: UICollectionView!
    
    lazy var viewControllers: [UIViewController] = {
        var viewControllers = [UIViewController]()
        for i in 0 ..< self.qestionsArray.count{
            viewControllers.append(makeChildViewController(at: i))
        }
        return viewControllers
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.paginationView.setCornerRadius(10)
        self.paginationView.dropShadow()
        
        
        self.loadQuestionnairs()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.loadQuestionnairs()
        
    }
    
    
    func loadQuestionnairs()  {
        
        
        ServerSync.shared.getQuestinnareList(data: nil) { (responseObject, error) in
            
            if(responseObject?["statusCode"] as? Int == Constants.ResponseStatus.Success)
            {
                
                let dataDict = responseObject?.value(forKey: "data")
                self.qestionsArray = dataDict as? NSArray
                self.setupPageMaster()
                self.setupPageFor(index: 0)
            }
            
            
        }
    }
    
    private func setupPageMaster() {
        
        self.pageMaster.pageDelegate = self
        self.pageMaster.isInfinite = false
        self.pageMaster.view.frame = self.paginationView.bounds
        self.paginationView.addSubview(self.pageMaster.view)
        self.pageMaster.setup(viewControllers)
        self.pageMaster.didMove(toParent: self)
        
        
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        UserDefaults.standard.setValue(2, forKey: Constants.UserDefaultKeys.AppState)
        
        self.delegate.questionnairNextClicked()
    }
    
    func makeChildViewController(at index: Int?) -> UIViewController {
        
        let viewController  = self.storyboard!.instantiateViewController(withIdentifier: "QuestionsViewController") as! QuestionsViewController
        self.paginationView.layoutIfNeeded()
        viewController.questionsDict = self.qestionsArray.object(at: index!) as! NSDictionary
        viewController.delegate = self
        viewController.view.frame = self.paginationView.bounds
        return viewController
    }
    
    
    func pageMaster(_ master: PageMaster, didChangePage page: Int) {
        
        self.selectedQuestion = page
        self.numbeersCollectionView.reloadData()
        self.setupPageFor(index: page)
        
    }
    
    func setupPageFor(index:Int)  {
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.qestionsArray?.count ?? 0
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionNumbersCollectionViewCell", for: indexPath as IndexPath) as! QuestionNumbersCollectionViewCell
        if indexPath.row == self.selectedQuestion{
            cell.numberLabel.backgroundColor = "#E4F7FE".getUIColorFromHex()
        }
        else{
            cell.numberLabel.backgroundColor = .white
            
        }
        cell.numberLabel.text = String(indexPath.row+1)
        cell.numberLabel.setCornerRadius(12.5)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 10
        let dim = (Constants.ScreenSize.SCREEN_WIDTH-40)/cellsAcross
        return CGSize(width: dim, height: 45)
        
        // return CGSize(width: 35, height: 45)
        
    }
    
    
    func  questionAnswered()
    {
        if self.pageMaster.currentPage != self.qestionsArray.count-1 {
            
            self.pageMaster.setPage(self.pageMaster.currentPage+1, animated: true)
        }
        else{
            
            UserDefaults.standard.setValue(2, forKey: Constants.UserDefaultKeys.AppState)
            
            self.delegate.questionnairNextClicked()
        }
        
    }
    
}
