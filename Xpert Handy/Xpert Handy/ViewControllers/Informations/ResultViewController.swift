//
//  ResultViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit

@objc protocol ResultDelegate: AnyObject {
    
    func  resultNextClicked()
    
}
class ResultViewController: UIViewController {
    
    @IBOutlet var congratsLabel: UILabel!
    @IBOutlet var resultView: UIView!
    @IBOutlet var numbersView: UIView!
    @IBOutlet var answersLabel: UILabel!
    @IBOutlet var outofLabel: UILabel!
    @IBOutlet var resultButton: UIButton!
    var delegate : ResultDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.congratsLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.answersLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 72)
        self.outofLabel.font = UIFont(name: Constants.Font.REGULAR_FONT, size: 16)
        self.numbersView.setCornerRadius(80)
        self.resultView.setCornerRadius(20)
        self.resultView.dropShadow()
        self.resultButton.setCornerRadius(4.0)
        self.resultButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        self.delegate.resultNextClicked()
        
    }
}
