//
//  SelectWorkAreaViewController.swift
//  Xpert Handy
//
//  Created by Nithin Michael on 9/12/20.
//  Copyright © 2020 Nithin Michael. All rights reserved.
//

import UIKit
import SDWebImage


@objc protocol SelectWorkAreaDelegate: AnyObject {
    
    func  workAreaSelected(serviceArray: NSMutableArray)
    
}

class SelectWorkAreaViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var selectionLabel: UILabel!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var skillsCollectionView: UICollectionView!
    
    var delegate : SelectWorkAreaDelegate!
    var serviceArray = NSArray()
    var selectedService = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.nextButton.setCornerRadius(4.0)
        self.searchView.setCornerRadius(4.0)
        self.selectionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 18)
        self.descriptionLabel.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 12)
        self.nextButton.titleLabel?.font = UIFont(name: Constants.Font.MEDIUM_FONT, size: 16)
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (Constants.ScreenSize.SCREEN_WIDTH-50)/2, height: 66)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .vertical
        self.skillsCollectionView!.collectionViewLayout = layout
        
        self.loadServieCategory()
        
        // Do any additional setup after loading the view.
    }
    
    func loadServieCategory()  {
        
        ServerSync.shared.getServiceCategories(data: nil) { (responseObject, error) in
            
            if(responseObject != nil)
            {
                if(responseObject!["statusCode"] as? Int == Constants.ResponseStatus.Success)
                {
                    
                    let dataDict = responseObject?.value(forKey: "data")
                    self.serviceArray = dataDict as! NSArray
                    self.skillsCollectionView!.reloadData()
                }
            }
            
        }
    }
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        // self.delegate.workAreaSelected(serviceArray: self.selectedService)
        
        
        if self.selectedService.count == 0 {
            self.alertMessageOk(title: "Service category.", message: "Please select your service category.")
            
        }
        else{
            self.delegate.workAreaSelected(serviceArray: self.selectedService)
            
        }
        
        
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SkillsCollectionViewCell", for: indexPath as IndexPath) as! SkillsCollectionViewCell
        
        let serviceDict = self.serviceArray[indexPath.row] as! NSDictionary
        let color    = serviceDict.value(forKey: "background_colour_code") as! String
        let image    = serviceDict.value(forKey: "image") as! String
        let name    = serviceDict.value(forKey: "name") as! String
        let serviceId = serviceDict.value(forKey: "id") as! NSNumber
        cell.nameLabel.text = name
        if self.selectedService.contains(serviceId) {
            cell.iconImageView.image = UIImage(named: "gender_selected")
            
        }
        else{
            cell.iconImageView.sd_setImage(with: URL(string: image ), placeholderImage: nil)
            
        }
        cell.bgView.backgroundColor = color.getUIColorFromHex()
        
        
        
        
        return cell
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (Constants.ScreenSize.SCREEN_WIDTH-50)/2, height: 66)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let serviceDict = self.serviceArray[indexPath.row] as! NSDictionary
        let serviceId = serviceDict.value(forKey: "id") as! NSNumber
        
        
        if self.selectedService.contains(serviceId) {
            
            self.selectedService.remove(serviceId)
            
        }
        else{
            
            self.selectedService.removeAllObjects()
            self.selectedService.add(serviceId)
            
        }
        self.skillsCollectionView.reloadData()
    }
    
}
